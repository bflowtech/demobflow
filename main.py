from src.views.dashboard_view import DashboardView
from src.controllers.dashboard_controller import DashboardController

if __name__ == '__main__':
    app = DashboardController(DashboardView)
    app.run()

