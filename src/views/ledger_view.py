from src.views.base_view import BaseView
import tkinter as tk
from tkinter import ttk


class LedgerView(BaseView):
    
    def __init__(self, ledger_controller):
        super(LedgerView, self).__init__()
        self.title('Manage Ledger')
        self.ledger_frame = tk.Frame(self)
        self.controller = ledger_controller
        self.set_layout()

    def set_layout(self):
        ledger_label = tk.Label(self.ledger_frame, text='Ledger')
        ledger_label.grid(row=1,column=2)

        accounts_label = tk.Label(self.ledger_frame, text='Accounts')
        accounts_label.grid(row=2,column=1)
        
        self.account_clicked = tk.StringVar()
        self.account_clicked_id = tk.IntVar()
        
        group_of_accounts = [
            'ASSET',
            'LIABLITY',
            'INCOME',
            'EXPENSE',
            ]

        self.account_clicked.set(group_of_accounts[0])
        self.account_option = ttk.Combobox(self.ledger_frame,textvariable=self.account_clicked,values=group_of_accounts)
        self.account_option.bind('<<ComboboxSelected>>',self.controller.selected_account)
        self.account_option.grid(row=2,column=2)
        self.account_clicked.trace("w", self.controller.load_accounts)
        groups_label = tk.Label(self.ledger_frame, text='Groups')
        groups_label.grid(row=3, column=1)
        self.group_clicked = tk.StringVar()
        self.group_clicked_id = tk.IntVar()
        self.group_option = ttk.Combobox(self.ledger_frame,textvariable=self.group_clicked)
        self.group_option.grid(row=3, column=2)
        self.group_option.bind('<KeyRelease>',self.controller.on_change_groups)
        self.group_option.bind('<<ComboboxSelected>>',self.controller.selected_group)

        ledger_name = tk.Label(self.ledger_frame,text='Ledger Name')
        ledger_name.grid(row=4,column=1)
        ledger_name_input = tk.Entry(self.ledger_frame)
        ledger_name_input.grid(row=4,column=2)



        self.income_group_form()

        self.ledger_frame.pack(pady=10)

    #asset Group
    def asset_forms(self):
        #current asset
        credit_period_label = tk.Label(self.ledger_frame,text='Credit Period')
        credit_period_label.grid(row=5,column=1)
        self.credit_period_input = tk.Entry(self.ledger_frame)
        self.credit_period_input.grid(row=5,column=2)

        contact_details_label = tk.Label(self.ledger_frame,text='Contact Details')
        contact_details_label.grid(row=6,column=1)
        self.contact_details_input = tk.Entry(self.ledger_frame)
        self.contact_details_input.grid(row=6,column=2)

        address_details_label = tk.Label(self.ledger_frame,text='Address')
        address_details_label.grid(row=7,column=1)
        self.address_details_input = tk.Entry(self.ledger_frame)
        self.address_details_input.grid(row=7,column=2)

        phone_number_label = tk.Label(self.ledger_frame,text='Phone Number')
        phone_number_label.grid(row=8,column=1)
        self.phone_number_input = tk.Entry(self.ledger_frame)
        self.phone_number_input.grid(row=8,column=2)

        pan_no_label = tk.Label(self.ledger_frame,text='Pan No')
        pan_no_label.grid(row=9,column=1)
        self.pan_no_input = tk.Entry(self.ledger_frame)
        self.pan_no_input.grid(row=9,column=2)

        gst_label = tk.Label(self.ledger_frame,text='GST Type')
        gst_label.grid(row=9,column=1)
        
        types_of_gst = [
            'Regular',
            'Composition',
            'Unregistered',
            'Customer'
            ]

        self.gst_clicked = tk.StringVar()
        self.gst_clicked.set(types_of_gst[0])
        self.gst_options = ttk.Combobox(self.ledger_frame, values=types_of_gst, textvariable=self.gst_clicked)
        self.gst_options.grid(row=9,column=2)
        self.gst_clicked.trace('w', self.controller.gst_onchange)


        gst_no_label = tk.Label(self.ledger_frame, text='Gst No')
        gst_no_label.grid(row=10,column=1)
        self.gst_no_input = tk.Entry(self.ledger_frame)
        self.gst_no_input.grid(row=10,column=2)

        gst_percentage_label = tk.Label(self.ledger_frame,text='Gst %')
        gst_percentage_label.grid(row=11,column=1)
        self.gst_percentage_input = tk.Entry(self.ledger_frame)
        self.gst_percentage_input.grid(row=11,column=2)

        opening_balance_label = tk.Label(self.ledger_frame,text='Opening Balance')
        opening_balance_label.grid(row=12,column=1)
        self.opening_balance_input = tk.Entry(self.ledger_frame)
        self.opening_balance_input.grid(row=12,column=2)

        self.ledger_add_btn = tk.Button(self.ledger_frame,text='ADD',command=self.controller.add_ledger)
        self.ledger_add_btn.grid(row=13,column=2)

    def bank_forms(self):
        contact_details_label = tk.Label(self.ledger_frame, text='Contact Details')
        contact_details_label.grid(row=5,column=1)
        contact_details_input = tk.Entry(self.ledger_frame)
        contact_details_input.grid(row=5,column=2)

        address_label = tk.Label(self.ledger_frame, text='Address')
        address_label.grid(row=6,column=1)
        address_entry = tk.Entry(self.ledger_frame)
        address_entry.grid(row=6,column=2)

        phone_number_label = tk.Label(self.ledger_frame, text='Phone Number')
        phone_number_label.grid(row=7,column=1)
        phone_number_entry = tk.Entry(self.ledger_frame)
        phone_number_entry.grid(row=7,column=2)

        #  gstType
        #  gst_no

        bank_name = tk.Label(self.ledger_frame, text='Bank Name')
        bank_name.grid(row=8,column=1)

        bank_input = tk.Entry(self.ledger_frame)
        bank_input.grid(row=8,column=2)

        bank_address = tk.Label(self.ledger_frame, text='Bank Address')
        bank_address.grid(row=9,column=1)
        bank_address_input = tk.Entry(self.ledger_frame)
        bank_address_input.grid(row=9,column=2)

        account_no = tk.Label(self.ledger_frame, text='Bank A/C No')
        account_no.grid(row=10,column=1)
        account_no_input = tk.Entry(self.ledger_frame)
        account_no_input.grid(row=10,column=2)

        ifsc_code = tk.Label(self.ledger_frame, text='IFSC CODE')
        ifsc_code.grid(row=11,column=1)
        ifsc_code_input = tk.Entry(self.ledger_frame)
        ifsc_code_input.grid(row=11,column=2)

        branch_no = tk.Label(self.ledger_frame, text='Branch No')
        branch_no.grid(row=12,column=1)
        branch_no_input = tk.Entry(self.ledger_frame)
        branch_no_input.grid(row=12,column=2)

        branch_code = tk.Label(self.ledger_frame, text='Branch Code')
        branch_code.grid(row=13,column=1)
        branch_code_input = tk.Entry(self.ledger_frame)
        branch_code_input.grid(row=13,column=2)

        bank_contact_label = tk.Label(self.ledger_frame, text='Bank Contact No')
        bank_contact_label.grid(row=14,column=1)
        bank_contact_input = tk.Entry(self.ledger_frame)
        bank_contact_input.grid(row=14,column=2)

        cheque_no_label = tk.Label(self.ledger_frame, text='Cheque No')
        cheque_no_label.grid(row=15,column=1)
        cheque_no_input = tk.Entry(self.ledger_frame)
        cheque_no_input.grid(row=15,column=2)

        opening_balance_label = tk.Label(self.ledger_frame, text='Opening Balance')
        opening_balance_label.grid(row=16,column=1)
        opening_balance_input = tk.Entry(self.ledger_frame)
        opening_balance_input.grid(row=16,column=2)

        self.ledger_add_btn = tk.Button(self.ledger_frame,text='ADD')
        self.ledger_add_btn.grid(row=17,column=2)

    def cash_in_hand_forms(self):
        contact_details_label = tk.Label(self.ledger_frame,text='Contact Details')
        contact_details_label.grid(row=5,column=1)
        contact_details_input = tk.Entry(self.ledger_frame)
        contact_details_input.grid(row=5,column=2)

        address_details_label = tk.Label(self.ledger_frame,text='Address')
        address_details_label.grid(row=6,column=1)
        address_details_input = tk.Entry(self.ledger_frame)
        address_details_input.grid(row=6,column=2)

        phone_number_label = tk.Label(self.ledger_frame,text='Phone Number')
        phone_number_label.grid(row=7,column=1)
        phone_number_input = tk.Entry(self.ledger_frame)
        phone_number_input.grid(row=7,column=2)

        opening_balance_label = tk.Label(self.ledger_frame,text='Opening Balance')
        opening_balance_label.grid(row=8,column=1)
        opening_balance_input = tk.Entry(self.ledger_frame)
        opening_balance_input.grid(row=8,column=2)

    def petty_cash_forms(self):
        contact_details_label = tk.Label(self.ledger_frame,text='Contact Details')
        contact_details_label.grid(row=5,column=1)
        contact_details_input = tk.Entry(self.ledger_frame)
        contact_details_input.grid(row=5,column=2)

        address_details_label = tk.Label(self.ledger_frame,text='Address')
        address_details_label.grid(row=6,column=1)
        address_details_input = tk.Entry(self.ledger_frame)
        address_details_input.grid(row=6,column=2)

        phone_number_label = tk.Label(self.ledger_frame,text='Phone Number')
        phone_number_label.grid(row=7,column=1)
        phone_number_input = tk.Entry(self.ledger_frame)
        phone_number_input.grid(row=7,column=2)

        opening_balance_label = tk.Label(self.ledger_frame,text='Opening Balance')
        opening_balance_label.grid(row=8,column=1)
        opening_balance_input = tk.Entry(self.ledger_frame)
        opening_balance_input.grid(row=8,column=2)

    def sundry_debtor_forms(self):
        credit_period_label = tk.Label(self.ledger_frame,text='Credit Period')
        credit_period_label.grid(row=5,column=1)
        credit_period_input = tk.Entry(self.ledger_frame)
        credit_period_input.grid(row=5,column=2)


        contact_details_label = tk.Label(self.ledger_frame,text='Contact Details')
        contact_details_label.grid(row=6,column=1)
        contact_details_input = tk.Entry(self.ledger_frame)
        contact_details_input.grid(row=6,column=2)

        address_details_label = tk.Label(self.ledger_frame,text='Address')
        address_details_label.grid(row=7,column=1)
        address_details_input = tk.Entry(self.ledger_frame)
        address_details_input.grid(row=7,column=2)

        phone_number_label = tk.Label(self.ledger_frame,text='Phone Number')
        phone_number_label.grid(row=8,column=1)
        phone_number_input = tk.Entry(self.ledger_frame)
        phone_number_input.grid(row=8,column=2)

        opening_balance_label = tk.Label(self.ledger_frame,text='Opening Balance')
        opening_balance_label.grid(row=9,column=1)
        opening_balance_input = tk.Entry(self.ledger_frame)
        opening_balance_input.grid(row=9,column=2)

    def bills_recivable_forms(self):
        credit_period_label = tk.Label(self.ledger_frame,text='Credit Period')
        credit_period_label.grid(row=5,column=1)
        credit_period_input = tk.Entry(self.ledger_frame)
        credit_period_input.grid(row=5,column=2)


        contact_details_label = tk.Label(self.ledger_frame,text='Contact Details')
        contact_details_label.grid(row=6,column=1)
        contact_details_input = tk.Entry(self.ledger_frame)
        contact_details_input.grid(row=6,column=2)

        address_details_label = tk.Label(self.ledger_frame,text='Address')
        address_details_label.grid(row=7,column=1)
        address_details_input = tk.Entry(self.ledger_frame)
        address_details_input.grid(row=7,column=2)

        phone_number_label = tk.Label(self.ledger_frame,text='Phone Number')
        phone_number_label.grid(row=8,column=1)
        phone_number_input = tk.Entry(self.ledger_frame)
        phone_number_input.grid(row=8,column=2)

        opening_balance_label = tk.Label(self.ledger_frame,text='Opening Balance')
        opening_balance_label.grid(row=9,column=1)
        opening_balance_input = tk.Entry(self.ledger_frame)
        opening_balance_input.grid(row=9,column=2)

    def prepaid_expense_forms(self):

        contact_details_label = tk.Label(self.ledger_frame,text='Contact Details')
        contact_details_label.grid(row=5,column=1)
        contact_details_input = tk.Entry(self.ledger_frame)
        contact_details_input.grid(row=5,column=2)

        address_details_label = tk.Label(self.ledger_frame,text='Address')
        address_details_label.grid(row=6,column=1)
        address_details_input = tk.Entry(self.ledger_frame)
        address_details_input.grid(row=6,column=2)

        phone_number_label = tk.Label(self.ledger_frame,text='Phone Number')
        phone_number_label.grid(row=7,column=1)
        phone_number_input = tk.Entry(self.ledger_frame)
        phone_number_input.grid(row=7,column=2)

        opening_balance_label = tk.Label(self.ledger_frame,text='Opening Balance')
        opening_balance_label.grid(row=8,column=1)
        opening_balance_input = tk.Entry(self.ledger_frame)
        opening_balance_input.grid(row=8,column=2)

    def accured_income_forms(self):
        contact_details_label = tk.Label(self.ledger_frame,text='Contact Details')
        contact_details_label.grid(row=5,column=1)
        contact_details_input = tk.Entry(self.ledger_frame)
        contact_details_input.grid(row=5,column=2)

        address_details_label = tk.Label(self.ledger_frame,text='Address')
        address_details_label.grid(row=6,column=1)
        address_details_input = tk.Entry(self.ledger_frame)
        address_details_input.grid(row=6,column=2)

        phone_number_label = tk.Label(self.ledger_frame,text='Phone Number')
        phone_number_label.grid(row=7,column=1)
        phone_number_input = tk.Entry(self.ledger_frame)
        phone_number_input.grid(row=7,column=2)

        opening_balance_label = tk.Label(self.ledger_frame,text='Opening Balance')
        opening_balance_label.grid(row=8,column=1)
        opening_balance_input = tk.Entry(self.ledger_frame)
        opening_balance_input.grid(row=8,column=2)

    #Liablity Group
    def outstanding_expense_form(self):
        contact_details_label = tk.Label(self.ledger_frame,text='Contact Details')
        contact_details_label.grid(row=5,column=1)
        contact_details_input = tk.Entry(self.ledger_frame)
        contact_details_input.grid(row=5,column=2)

        address_details_label = tk.Label(self.ledger_frame,text='Address')
        address_details_label.grid(row=6,column=1)
        address_details_input = tk.Entry(self.ledger_frame)
        address_details_input.grid(row=6,column=2)

        phone_number_label = tk.Label(self.ledger_frame,text='Phone Number')
        phone_number_label.grid(row=7,column=1)
        phone_number_input = tk.Entry(self.ledger_frame)
        phone_number_input.grid(row=7,column=2)

        opening_balance_label = tk.Label(self.ledger_frame,text='Opening Balance')
        opening_balance_label.grid(row=8,column=1)
        opening_balance_input = tk.Entry(self.ledger_frame)
        opening_balance_input.grid(row=8,column=2)

    def duties_and_taxes_form(self):
        type_of_duty = tk.Label(self.ledger_frame, text='Type Of Duty')
        type_of_duty.grid(row=5,column=1)

        tax_type = tk.Label(self.ledger_frame,text='Tax Type')
        tax_type.grid(row=6,column=1)

        percentage_of_tax = tk.Label(self.ledger_frame,text='Percentage of Tax')
        percentage_of_tax.grid(row=7,column=1)

        opening_balance = tk.Label(self.ledger_frame,text='Opening Balance')
        opening_balance.grid(row=8,column=1)
        opening_balance_input = tk.Entry(self.ledger_frame)
        opening_balance_input.grid(row=8,column=2)

    def capital_form(self):
        contact_number_label = tk.Label(self.ledger_frame,text='Contact Number')
        contact_number_label.grid(row=5,column=1)
        contact_number_input = tk.Entry(self.ledger_frame)
        contact_number_input.grid(row=5,column=2)

        address_label = tk.Label(self.ledger_frame,text='Address Label')
        address_label.grid(row=6,column=1)
        address_input = tk.Entry(self.ledger_frame)
        address_input.grid(row=6,column=2)

        phone_number_label = tk.Label(self.ledger_frame,text='Phone Number')
        phone_number_label.grid(row=7,column=1)
        phone_number_input = tk.Entry(self.ledger_frame)
        phone_number_input.grid(row=7,column=2)

        opening_balance_label = tk.Label(self.ledger_frame,text='Opening Balance')
        opening_balance_label.grid(row=8,column=1)
        opening_balance_input = tk.Entry(self.ledger_frame)
        opening_balance_input.grid(row=8,column=2)

    def drawings_form(self):
        contact_number_label = tk.Label(self.ledger_frame,text='Contact Number')
        contact_number_label.grid(row=5,column=1)
        contact_number_input = tk.Entry(self.ledger_frame)
        contact_number_input.grid(row=5,column=2)

        address_label = tk.Label(self.ledger_frame,text='Address')
        address_label.grid(row=6,column=1)
        address_input = tk.Entry(self.ledger_frame)
        address_input.grid(row=6,column=2)

        opening_balance_label = tk.Label(self.ledger_frame,text='Opening Balance')
        opening_balance_label.grid(row=7,column=1)
        opening_balance_input = tk.Entry(self.ledger_frame)
        opening_balance_input.grid(row=7,column=2)

    def bank_loans_form(self):
        contact_number_label = tk.Label(self.ledger_frame,text='Contact Number')
        contact_number_label.grid(row=5,column=1)
        contact_number_input = tk.Entry(self.ledger_frame)
        contact_number_input.grid(row=5,column=2)

        address_label = tk.Label(self.ledger_frame,text='Address')
        address_label.grid(row=6,column=1)
        address_input = tk.Entry(self.ledger_frame)
        address_input.grid(row=6,column=2)

        bank_name_label = tk.Label(self.ledger_frame,text='Bank Name')
        bank_name_label.grid(row=7,column=1)
        bank_name_input = tk.Entry(self.ledger_frame)
        bank_name_input.grid(row=7,column=2)

        account_no_label = tk.Label(self.ledger_frame,text='A/C Number')
        account_no_label.grid(row=8,column=1)
        account_no_input = tk.Entry(self.ledger_frame)
        account_no_input.grid(row=8,column=2)

        ifsc_code_label = tk.Label(self.ledger_frame,text='IFSC Code')
        ifsc_code_label.grid(row=9,column=1)
        ifsc_code_input = tk.Entry(self.ledger_frame)
        ifsc_code_input.grid(row=9,column=2)

        branch_code_label = tk.Label(self.ledger_frame,text='Branch Code')
        branch_code_label.grid(row=10,column=1)
        branch_code_input = tk.Entry(self.ledger_frame)
        branch_code_input.grid(row=10,column=2)

        opening_balance_label = tk.Label(self.ledger_frame,text='Opening Balance')
        opening_balance_label.grid(row=11,column=1)
        opening_balance_input = tk.Entry(self.ledger_frame)
        opening_balance_input.grid(row=11,column=2)

    def loan_from_financers(self):
        contact_number_label = tk.Label(self.ledger_frame,text='Contact Number')
        contact_number_label.grid(row=5,column=1)
        contact_number_input = tk.Entry(self.ledger_frame)
        contact_number_input.grid(row=5,column=2)

        address_label = tk.Label(self.ledger_frame,text='Address')
        address_label.grid(row=6,column=1)
        address_input = tk.Entry(self.ledger_frame)
        address_input.grid(row=6,column=2)

        phone_number_label = tk.Label(self.ledger_frame,text='Phone Number')
        phone_number_label.grid(row=7,column=1)
        phone_number = tk.Entry(self.ledger_frame)
        phone_number.grid(row=7,column=2)

        opening_balance_label = tk.Label(self.ledger_frame,text='Opening Balance')
        opening_balance_label.grid(row=8,column=1)
        opening_balance_input = tk.Entry(self.ledger_frame)
        opening_balance_input.grid(row=8,column=2)

    def bills_payable_form(self):
        credit_period_label = tk.Label(self.ledger_frame,text='Credit Period')
        credit_period_label.grid(row=5,column=1)
        credit_period_input = tk.Entry(self.ledger_frame)
        credit_period_input.grid(row=5,column=2)

        contact_number_label = tk.Label(self.ledger_frame,text='Contact Number')
        contact_number_label.grid(row=6,column=1)
        contact_number_input = tk.Entry(self.ledger_frame)
        contact_number_input.grid(row=6,column=2)

        address_label = tk.Label(self.ledger_frame,text='Address')
        address_label.grid(row=7,column=1)
        address_input = tk.Entry(self.ledger_frame)
        address_input.grid(row=7,column=2)

        phone_number_label = tk.Label(self.ledger_frame,text='Phone Number')
        phone_number_label.grid(row=8,column=1)
        phone_number_input = tk.Entry(self.ledger_frame)
        phone_number_input.grid(row=8,column=2)

        pancard_no_label = tk.Label(self.ledger_frame,text='Pancard No')
        pancard_no_label.grid(row=9,column=1)
        pancard_no_input = tk.Entry(self.ledger_frame)
        pancard_no_input.grid(row=9,column=2)

        opening_balance_label = tk.Label(self.ledger_frame,text='Opening Balance')
        opening_balance_label.grid(row=10,column=1)
        opening_balance_input = tk.Entry(self.ledger_frame)
        opening_balance_input.grid(row=10,column=2)
        
    def sundry_creditors_form(self):
        credit_period_label = tk.Label(self.ledger_frame,text='Credit Label')
        credit_period_label.grid(row=5,column=1)
        credit_period_input = tk.Entry(self.ledger_frame)
        credit_period_input.grid(row=5,column=2)
        
        contact_number_label = tk.Label(self.ledger_frame,text='Contact Number')
        contact_number_label.grid(row=6,column=1)
        contact_number_input = tk.Entry(self.ledger_frame)
        contact_number_input.grid(row=6,column=2)

        address_label = tk.Label(self.ledger_frame,text='Address')
        address_label.grid(row=6,column=1)
        address_input = tk.Entry(self.ledger_frame)
        address_input.grid(row=6,column=2)

        phone_number_label = tk.Label(self.ledger_frame,text='Phone Number')
        phone_number_label.grid(row=7,column=1)
        phone_number_input = tk.Entry(self.ledger_frame)
        phone_number_input.grid(row=7,column=2)

        pancard_no_label = tk.Label(self.ledger_frame,text='Pancard No')
        pancard_no_label.grid(row=8,column=1)
        pancard_no_input = tk.Entry(self.ledger_frame)
        pancard_no_input.grid(row=8,column=2)

        opening_balance_label = tk.Label(self.ledger_frame,text='Opening Balance')
        opening_balance_label.grid(row=9,column=1)
        opening_balance_input = tk.Entry(self.ledger_frame)
        opening_balance_input.grid(row=9,column=2)

    #Expense Group
    def expense_group_form(self):
        gst_percentage_label = tk.Label(self.ledger_frame,text='GST %')
        gst_percentage_label.grid(row=5,column=1)
        gst_percentage_input = tk.Entry(self.ledger_frame)
        gst_percentage_input.grid(row=5,column=2)

        opening_balance_label = tk.Label(self.ledger_frame,text='Opening Balance')
        opening_balance_label.grid(row=6,column=1)
        opening_balance_input = tk.Entry(self.ledger_frame)
        opening_balance_input.grid(row=6,column=2)

    #Income Group
    def income_group_form(self):
        gst_percentage_label = tk.Label(self.ledger_frame,text='GST %')
        gst_percentage_label.grid(row=5,column=1)
        gst_percentage_input = tk.Entry(self.ledger_frame)
        gst_percentage_input.grid(row=5,column=2)

        opening_balance_label = tk.Label(self.ledger_frame,text='Opening Balance')
        opening_balance_label.grid(row=6,column=1)
        opening_balance_input = tk.Entry(self.ledger_frame)
        opening_balance_input.grid(row=6,column=2)





