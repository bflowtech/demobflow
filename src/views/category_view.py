import tkinter as tk
from tkinter import ttk
from src.views.base_view import BaseView

class CategoryView(BaseView):
    def __init__(self,category_controller):
        super(CategoryView, self).__init__()
        self.title('Category')
        self.controller = category_controller
        self.category_frame = tk.Frame(self)
        self.set_layout()

    def set_layout(self):
        self.category_label = tk.Label(self.category_frame, text='Category Management', font='50')
        self.category_label.grid(row=0,column=2)

        category_label = tk.Label(self.category_frame, text='Category Name')
        category_label.grid(row=1,column=1)
        self.category_input = tk.Entry(self.category_frame)
        self.category_input.grid(row=1, column=2, padx=10, pady=10)
        self.category_add_btn = tk.Button(self.category_frame, text='Add',command=self.controller.add_item)
        self.category_add_btn.grid(row=1, column=3, padx=10, pady=10)


        # Combobox creation
        self.department_name = tk.StringVar()
        self.department_id = tk.IntVar()
        department_label = tk.Label(self.category_frame,text='Select Department')
        department_label.grid(row=2,column=1)
        self.departments_list = ttk.Combobox(self.category_frame, width=10)
        self.departments_list.grid(row=2,column=2)
        self.departments_list.bind('<<ComboboxSelected>>',self.controller.select_department)

        # define Tree Widget
        self.category_list_view = ttk.Treeview(self.category_frame)

        # format columns
        self.category_list_view['columns'] = ('ID', 'Category Name','Department')
        self.category_list_view.column('#0', width=0, stretch='no')
        self.category_list_view.column('ID', anchor='w', width=120)
        self.category_list_view.column('Category Name', anchor='w', width=120)
        self.category_list_view.column('Department', anchor='w', width=120)

        # create Headings
        self.category_list_view.heading('#0', text='', anchor='w')
        self.category_list_view.heading('ID', text='ID', anchor='w')
        self.category_list_view.heading('Category Name', text='Category Name', anchor='w')
        self.category_list_view.heading('Department', text='Department', anchor='w')

        self.category_list_view.grid(row=3, column=2)

        self.category_list_view.bind("<ButtonRelease-1>",self.controller.select_item)

        self.category_update_btn = tk.Button(self.category_frame, text='Update',command=self.controller.update_item)
        self.category_update_btn.grid_forget()

        self.category_edit_btn = tk.Button(self.category_frame, text='Edit',command=self.controller.edit_item)
        self.category_edit_btn.grid(row=3, column=3)

        self.category_delete_btn = tk.Button(self.category_frame, text='Delete',command=self.controller.delete_category)
        self.category_delete_btn.grid(row=4, column=3)

        self.search_box = tk.Entry(self.category_frame)
        self.search_box.grid(row=4,column=2)
        self.search_box.bind('<KeyRelease>',self.controller.serach_category)

        self.category_frame.pack(pady=10)
