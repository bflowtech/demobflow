import tkinter as tk
from tkinter import ttk
from src.views.base_view import BaseView

class StocksReportView(BaseView):
    def __init__(self,stocks_reports_controller):
        super(StocksReportView,self).__init__()
        self.title('Stocks Report View')
        self.report_frame = tk.Frame(self)
        self.controller = stocks_reports_controller
        self.report_frame.pack()

        self.set_layout()

    def set_layout(self):
        stocks_report_label = tk.Label(self.report_frame,text='Stocks Report')
        stocks_report_label.pack()


        search_stock_label = tk.LabelFrame(self.report_frame,text='Search Item')
        search_stock_label.pack()

        print_btn = tk.Button(self.report_frame,text='Print Report',command=self.controller.generate_report)
        print_btn.pack(side='right')

        

        self.category_dropdown = ttk.Combobox(search_stock_label, width=20)
        self.category_dropdown.grid(row=0,column=0,padx=10,pady=10)
        self.category_dropdown['values'] = ('Brands','Department','Category','Sub-Category','Item Wise','BatchWise')
        self.category_dropdown.bind("<<ComboboxSelected>>",self.controller.category_selected)

        # self.seconddropdown = ttk.Combobox(search_stock_label, width=20)
        # self.seconddropdown['values'] = ('Brands','Department','Category','Sub-Category')
        # self.seconddropdown.grid(row=0,column=1,padx=10,pady=10)
        # self.seconddropdown.bind("<<ComboboxSelected>>",self.controller.new_category_selected)

        self.search_stock_entry = tk.Entry(search_stock_label)
        self.search_stock_entry.grid(row=0,column=2,padx=10,pady=10)
        self.search_stock_entry.bind('<KeyRelease>', self.controller.search_stocks)



        # define Tree Widget
        style = ttk.Style()
        style.theme_use('default')
        style.configure('Treeview',
        background='#D3D3D3',
        foreground='black',
        rowheight=25,
        fieldbackground='#D3D3D3'
        )

        style.map('Treeview',
        background=[('selected','#347083')])

        tree_scroll = tk.Scrollbar(self.report_frame)
        tree_scroll.pack(side='right',fill='y')

        self.stock_report_view = ttk.Treeview(self.report_frame,yscrollcommand=tree_scroll.set,selectmode='extended')
        self.stock_report_view.pack()

        tree_scroll.config(command=self.stock_report_view.yview)

        self.stock_report_view['columns'] = ('Sl.No','Name','Department','Opening Stock','Inward Quantity','Purchase Price',
                                                'Outward Quantity','Selling Price','Margin Amount','Margin %','Closing Stock','Closing Value')

        
        self.stock_report_view.column('#0', width=0, stretch='no')
        self.stock_report_view.column('Sl.No',anchor='w',width=30)
        self.stock_report_view.column('Name',anchor='w',width=150)
        self.stock_report_view.column('Department',anchor='w',width=100)
        self.stock_report_view.column('Opening Stock',anchor='center',width=100)
        self.stock_report_view.column('Inward Quantity',anchor='center',width=100)
        self.stock_report_view.column('Purchase Price',anchor='center',width=100)
        self.stock_report_view.column('Outward Quantity',anchor='center',width=100)
        self.stock_report_view.column('Selling Price',anchor='center',width=100)
        self.stock_report_view.column('Margin Amount',anchor='center',width=100)
        self.stock_report_view.column('Margin %',anchor='center',width=100)
        self.stock_report_view.column('Closing Stock',anchor='center',width=100)
        self.stock_report_view.column('Closing Value',anchor='center',width=100,stretch='yes')

        self.stock_report_view.heading('#0', text='', anchor='w')
        self.stock_report_view.heading('Sl.No',text="Sl.No",anchor='w')
        self.stock_report_view.heading('Name',text='Item Name',anchor='w')
        self.stock_report_view.heading('Department',text='Department',anchor='center')
        self.stock_report_view.heading('Opening Stock',text="Opening Stock",anchor='center')
        self.stock_report_view.heading('Inward Quantity',text="Inward Quantity",anchor='center')
        self.stock_report_view.heading('Purchase Price',text="Cost Price",anchor='center')
        self.stock_report_view.heading('Outward Quantity',text="Outward Quantity",anchor='center')
        self.stock_report_view.heading('Selling Price',text="Selling Price",anchor='center')
        self.stock_report_view.heading('Margin Amount',text="Margin Amount",anchor='center')
        self.stock_report_view.heading('Margin %',text="Margin %",anchor='center')
        self.stock_report_view.heading('Closing Stock',text='Closing Stock',anchor='w')
        self.stock_report_view.heading('Closing Value',text="Closing Value",anchor='w')


        self.data = []


        self.stock_report_view.tag_configure('oddrow', background='white')
        self.stock_report_view.tag_configure('evenrow', background='lightblue')

