import tkinter as tk
from tkinter import ttk
from src.views.base_view import BaseView

class UnitsView(BaseView):
    def __init__(self,units_controller):
        super(UnitsView, self).__init__()
        self.title('Units')
        self.unit_frame = tk.Frame(self)
        self.units_controller = units_controller
        self.set_layout()


    def set_layout(self):
        self.units_label = tk.Label(self.unit_frame, text='Units', font='50')
        self.units_label.grid(row=0, column=2)

        unit_label = tk.Label(self.unit_frame,text='Unit Name')
        unit_label.grid(row=1,column=1)

        self.units_input = tk.Entry(self.unit_frame)
        self.units_input.grid(row=1, column=2, padx=10, pady=10)
        
        decimal_label = tk.Label(self.unit_frame,text='No:Of Decimal')
        decimal_label.grid(row=2,column=1)

        self.decimal_input = tk.Entry(self.unit_frame)
        self.decimal_input.grid(row=2, column=2, padx=10, pady=10)

        self.units_add_btn = tk.Button(self.unit_frame, text='Add',command=self.units_controller.add_item)
        self.units_add_btn.grid(row=2, column=3, padx=10, pady=10)

        # define Tree Widget
        self.units_list_view = ttk.Treeview(self.unit_frame)

        # format columns
        self.units_list_view['columns'] = ('ID', 'Unit Name', 'No:Decimal')
        self.units_list_view.column('#0', width=0, stretch='no')
        self.units_list_view.column('ID', anchor='w', width=120)
        self.units_list_view.column('Unit Name', anchor='w', width=120)
        self.units_list_view.column('No:Decimal', anchor='w', width=120)

        # create Headings
        self.units_list_view.heading('#0', text='', anchor='w')
        self.units_list_view.heading('ID', text='ID', anchor='w')
        self.units_list_view.heading('Unit Name', text='Unit Name', anchor='w')
        self.units_list_view.heading('No:Decimal', text='No:Decimal', anchor='w')


        self.units_list_view.grid(row=3, column=2)

        self.units_list_view.bind("<ButtonRelease-1>", self.units_controller.select_item)

        self.units_update_btn = tk.Button(self.unit_frame, text='Update',command=self.units_controller.update_item)
        self.units_update_btn.grid_forget()

        self.units_edit_btn = tk.Button(self.unit_frame, text='Edit',command=self.units_controller.edit_item)
        self.units_edit_btn.grid(row=3, column=3)

        self.units_delete_btn = tk.Button(self.unit_frame, text='Delete',command=self.units_controller.delete_item)
        self.units_delete_btn.grid(row=4, column=3)
        
        self.unit_frame.pack(pady=10)
