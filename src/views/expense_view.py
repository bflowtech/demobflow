from src.views.base_view import BaseView
import tkinter as tk
from tkinter import ttk


class ExpenseView(BaseView):
    def __init__(self, expense_controller):
        super(ExpenseView, self).__init__()
        self.title('Expense Group')
        self.expense_frame = tk.Frame(self)
        self.expense_controller = expense_controller
        self.set_layout()

    def set_layout(self):
        expense_label = tk.Label(self.expense_frame, text='Expense Groups')
        expense_label.grid(row=1,column=2)

        self.expense_group_input = tk.Entry(self.expense_frame)
        self.expense_group_input.grid(row=3,column=1)

        self.expense_group_add = tk.Button(self.expense_frame, text='Add',command=self.expense_controller.add_item)
        self.expense_group_add.grid(row=3, column=2, padx=10, pady=10)

        self.expense_group_view = ttk.Treeview(self.expense_frame,height=10)
        self.expense_group_view["columns"] = ("id", "Name")
        
        self.expense_group_view.column('#0', width=0, stretch='no')
        self.expense_group_view.column("id", width = 100, anchor ='w')
        self.expense_group_view.column("Name", width = 200, anchor ='w')

        self.expense_group_view.heading('#0', text='', anchor='e')
        self.expense_group_view.heading("id", text ="ID")
        self.expense_group_view.heading("Name", text ="Name")
        self.expense_group_view.grid(row=4,column=1)
        

        self.expense_group_view.bind("<ButtonRelease-1>", self.expense_controller.select_item)


        self.expense_group_update_btn = tk.Button(self.expense_frame, text='Update',command=self.expense_controller.update_item)
        self.expense_group_update_btn.grid_forget()

        self.expense_group_edit_btn = tk.Button(self.expense_frame, text='Edit',command=self.expense_controller.edit_item)
        self.expense_group_edit_btn.grid(row=4, column=2)

        self.expense_group_delete_btn = tk.Button(self.expense_frame, text='Delete',command=self.expense_controller.delete_item)
        self.expense_group_delete_btn.grid(row=5, column=2)

        self.ledger_popupBtn = tk.Button(self.expense_frame, text='Ledger Management',command=self.expense_ledger_popup)
        self.ledger_popupBtn.grid(row=6, column=1)

        self.expense_frame.pack(pady=10)


    def expense_ledger_popup(self):
        self.expense_ledger_frame = tk.Toplevel(self.expense_frame)
        self.expense_ledger_frame.geometry('600x400')
        self.expense_ledger_frame.wm_title('Liablity Ledger Management')
        
        selected = self.expense_group_view.focus()
        group_id,group_name = self.expense_group_view.item(selected, 'values')

        self.expense_ledger_form()

        if 'Purchase Return' == group_name:
            self.type_input.configure(state='normal')
            self.type_input.delete(0, 'end')
            self.type_input.insert(0, 'Credit')
            self.type_input.configure(state='readonly')

    def expense_ledger_form(self):
        ledger_name_label = tk.Label(self.expense_ledger_frame,text='Ledger Name')
        ledger_name_label.grid(row=3,column=1)
        self.ledger_name_input = tk.Entry(self.expense_ledger_frame)
        self.ledger_name_input.grid(row=3,column=2)

        
        type_label = tk.Label(self.expense_ledger_frame,text='Type')
        type_label.grid(row=3,column=3)
        self.type_input = tk.Entry(self.expense_ledger_frame)
        self.type_input.insert('end', 'Debit')
        self.type_input.configure(state='disabled')
        self.type_input.grid(row=3,column=4)


        gst_percentage_label = tk.Label(self.expense_ledger_frame,text='Gst %')
        gst_percentage_label.grid(row=4,column=1)
        self.gst_percentage_input = tk.Entry(self.expense_ledger_frame)
        self.gst_percentage_input.grid(row=4,column=2)

        opening_balance_label = tk.Label(self.expense_ledger_frame,text='Opening Balance')
        opening_balance_label.grid(row=5,column=1)
        self.opening_balance_input = tk.Entry(self.expense_ledger_frame)
        self.opening_balance_input.grid(row=5,column=2)

        self.ledger_add_btn = tk.Button(self.expense_ledger_frame,text='ADD',command=self.expense_controller.add_to_ledger)
        self.ledger_add_btn.grid(row=6,column=2)