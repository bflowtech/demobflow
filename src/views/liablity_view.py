from src.views.base_view import BaseView
import tkinter as tk
from tkinter import ttk


class LiablityView(BaseView):
    def __init__(self, liablity_controller):
        super(LiablityView, self).__init__()
        self.title('Liablity Group')
        self.liablity_frame = tk.Frame(self)
        self.liablity_controller = liablity_controller
        self.set_layout()


    def set_layout(self):
        liablity_label = tk.Label(self.liablity_frame, text='Liablity Groups')
        liablity_label.grid(row=1,column=2)

        self.liablity_group_input = tk.Entry(self.liablity_frame)
        self.liablity_group_input.grid(row=3,column=1)

        self.liablity_group_add = tk.Button(self.liablity_frame, text='Add',command=self.liablity_controller.add_item)
        self.liablity_group_add.grid(row=3, column=2, padx=10, pady=10)

        self.liablity_group_view = ttk.Treeview(self.liablity_frame,height=10)
        self.liablity_group_view["columns"] = ("id", "Name")
        
        self.liablity_group_view.column('#0', width=0, stretch='no')
        self.liablity_group_view.column("id", width = 100, anchor ='w')
        self.liablity_group_view.column("Name", width = 200, anchor ='w')

        self.liablity_group_view.heading('#0', text='', anchor='e')
        self.liablity_group_view.heading("id", text ="ID")
        self.liablity_group_view.heading("Name", text ="Name")
        self.liablity_group_view.grid(row=4,column=1)
        

        self.liablity_group_view.bind("<ButtonRelease-1>", self.liablity_controller.select_item)


        self.liablity_group_update_btn = tk.Button(self.liablity_frame, text='Update',command=self.liablity_controller.update_item)
        self.liablity_group_update_btn.grid_forget()

        self.liablity_group_edit_btn = tk.Button(self.liablity_frame, text='Edit',command=self.liablity_controller.edit_item)
        self.liablity_group_edit_btn.grid(row=4, column=2)

        self.liablity_group_delete_btn = tk.Button(self.liablity_frame, text='Delete',command=self.liablity_controller.delete_item)
        self.liablity_group_delete_btn.grid(row=5, column=2)

        
        self.ledger_popupBtn = tk.Button(self.liablity_frame, text='Ledger Management',command=self.liablity_ledger_popup)
        self.ledger_popupBtn.grid(row=6, column=1)

        self.liablity_frame.pack(pady=10)


    def liablity_ledger_popup(self):
        self.liablity_ledger_frame = tk.Toplevel(self.liablity_frame)
        self.liablity_ledger_frame.geometry('600x400')
        self.liablity_ledger_frame.wm_title('Liablity Ledger Management')
        
        selected = self.liablity_group_view.focus()
        group_id,group_name = self.liablity_group_view.item(selected, 'values')

        if 'Outstanding Expenses' == group_name:
            self.outstanding_exp_form()
        elif 'Duties And Taxes' == group_name:
            self.duties_and_taxes_form()
        elif 'Capital' == group_name:
            self.capital_form()
        elif 'Drawings' == group_name:
            self.drawing_form()
        elif 'Bank Loans' == group_name:
            self.bank_loans_form()
        elif 'Loans From Financers' == group_name:
            self.loans_from_financers_form()
        elif 'Bills Payable' == group_name:
            self.bills_payable_form()
        elif 'Sundry Creditors' == group_name:
            self.sundry_creditors_form()

    def outstanding_exp_form(self):
        ledger_name_label = tk.Label(self.liablity_ledger_frame,text='Ledger Name')
        ledger_name_label.grid(row=3,column=1)
        self.ledger_name_input = tk.Entry(self.liablity_ledger_frame)
        self.ledger_name_input.grid(row=3,column=2)

        type_label = tk.Label(self.liablity_ledger_frame,text='Type')
        type_label.grid(row=3,column=3)
        self.type_input = tk.Entry(self.liablity_ledger_frame)
        self.type_input.insert('end', 'Credit')
        self.type_input.configure(state='disabled')
        self.type_input.grid(row=3,column=4)

        contact_number_label = tk.Label(self.liablity_ledger_frame,text='Contact Number')
        contact_number_label.grid(row=4,column=1)
        self.contact_details_input = tk.Entry(self.liablity_ledger_frame)
        self.contact_details_input.grid(row=4,column=2)

        address_label = tk.Label(self.liablity_ledger_frame,text='Address')
        address_label.grid(row=5,column=1)
        self.address_details_input = tk.Entry(self.liablity_ledger_frame)
        self.address_details_input.grid(row=5,column=2)

        phone_number_label = tk.Label(self.liablity_ledger_frame,text='Phone Number')
        phone_number_label.grid(row=6,column=1)
        self.phone_number_input = tk.Entry(self.liablity_ledger_frame)
        self.phone_number_input.grid(row=6,column=2)

        opening_balance_label = tk.Label(self.liablity_ledger_frame,text='Opening Balance')
        opening_balance_label.grid(row=9,column=1)
        self.opening_balance_input = tk.Entry(self.liablity_ledger_frame)
        self.opening_balance_input.grid(row=9,column=2)

        self.ledger_add_btn = tk.Button(self.liablity_ledger_frame,text='ADD',command=self.liablity_controller.add_to_ledger)
        self.ledger_add_btn.grid(row=10,column=2)

    def duties_and_taxes_form(self):
        ledger_name_label = tk.Label(self.liablity_ledger_frame,text='Ledger Name')
        ledger_name_label.grid(row=3,column=1)
        self.ledger_name_input = tk.Entry(self.liablity_ledger_frame)
        self.ledger_name_input.grid(row=3,column=2)
        
        type_label = tk.Label(self.liablity_ledger_frame,text='Type')
        type_label.grid(row=3,column=3)
        self.type_input = tk.Entry(self.liablity_ledger_frame)
        self.type_input.insert('end', 'Credit')
        self.type_input.configure(state='disabled')
        self.type_input.grid(row=3,column=4)

        contact_number_label = tk.Label(self.liablity_ledger_frame,text='Contact Number')
        contact_number_label.grid(row=4,column=1)
        self.contact_details_input = tk.Entry(self.liablity_ledger_frame)
        self.contact_details_input.grid(row=4,column=2)

        phone_number_label = tk.Label(self.liablity_ledger_frame,text='Phone Number')
        phone_number_label.grid(row=5,column=1)
        self.phone_number_input = tk.Entry(self.liablity_ledger_frame)
        self.phone_number_input.grid(row=5,column=2)


        duites_label = tk.Label(self.liablity_ledger_frame,text='Types Of Duties')
        duites_label.grid(row=6,column=1)

        types_of_duties = [
            'TDS',
            'GST',
            'TCS',
            'CESS',
        ]

        self.duties_clicked = tk.StringVar()
        self.duties_clicked.set(types_of_duties[0])
        self.duties_options = ttk.Combobox(self.liablity_ledger_frame, values=types_of_duties, textvariable=self.duties_clicked)
        self.duties_options.grid(row=6,column=2)


        tax_type_label = tk.Label(self.liablity_ledger_frame,text='Tax Type')
        tax_type_label.grid(row=7,column=1)

        types_of_taxes = [
            'IGST',
            'CGST',
            'SGST',
            'UTGST',
        ]

        self.tax_clicked = tk.StringVar()
        self.tax_clicked.set(types_of_taxes[0])
        self.tax_options = ttk.Combobox(self.liablity_ledger_frame, values=types_of_taxes, textvariable=self.tax_clicked)
        self.tax_options.grid(row=7,column=2)
        self.tax_clicked.trace('w', self.liablity_controller.tax_onchange)



        percentage_tax_label = tk.Label(self.liablity_ledger_frame,text='Percentage Of Tax')
        percentage_tax_label.grid(row=8 ,column=1)
        self.percentage_tax_input = tk.Entry(self.liablity_ledger_frame)
        self.percentage_tax_input.grid(row=8, column=2)

        opening_balance_label = tk.Label(self.liablity_ledger_frame,text='Opening Balance')
        opening_balance_label.grid(row=9,column=1)
        self.opening_balance_input = tk.Entry(self.liablity_ledger_frame)
        self.opening_balance_input.grid(row=9,column=2)
        
        self.ledger_add_btn = tk.Button(self.liablity_ledger_frame,text='ADD',command=self.liablity_controller.add_to_ledger)
        self.ledger_add_btn.grid(row=10,column=2)

    def capital_form(self):
        ledger_name_label = tk.Label(self.liablity_ledger_frame,text='Ledger Name')
        ledger_name_label.grid(row=3,column=1)
        self.ledger_name_input = tk.Entry(self.liablity_ledger_frame)
        self.ledger_name_input.grid(row=3,column=2)

        
        type_label = tk.Label(self.liablity_ledger_frame,text='Type')
        type_label.grid(row=3,column=3)
        self.type_input = tk.Entry(self.liablity_ledger_frame)
        self.type_input.insert('end', 'Credit')
        self.type_input.configure(state='disabled')
        self.type_input.grid(row=3,column=4)

        contact_number_label = tk.Label(self.liablity_ledger_frame,text='Contact Number')
        contact_number_label.grid(row=4,column=1)
        self.contact_details_input = tk.Entry(self.liablity_ledger_frame)
        self.contact_details_input.grid(row=4,column=2)

        address_label = tk.Label(self.liablity_ledger_frame,text='Address')
        address_label.grid(row=5,column=1)
        self.address_details_input = tk.Entry(self.liablity_ledger_frame)
        self.address_details_input.grid(row=5,column=2)

        phone_number_label = tk.Label(self.liablity_ledger_frame,text='Phone Number')
        phone_number_label.grid(row=6,column=1)
        self.phone_number_input = tk.Entry(self.liablity_ledger_frame)
        self.phone_number_input.grid(row=6,column=2)


        opening_balance = tk.Label(self.liablity_ledger_frame,text='Opening Balance')
        opening_balance.grid(row=9,column=1)
        self.opening_balance_input = tk.Entry(self.liablity_ledger_frame)
        self.opening_balance_input.grid(row=9,column=2)

        self.ledger_add_btn = tk.Button(self.liablity_ledger_frame,text='ADD',command=self.liablity_controller.add_to_ledger)
        self.ledger_add_btn.grid(row=10,column=2)

    def drawing_form(self):
        ledger_name_label = tk.Label(self.liablity_ledger_frame,text='Ledger Name')
        ledger_name_label.grid(row=3,column=1)
        self.ledger_name_input = tk.Entry(self.liablity_ledger_frame)
        self.ledger_name_input.grid(row=3,column=2)

        type_label = tk.Label(self.liablity_ledger_frame,text='Type')
        type_label.grid(row=3,column=3)
        self.type_input = tk.Entry(self.liablity_ledger_frame)
        self.type_input.insert('end', 'Debit')
        self.type_input.configure(state='disabled')
        self.type_input.grid(row=3,column=4)
        
        contact_number_label = tk.Label(self.liablity_ledger_frame,text='Contact Number')
        contact_number_label.grid(row=4,column=1)
        contact_details_input = tk.Entry(self.liablity_ledger_frame)
        contact_details_input.grid(row=4,column=2)

        address_label = tk.Label(self.liablity_ledger_frame,text='Address')
        address_label.grid(row=5,column=1)
        address_details_input = tk.Entry(self.liablity_ledger_frame)
        address_details_input.grid(row=5,column=2)

        phone_number_label = tk.Label(self.liablity_ledger_frame,text='Phone Number')
        phone_number_label.grid(row=6,column=1)
        self.phone_number_input = tk.Entry(self.liablity_ledger_frame)
        self.phone_number_input.grid(row=6,column=2)

        gst_label = tk.Label(self.liablity_ledger_frame,text='GST Type')
        gst_label.grid(row=7,column=1)
        
        types_of_gst = [
            'Regular',
            'Composition',
            'Unregistered',
            'Customer'
            ]

        self.gst_clicked = tk.StringVar()
        self.gst_clicked.set(types_of_gst[0])
        self.gst_options = ttk.Combobox(self.liablity_ledger_frame, values=types_of_gst, textvariable=self.gst_clicked)
        self.gst_options.grid(row=7,column=2)
        self.gst_clicked.trace('w', self.liablity_controller.gst_onchange)


        gst_no_label = tk.Label(self.liablity_ledger_frame, text='Gst No')
        gst_no_label.grid(row=8,column=1)
        self.gst_no_input = tk.Entry(self.liablity_ledger_frame)
        self.gst_no_input.grid(row=8,column=2)

        opening_balance_label = tk.Label(self.liablity_ledger_frame,text='Opening Balance')
        opening_balance_label.grid(row=9,column=1)
        self.opening_balance_input = tk.Entry(self.liablity_ledger_frame)
        self.opening_balance_input.grid(row=9,column=2)

        self.ledger_add_btn = tk.Button(self.liablity_ledger_frame,text='ADD',command=self.liablity_controller.add_to_ledger)
        self.ledger_add_btn.grid(row=10,column=2)

    def bank_loans_form(self):
        ledger_name_label = tk.Label(self.liablity_ledger_frame,text='Ledger Name')
        ledger_name_label.grid(row=3,column=1)
        self.ledger_name_input = tk.Entry(self.liablity_ledger_frame)
        self.ledger_name_input.grid(row=3,column=2)

        
        type_label = tk.Label(self.liablity_ledger_frame,text='Type')
        type_label.grid(row=3,column=3)
        self.type_input = tk.Entry(self.liablity_ledger_frame)
        self.type_input.insert('end', 'Credit')
        self.type_input.configure(state='disabled')
        self.type_input.grid(row=3,column=4)

        contact_number_label = tk.Label(self.liablity_ledger_frame,text='Contact Number')
        contact_number_label.grid(row=4,column=1)
        self.contact_details_input = tk.Entry(self.liablity_ledger_frame)
        self.contact_details_input.grid(row=4,column=2)

        address_label = tk.Label(self.liablity_ledger_frame,text='Address')
        address_label.grid(row=5,column=1)
        self.address_details_input = tk.Entry(self.liablity_ledger_frame)
        self.address_details_input.grid(row=5,column=2)

        phone_number_label = tk.Label(self.liablity_ledger_frame,text='Phone Number')
        phone_number_label.grid(row=6,column=1)
        self.phone_number_input = tk.Entry(self.liablity_ledger_frame)
        self.phone_number_input.grid(row=6,column=2)

        gst_label = tk.Label(self.liablity_ledger_frame,text='GST Type')
        gst_label.grid(row=7,column=1)
        
        types_of_gst = [
            'Regular',
            'Composition',
            'Unregistered',
            'Customer'
            ]

        self.gst_clicked = tk.StringVar()
        self.gst_clicked.set(types_of_gst[0])
        self.gst_options = ttk.Combobox(self.liablity_ledger_frame, values=types_of_gst, textvariable=self.gst_clicked)
        self.gst_options.grid(row=7,column=2)
        self.gst_clicked.trace('w', self.liablity_controller.gst_onchange)


        gst_no_label = tk.Label(self.liablity_ledger_frame, text='Gst No')
        gst_no_label.grid(row=8,column=1)
        self.gst_no_input = tk.Entry(self.liablity_ledger_frame)
        self.gst_no_input.grid(row=8,column=2)

        bank_name = tk.Label(self.liablity_ledger_frame,text='Bank Name')
        bank_name.grid(row=9,column=1)
        self.bank_input = tk.Entry(self.liablity_ledger_frame)
        self.bank_input.grid(row=9,column=2)

        bank_address = tk.Label(self.liablity_ledger_frame, text='Bank Address')
        bank_address.grid(row=10,column=1)
        self.bank_address_input = tk.Entry(self.liablity_ledger_frame)
        self.bank_address_input.grid(row=10,column=2)

        account_no = tk.Label(self.liablity_ledger_frame, text='Bank A/C No')
        account_no.grid(row=11,column=1)
        self.account_no_input = tk.Entry(self.liablity_ledger_frame)
        self.account_no_input.grid(row=11,column=2)

        ifsc_code = tk.Label(self.liablity_ledger_frame, text='IFSC CODE')
        ifsc_code.grid(row=12,column=1)
        self.ifsc_code_input = tk.Entry(self.liablity_ledger_frame)
        self.ifsc_code_input.grid(row=12,column=2)

        branch_no = tk.Label(self.liablity_ledger_frame, text='Branch No')
        branch_no.grid(row=13,column=1)
        self.branch_no_input = tk.Entry(self.liablity_ledger_frame)
        self.branch_no_input.grid(row=13,column=2)

        branch_code = tk.Label(self.liablity_ledger_frame,text='Branch Code')
        branch_code.grid(row=14,column=1)
        self.branch_code_input = tk.Entry(self.liablity_ledger_frame)
        self.branch_code_input.grid(row=14,column=2)

        bank_contact_label = tk.Label(self.liablity_ledger_frame, text='Bank Contact No')
        bank_contact_label.grid(row=15,column=1)
        self.bank_contact_input = tk.Entry(self.liablity_ledger_frame)
        self.bank_contact_input.grid(row=15,column=2)

        opening_balance = tk.Label(self.liablity_ledger_frame,text='Opening Balance')
        opening_balance.grid(row=16,column=1)
        self.opening_balance_input = tk.Entry(self.liablity_ledger_frame)
        self.opening_balance_input.grid(row=16,column=2)

        self.ledger_add_btn = tk.Button(self.liablity_ledger_frame,text='ADD',command=self.liablity_controller.add_to_ledger)
        self.ledger_add_btn.grid(row=17,column=2)
        
    def loans_from_financers_form(self):
        ledger_name_label = tk.Label(self.liablity_ledger_frame,text='Ledger Name')
        ledger_name_label.grid(row=3,column=1)
        self.ledger_name_input = tk.Entry(self.liablity_ledger_frame)
        self.ledger_name_input.grid(row=3,column=2)

        type_label = tk.Label(self.liablity_ledger_frame,text='Type')
        type_label.grid(row=3,column=3)
        self.type_input = tk.Entry(self.liablity_ledger_frame)
        self.type_input.insert('end', 'Credit')
        self.type_input.configure(state='disabled')
        self.type_input.grid(row=3,column=4)

        contact_number_label = tk.Label(self.liablity_ledger_frame,text='Contact Number')
        contact_number_label.grid(row=4,column=1)
        self.contact_details_input = tk.Entry(self.liablity_ledger_frame)
        self.contact_details_input.grid(row=4,column=2)

        address_label = tk.Label(self.liablity_ledger_frame,text='Address')
        address_label.grid(row=5,column=1)
        self.address_details_input = tk.Entry(self.liablity_ledger_frame)
        self.address_details_input.grid(row=5,column=2)

        phone_number_label = tk.Label(self.liablity_ledger_frame,text='Phone No')
        phone_number_label.grid(row=6,column=1)
        self.phone_number_input = tk.Entry(self.liablity_ledger_frame)
        self.phone_number_input.grid(row=6,column=2)

        gst_label = tk.Label(self.liablity_ledger_frame,text='GST Type')
        gst_label.grid(row=7,column=1)
        
        types_of_gst = [
            'Regular',
            'Composition',
            'Unregistered',
            'Customer'
            ]

        self.gst_clicked = tk.StringVar()
        self.gst_clicked.set(types_of_gst[0])
        self.gst_options = ttk.Combobox(self.liablity_ledger_frame, values=types_of_gst, textvariable=self.gst_clicked)
        self.gst_options.grid(row=7,column=2)
        self.gst_clicked.trace('w', self.liablity_controller.gst_onchange)


        gst_no_label = tk.Label(self.liablity_ledger_frame, text='Gst No')
        gst_no_label.grid(row=8,column=1)
        self.gst_no_input = tk.Entry(self.liablity_ledger_frame)
        self.gst_no_input.grid(row=8,column=2)

        opening_balance_label = tk.Label(self.liablity_ledger_frame,text='Opening Balance')
        opening_balance_label.grid(row=9,column=1)
        self.opening_balance_input = tk.Entry(self.liablity_ledger_frame)
        self.opening_balance_input.grid(row=9,column=2)
        
        self.ledger_add_btn = tk.Button(self.liablity_ledger_frame,text='ADD',command=self.liablity_controller.add_to_ledger)
        self.ledger_add_btn.grid(row=10,column=2)

    def bills_payable_form(self):
        ledger_name_label = tk.Label(self.liablity_ledger_frame,text='Ledger Name')
        ledger_name_label.grid(row=3,column=1)
        self.ledger_name_input = tk.Entry(self.liablity_ledger_frame)
        self.ledger_name_input.grid(row=3,column=2)

        type_label = tk.Label(self.liablity_ledger_frame,text='Type')
        type_label.grid(row=3,column=3)
        self.type_input = tk.Entry(self.liablity_ledger_frame)
        self.type_input.insert('end', 'Credit')
        self.type_input.configure(state='disabled')
        self.type_input.grid(row=3,column=4)

        credit_period = tk.Label(self.liablity_ledger_frame,text='Credit Period')
        credit_period.grid(row=4,column=1)
        self.credit_period_input = tk.Entry(self.liablity_ledger_frame)
        self.credit_period_input.grid(row=4,column=2)

        contact_number = tk.Label(self.liablity_ledger_frame,text='Contact Number')
        contact_number.grid(row=5,column=1)
        self.contact_number_input = tk.Entry(self.liablity_ledger_frame)
        self.contact_number_input.grid(row=5,column=2)

        address_label = tk.Label(self.liablity_ledger_frame,text='Address')
        address_label.grid(row=6,column=1)
        self.address_details_input = tk.Entry(self.liablity_ledger_frame)
        self.address_details_input.grid(row=6,column=2)

        phone_number_label = tk.Label(self.liablity_ledger_frame,text='Phone No')
        phone_number_label.grid(row=7,column=1)
        self.phone_number_input = tk.Entry(self.liablity_ledger_frame)
        self.phone_number_input.grid(row=7,column=2)

        pancard_no_label = tk.Label(self.liablity_ledger_frame,text='Pancard No')
        pancard_no_label.grid(row=8,column=1)
        self.pancard_no_input = tk.Entry(self.liablity_ledger_frame)
        self.pancard_no_input.grid(row=8,column=2)


        gst_label = tk.Label(self.liablity_ledger_frame,text='GST Type')
        gst_label.grid(row=9,column=1)
        
        types_of_gst = [
            'Regular',
            'Composition',
            'Unregistered',
            'Customer'
            ]

        self.gst_clicked = tk.StringVar()
        self.gst_clicked.set(types_of_gst[0])
        self.gst_options = ttk.Combobox(self.liablity_ledger_frame, values=types_of_gst, textvariable=self.gst_clicked)
        self.gst_options.grid(row=9,column=2)
        self.gst_clicked.trace('w', self.liablity_controller.gst_onchange)


        gst_no_label = tk.Label(self.liablity_ledger_frame, text='Gst No')
        gst_no_label.grid(row=10,column=1)
        self.gst_no_input = tk.Entry(self.liablity_ledger_frame)
        self.gst_no_input.grid(row=10,column=2)

        opening_balance_label = tk.Label(self.liablity_ledger_frame,text='Opening Balance')
        opening_balance_label.grid(row=11,column=1)
        self.opening_balance_input = tk.Entry(self.liablity_ledger_frame)
        self.opening_balance_input.grid(row=11,column=2)

        self.ledger_add_btn = tk.Button(self.liablity_ledger_frame,text='ADD',command=self.liablity_controller.add_to_ledger)
        self.ledger_add_btn.grid(row=12,column=2)
        
    def sundry_creditors_form(self):
        ledger_name_label = tk.Label(self.liablity_ledger_frame,text='Ledger Name')
        ledger_name_label.grid(row=3,column=1)
        self.ledger_name_input = tk.Entry(self.liablity_ledger_frame)
        self.ledger_name_input.grid(row=3,column=2)

        type_label = tk.Label(self.liablity_ledger_frame,text='Type')
        type_label.grid(row=3,column=3)
        self.type_input = tk.Entry(self.liablity_ledger_frame)
        self.type_input.insert('end', 'Credit')
        self.type_input.configure(state='disabled')
        self.type_input.grid(row=3,column=4)

        credit_period = tk.Label(self.liablity_ledger_frame,text='Credit Period')
        credit_period.grid(row=4,column=1)
        self.credit_period_input = tk.Entry(self.liablity_ledger_frame)
        self.credit_period_input.grid(row=4,column=2)

        contact_number = tk.Label(self.liablity_ledger_frame,text='Contact Number')
        contact_number.grid(row=5,column=1)
        self.contact_number_input = tk.Entry(self.liablity_ledger_frame)
        self.contact_number_input.grid(row=5,column=2)

        address_label = tk.Label(self.liablity_ledger_frame,text='Address')
        address_label.grid(row=6,column=1)
        self.address_details_input = tk.Entry(self.liablity_ledger_frame)
        self.address_details_input.grid(row=6,column=2)

        phone_number_label = tk.Label(self.liablity_ledger_frame,text='Phone No')
        phone_number_label.grid(row=7,column=1)
        self.phone_number_input = tk.Entry(self.liablity_ledger_frame)
        self.phone_number_input.grid(row=7,column=2)

        pancard_no_label = tk.Label(self.liablity_ledger_frame,text='Pancard No')
        pancard_no_label.grid(row=8,column=1)
        self.pancard_no_input = tk.Entry(self.liablity_ledger_frame)
        self.pancard_no_input.grid(row=8,column=2)


        gst_label = tk.Label(self.liablity_ledger_frame,text='GST Type')
        gst_label.grid(row=9,column=1)
        
        types_of_gst = [
            'Regular',
            'Composition',
            'Unregistered',
            'Customer'
            ]

        self.gst_clicked = tk.StringVar()
        self.gst_clicked.set(types_of_gst[0])
        self.gst_options = ttk.Combobox(self.liablity_ledger_frame, values=types_of_gst, textvariable=self.gst_clicked)
        self.gst_options.grid(row=9,column=2)
        self.gst_clicked.trace('w', self.liablity_controller.gst_onchange)


        gst_no_label = tk.Label(self.liablity_ledger_frame, text='Gst No')
        gst_no_label.grid(row=10,column=1)
        self.gst_no_input = tk.Entry(self.liablity_ledger_frame)
        self.gst_no_input.grid(row=10,column=2)

        opening_balance_label = tk.Label(self.liablity_ledger_frame,text='Opening Balance')
        opening_balance_label.grid(row=11,column=1)
        self.opening_balance_input = tk.Entry(self.liablity_ledger_frame)
        self.opening_balance_input.grid(row=11,column=2)

        self.ledger_add_btn = tk.Button(self.liablity_ledger_frame,text='ADD',command=self.liablity_controller.add_to_ledger)
        self.ledger_add_btn.grid(row=12,column=2)
        