import tkinter as tk
from tkinter import ttk
from src.views.base_view import BaseView
from tkcalendar import DateEntry

class SalesVoucherView(BaseView):
    def __init__(self,sales_voucher_controller):
        super(SalesVoucherView,self).__init__()
        self.title('Sales Voucher View')
        self.sales_voucher_frame = tk.Frame(self)
        self.controller = sales_voucher_controller
        self.sales_voucher_frame.pack()

        self.set_layout()


    def set_layout(self):
        tree_frame = self.sales_voucher_frame

        header_view = tk.LabelFrame(self.sales_voucher_frame,text='Header view')
        header_view.pack(side='top',fill='x')

        lpo_no_label = tk.Label(header_view,text='LPO No')
        lpo_no_label.grid(row=0,column=0,padx=10,pady=10)
        self.lpo_entry = tk.Entry(header_view)
        self.lpo_entry.grid(row=0,column=1,padx=10,pady=10)
        # self.lpo_entry.bind("<KeyRelease>", self.controller.lpo_changed)

        sales_voucher_no = tk.Label(header_view,text='Sales Voucher No')
        sales_voucher_no.grid(row=1,column=0,padx=10,pady=10)
        self.sales_voucher_entry = tk.Entry(header_view)
        self.sales_voucher_entry.grid(row=1,column=1,padx=10,pady=10)

        party_account_label = tk.Label(header_view,text='Party A/C Details')
        party_account_label.grid(row=2,column=0,padx=10,pady=10)
        self.party_ac_value = tk.StringVar()
        self.party_account_type = ttk.Combobox(header_view,textvariable=self.party_ac_value)
        self.party_account_type.grid(row=2,column=1,padx=10,pady=10)
        self.party_account_type.bind('<<ComboboxSelected>>',self.controller.supplier_selected)

        party_account_date_label = tk.Label(header_view, text='Date')
        party_account_date_label.grid(row=1,column=3)
        self.party_account_date = DateEntry(header_view, selectmode='day', date_pattern='dd/mm/y')
        self.party_account_date.grid(row=1,column=4,padx=10,pady=10)

        gst_type_label = tk.Label(header_view,text='GST Type')
        gst_type_label.grid(row=0,column=5)
        self.gst_type_value = tk.StringVar()
        self.gst_type_value.set('Select An Item')
        self.gst_type = ttk.Combobox(header_view,textvariable=self.gst_type_value)
        self.gst_type.grid(row=0,column=6)

        style = ttk.Style()
        style.theme_use('default')
        style.configure('Treeview',
        background='#D3D3D3',
        foreground='black',
        rowheight=25,
        fieldbackground='#D3D3D3'
        )

        style.map('Treeview',
        background=[('selected','#347083')])

        tree_scroll = tk.Scrollbar(tree_frame)
        tree_scroll.pack(side='right',fill='y')

        self.my_tree = ttk.Treeview(tree_frame,yscrollcommand=tree_scroll.set,selectmode='extended')
        self.my_tree.pack()

        tree_scroll.config(command=self.my_tree.yview)

        self.my_tree['columns'] = ('Sl No','Barcode','Batch No','Name','HSN Code','Quantity','Cost','Discount',
                                    'Taxable','Cgst','Sgst','Igst','Cess','Ledger Type','Total Amount')

        self.my_tree.column('#0', width=0, stretch='no')
        self.my_tree.column('Sl No',anchor='w',width=140)
        self.my_tree.column('Barcode',anchor='w',width=140)
        self.my_tree.column('Batch No',anchor='center',width=140)
        self.my_tree.column('Name',anchor='center',width=140)
        self.my_tree.column('HSN Code',anchor='center',width=140)
        self.my_tree.column('Quantity',anchor='center',width=140)
        self.my_tree.column('Cost',anchor='center',width=140)
        self.my_tree.column('Discount',anchor='center',width=140)
        self.my_tree.column('Taxable',anchor='center',width=140)
        self.my_tree.column('Cgst',anchor='center',width=100)
        self.my_tree.column('Sgst',anchor='center',width=100)
        self.my_tree.column('Igst',anchor='center',width=100)
        self.my_tree.column('Cess',anchor='center',width=100)
        self.my_tree.column('Ledger Type',anchor='center',width=100)
        self.my_tree.column('Total Amount',anchor='center',width=140,stretch='yes')

        self.my_tree.heading('#0', text='', anchor='w')
        self.my_tree.heading('Sl No',text="Serial No",anchor='w')
        self.my_tree.heading('Barcode',text="Barcode",anchor='w')
        self.my_tree.heading('Batch No',text="Batch No",anchor='center')
        self.my_tree.heading('Name',text="Name Of Item",anchor='center')
        self.my_tree.heading('HSN Code',text="HSN Code",anchor='center')
        self.my_tree.heading('Quantity',text="Quantity",anchor='center')
        self.my_tree.heading('Cost',text="Cost",anchor='center')
        self.my_tree.heading('Discount',text="Discount",anchor='center')
        self.my_tree.heading('Taxable',text="Taxble",anchor='center')
        self.my_tree.heading('Cgst',text="CGST",anchor='center')
        self.my_tree.heading('Sgst',text="SGST",anchor='center')
        self.my_tree.heading('Igst',text="IGST",anchor='center')
        self.my_tree.heading('Cess',text="CESS",anchor='center')
        self.my_tree.heading('Ledger Type',text='Ledger Type',anchor='center')
        self.my_tree.heading('Total Amount',text="Total Amount",anchor='w')


        self.data = []


        self.my_tree.tag_configure('oddrow',background='white')
        self.my_tree.tag_configure('evenrow',background='lightblue')


        #Input
        data_input_frame = tk.LabelFrame(tree_frame,text='Input Data')
        data_input_frame.pack(fill='x',expand='yes',padx=20)

        name_label = tk.Label(data_input_frame,text='Item Name')
        name_label.grid(row=0,column=0,padx=10,pady=10)
        self.item_clicked = tk.StringVar()
        self.item_clicked_id = tk.IntVar()
        self.items_combobox = ttk.Combobox(data_input_frame)
        self.items_combobox.bind('<<ComboboxSelected>>', self.controller.selected_item)
        self.items_combobox.bind('<KeyRelease>', self.controller.item_onchage)
        self.items_combobox.grid(row=0,column=1,padx=10,pady=10)

        batch_no_label = tk.Label(data_input_frame,text='Batch No')
        batch_no_label.grid(row=0,column=2,padx=10,pady=10)
        self.batch_no = tk.StringVar()
        self.batch_no_entry = ttk.Combobox(data_input_frame)
        self.batch_no_entry.bind('<<ComboboxSelected>>', self.controller.selected_batch)
        
        self.batch_no_entry.grid(row=0,column=3,padx=10,pady=10)

        barcode_label = tk.Label(data_input_frame,text='Barcode')
        barcode_label.grid(row=0,column=4,padx=10,pady=10)
        self.barcode_entry = tk.Entry(data_input_frame)
        self.barcode_entry.grid(row=0,column=5,padx=10,pady=10)


        hsn_code_label = tk.Label(data_input_frame,text='HSN CODE')
        hsn_code_label.grid(row=0,column=6,padx=10,pady=10)
        self.hsn_entry = tk.Entry(data_input_frame)
        self.hsn_entry.grid(row=0,column=7,padx=10,pady=10)

        rate_label = tk.Label(data_input_frame,text='Cost')
        rate_label.grid(row=0,column=8,padx=10,pady=10)
        self.rate_entry = tk.Entry(data_input_frame)
        self.rate_entry.grid(row=0,column=9,padx=10,pady=10)

        qty_label = tk.Label(data_input_frame,text='Quantity')
        qty_label.grid(row=0,column=10,padx=10,pady=10)
        self.qty_entry = tk.Entry(data_input_frame)
        self.qty_entry.grid(row=0,column=11,padx=10,pady=10)
        self.qty_entry.bind("<KeyRelease>", self.controller.on_qty_changed)
        # Tooltip(self.qty_entry, text='Item Expires In {}'.format(28) , wraplength=100)
        
        disc_label = tk.Label(data_input_frame,text='Discount')
        disc_label.grid(row=0,column=12,padx=10,pady=10)
        self.disc_entry = tk.Entry(data_input_frame)
        self.disc_entry.grid(row=0,column=13,padx=10,pady=10)
        self.disc_entry.bind("<KeyRelease>", self.controller.on_disc_changed)

        ledger_label = tk.Label(data_input_frame,text='Ledger')
        ledger_label.grid(row=0,column=14,padx=10,pady=10)
        
        self.ledger_clicked = tk.StringVar()
        self.ledger_clicked_id = tk.IntVar()
        self.ledger_combobox = ttk.Combobox(data_input_frame)
        self.ledger_combobox.bind('<<ComboboxSelected>>', self.controller.ledger_clicked)
        self.ledger_combobox.grid(row=0,column=15,padx=10,pady=10)

        gst_label = tk.Label(data_input_frame,text='GST %')
        gst_label.grid(row=1,column=0,padx=10,pady=10)
        self.gst_entry = tk.Entry(data_input_frame)
        self.gst_entry.grid(row=1,column=1,padx=10,pady=10)

        cgst_label = tk.Label(data_input_frame,text='CGST')
        cgst_label.grid(row=1,column=2,padx=10,pady=10)
        self.cgst_entry = tk.Entry(data_input_frame)
        self.cgst_entry.grid(row=1,column=3,padx=10,pady=10)

        sgst_label = tk.Label(data_input_frame,text='SGST')
        sgst_label.grid(row=1,column=4,padx=10,pady=10)
        self.sgst_entry = tk.Entry(data_input_frame)
        self.sgst_entry.grid(row=1,column=5,padx=10,pady=10)

        igst_label = tk.Label(data_input_frame,text='IGST')
        igst_label.grid(row=1,column=6,padx=10,pady=10)
        self.igst_entry = tk.Entry(data_input_frame)
        self.igst_entry.grid(row=1,column=7,padx=10,pady=10)

        cess_label = tk.Label(data_input_frame,text='CESS')
        cess_label.grid(row=1,column=8,padx=10,pady=10)
        self.cess_clicked = tk.StringVar()
        self.cess_clicked_id = tk.IntVar()
        self.cess_entry = ttk.Combobox(data_input_frame, textvariable=self.cess_clicked)
        self.cess_entry.bind('<<ComboboxSelected>>', self.controller.cess_selected)
        self.cess_entry.grid(row=1,column=9,padx=10,pady=10)

        total_amount_label = tk.Label(data_input_frame,text='Net Amount')
        total_amount_label.grid(row=1,column=10,padx=10,pady=10)
        self.item_netamount_entry = tk.Entry(data_input_frame)
        self.item_netamount_entry.grid(row=1,column=11,padx=10,pady=10)

        manufactring_date = tk.Label(data_input_frame,text='MFG Date')
        manufactring_date.grid(row=1,column=12,padx=10,pady=10)
        self.manufacturing_date_entry = DateEntry(data_input_frame,selectmode='day',date_pattern='dd/mm/y')
        self.manufacturing_date_entry.grid(row=1,column=13,padx=10,pady=10)

        expiry_date = tk.Label(data_input_frame,text='Expiry Date')
        expiry_date.grid(row=1,column=14,padx=10,pady=10)
        self.expiry_date_entry = DateEntry(data_input_frame,selectmode='day',date_pattern='dd/mm/y')
        self.expiry_date_entry.grid(row=1,column=15,padx=10,pady=10)

        
        taxable_label = tk.Label(data_input_frame,text='Taxable Amount')
        taxable_label.grid(row=2,column=0,padx=10,pady=10)
        self.taxable_entry = tk.Entry(data_input_frame)
        self.taxable_entry.grid(row=2,column=1,padx=10,pady=10)


        add_button = tk.Button(data_input_frame,text='Add',command=self.controller.add_to_sales_list)
        add_button.grid(row=2,column=13,padx=10,pady=10)

        remove_button = tk.Button(data_input_frame,text='Remove',command=self.controller.remove_from_sales_list)
        remove_button.grid(row=2,column=14,padx=10,pady=10)
        

        generate_orderbtn = tk.Button(self.sales_voucher_frame,text='Generate Order',command=self.controller.create_sale)
        generate_orderbtn.pack(side='right')
        generate_orderbtn.bind('<Return>', self.controller.create_sale)

        
        sales_list_view_btn = tk.Button(self.sales_voucher_frame,text='Sales List View',command=self.sales_list_popup)
        sales_list_view_btn.pack(side='left')


    def batches_popup(self):
        self.batches_pop = tk.Toplevel(self.sales_voucher_frame)
        self.batches_pop.geometry('600x400')
        self.batches_pop.wm_title('Batches')

        self.batches_treeview = ttk.Treeview(self.batches_pop)
        self.batches_treeview.pack()

        self.batches_treeview['columns'] = ('Batch_No','Quantity','Expiry')

        self.batches_treeview.column('#0', width=0, stretch='no')
        self.batches_treeview.column('Batch_No',anchor='w',width=140)
        self.batches_treeview.column('Quantity',anchor='w',width=140)
        self.batches_treeview.column('Expiry',anchor='w',width=140)

        self.batches_treeview.heading('#0', text='', anchor='w')
        self.batches_treeview.heading('Batch_No',text="Batch No",anchor='w')
        self.batches_treeview.heading('Quantity',text="Quantity",anchor='w')
        self.batches_treeview.heading('Expiry',text="Expiry",anchor='w')






    def sales_list_popup(self):
        self.sales_list_frame = tk.Toplevel(self.sales_voucher_frame)
        self.sales_list_frame.geometry('600x400')
        self.sales_list_frame.wm_title('Sales List View')

        style = ttk.Style()
        style.theme_use('default')
        style.configure('Treeview',
        background='#D3D3D3',
        foreground='black',
        rowheight=25,
        fieldbackground='#D3D3D3'
        )

        style.map('Treeview',
        background=[('selected','#347083')])

        tree_scroll = tk.Scrollbar(self.sales_list_frame)
        tree_scroll.pack(side='right',fill='y')

        self.sales_voucher_view = ttk.Treeview(self.sales_list_frame,yscrollcommand=tree_scroll.set,selectmode='extended')
        self.sales_voucher_view.pack()

        tree_scroll.config(command=self.my_tree.yview)

        self.sales_voucher_view['columns'] = ('invoice_no','invoice_date','gst_type','item_name','ledger','quantity','total_amount')

        self.sales_voucher_view.column('#0', width=0, stretch='no')
        self.sales_voucher_view.column('invoice_no',anchor='w',width=140)
        self.sales_voucher_view.column('invoice_date',anchor='w',width=140)
        self.sales_voucher_view.column('gst_type',anchor='center',width=140)
        self.sales_voucher_view.column('item_name',anchor='center',width=140)
        self.sales_voucher_view.column('ledger',anchor='center',width=140)
        self.sales_voucher_view.column('quantity',anchor='center',width=140)
        self.sales_voucher_view.column('total_amount',anchor='center',width=140)

        self.sales_voucher_view.heading('#0', text='', anchor='w')
        self.sales_voucher_view.heading('invoice_no',text="Invoice No",anchor='w')
        self.sales_voucher_view.heading('invoice_date',text="Invoice Date",anchor='w')
        self.sales_voucher_view.heading('gst_type',text="Gst Type",anchor='center')
        self.sales_voucher_view.heading('item_name',text="Item Name",anchor='center')
        self.sales_voucher_view.heading('ledger',text="Ledger",anchor='center')
        self.sales_voucher_view.heading('quantity',text="Quantity",anchor='center')
        self.sales_voucher_view.heading('total_amount',text="Total Amount",anchor='center')

        self.data = []

        self.sales_voucher_view.tag_configure('oddrow',background='white')
        self.sales_voucher_view.tag_configure('evenrow',background='lightblue')

        self.controller.sales_list_transaction()