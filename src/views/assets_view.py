from src.views.base_view import BaseView
import tkinter as tk
from tkinter import ttk


class AssetsView(BaseView):
    def __init__(self, assets_controller):
        super(AssetsView, self).__init__()
        self.title('Asset Group')
        self.assets_frame = tk.Frame(self)
        self.assets_controller = assets_controller
        self.set_layout()


    def set_layout(self):
        assets_lable = tk.Label(self.assets_frame, text='Assets Groups')
        assets_lable.grid(row=1,column=2)

        self.asset_group_input = tk.Entry(self.assets_frame)
        self.asset_group_input.grid(row=3,column=1)

        self.asset_group_add = tk.Button(self.assets_frame, text='Add',command=self.assets_controller.add_item)
        self.asset_group_add.grid(row=3, column=2, padx=10, pady=10)

        self.asset_group_view = ttk.Treeview(self.assets_frame,height=10)
        self.asset_group_view["columns"] = ("id", "Name")
        
        self.asset_group_view.column('#0', width=0, stretch='no')
        self.asset_group_view.column("id", width = 100, anchor ='w')
        self.asset_group_view.column("Name", width = 200, anchor ='w')

        self.asset_group_view.heading('#0', text='', anchor='e')
        self.asset_group_view.heading("id", text ="ID")
        self.asset_group_view.heading("Name", text ="Name")
        self.asset_group_view.grid(row=4, column=1, sticky='nsew')
        

        self.asset_group_view.bind("<ButtonRelease-1>", self.assets_controller.select_item)

        self.asset_group_update_btn = tk.Button(self.assets_frame, text='Update',command=self.assets_controller.update_item)
        self.asset_group_update_btn.grid_forget()

        self.asset_group_edit_btn = tk.Button(self.assets_frame, text='Edit',command=self.assets_controller.edit_item)
        self.asset_group_edit_btn.grid(row=4, column=2)

        self.asset_group_delete_btn = tk.Button(self.assets_frame, text='Delete',command=self.assets_controller.delete_item)
        self.asset_group_delete_btn.grid(row=5, column=2)

        self.ledger_popupBtn = tk.Button(self.assets_frame, text='Ledger Management',command=self.asset_ledger_popup)
        self.ledger_popupBtn.grid(row=6, column=1)
        
        self.assets_frame.pack(pady=10)


    def asset_ledger_popup(self):
        self.asset_ledger_frame = tk.Toplevel(self.assets_frame)
        self.asset_ledger_frame.geometry('600x400')
        self.asset_ledger_frame.wm_title('Asset Ledger Management')
        selected = self.asset_group_view.focus()
        group_id,group_name = self.asset_group_view.item(selected, 'values')
        if 'Current Asset' == group_name:
            self.current_asset_forms()
        elif 'Bank' == group_name:
            self.bank_forms()
        elif 'Cash in Hand' == group_name:
            self.cash_in_hand_forms()
        elif 'Petty Cash' == group_name:
            self.petty_cash_forms()
        elif 'Sundry debtors (customer)' == group_name:
            self.sundry_debtor_forms()
        elif 'Bills Receivable' == group_name:
            self.bills_recivable_forms()
        elif 'Prepaid expense' == group_name:
            self.prepaid_expense_forms()
        elif 'Accured Income' == group_name:
            self.accured_income_forms()

        self.asset_ledger_frame.wm_grid()


    #forms

    def current_asset_forms(self):
        ledger_name_label = tk.Label(self.asset_ledger_frame,text='Ledger Name')
        ledger_name_label.grid(row=4,column=1)
        self.ledger_name_input = tk.Entry(self.asset_ledger_frame)
        self.ledger_name_input.grid(row=4,column=2)


        credit_period_label = tk.Label(self.asset_ledger_frame,text='Credit Period')
        credit_period_label.grid(row=5,column=1)
        self.credit_period_input = tk.Entry(self.asset_ledger_frame)
        self.credit_period_input.grid(row=5,column=2)


        type_label = tk.Label(self.asset_ledger_frame,text='Type')
        type_label.grid(row=5,column=3)
        self.type_input = tk.Entry(self.asset_ledger_frame)
        self.type_input.insert('end', 'Debit')
        self.type_input.configure(state='disabled')
        self.type_input.grid(row=5,column=4)


        contact_details_label = tk.Label(self.asset_ledger_frame,text='Contact Details')
        contact_details_label.grid(row=6,column=1)
        self.contact_details_input = tk.Entry(self.asset_ledger_frame)
        self.contact_details_input.grid(row=6,column=2)

        address_details_label = tk.Label(self.asset_ledger_frame,text='Address')
        address_details_label.grid(row=7,column=1)
        self.address_details_input = tk.Entry(self.asset_ledger_frame)
        self.address_details_input.grid(row=7,column=2)

        phone_number_label = tk.Label(self.asset_ledger_frame,text='Phone Number')
        phone_number_label.grid(row=8,column=1)
        self.phone_number_input = tk.Entry(self.asset_ledger_frame)
        self.phone_number_input.grid(row=8,column=2)

        pan_no_label = tk.Label(self.asset_ledger_frame,text='Pan No')
        pan_no_label.grid(row=9,column=1)
        self.pan_no_input = tk.Entry(self.asset_ledger_frame)
        self.pan_no_input.grid(row=9,column=2)

        gst_label = tk.Label(self.asset_ledger_frame,text='GST Type')
        gst_label.grid(row=10,column=1)
        
        types_of_gst = [
            'Regular',
            'Composition',
            'Unregistered',
            'Customer'
            ]

        self.gst_clicked = tk.StringVar()
        self.gst_clicked.set(types_of_gst[0])
        self.gst_options = ttk.Combobox(self.asset_ledger_frame, values=types_of_gst, textvariable=self.gst_clicked)
        self.gst_options.grid(row=10,column=2)
        self.gst_clicked.trace('w', self.assets_controller.gst_onchange)


        gst_no_label = tk.Label(self.asset_ledger_frame, text='Gst No')
        gst_no_label.grid(row=11,column=1)
        self.gst_no_input = tk.Entry(self.asset_ledger_frame)
        self.gst_no_input.grid(row=11,column=2)

        gst_percentage_label = tk.Label(self.asset_ledger_frame,text='Gst %')
        gst_percentage_label.grid(row=12,column=1)
        self.gst_percentage_input = tk.Entry(self.asset_ledger_frame)
        self.gst_percentage_input.grid(row=12,column=2)

        opening_balance_label = tk.Label(self.asset_ledger_frame,text='Opening Balance')
        opening_balance_label.grid(row=13,column=1)
        self.opening_balance_input = tk.Entry(self.asset_ledger_frame)
        self.opening_balance_input.grid(row=13,column=2)


        self.ledger_add_btn = tk.Button(self.asset_ledger_frame,text='ADD',command=self.assets_controller.add_to_ledger)
        self.ledger_add_btn.grid(row=15,column=2)

        ledger_name_label = tk.Label(self.asset_ledger_frame,text='Ledger Name')
        ledger_name_label.grid(row=4,column=1)
        self.ledger_name_input = tk.Entry(self.asset_ledger_frame)
        self.ledger_name_input.grid(row=4,column=2)

    def bank_forms(self):
        ledger_name_label = tk.Label(self.asset_ledger_frame,text='Ledger Name')
        ledger_name_label.grid(row=3,column=1)
        self.ledger_name_input = tk.Entry(self.asset_ledger_frame)
        self.ledger_name_input.grid(row=3,column=2)

        credit_period_label = tk.Label(self.asset_ledger_frame,text='Credit Period')
        credit_period_label.grid(row=4,column=1)
        self.credit_period_input = tk.Entry(self.asset_ledger_frame)
        self.credit_period_input.grid(row=4,column=2)

        
        type_label = tk.Label(self.asset_ledger_frame,text='Type')
        type_label.grid(row=4,column=3)
        self.type_input = tk.Entry(self.asset_ledger_frame)
        self.type_input.insert('end', 'Debit')
        self.type_input.configure(state='disabled')
        self.type_input.grid(row=4,column=4)

        contact_details_label = tk.Label(self.asset_ledger_frame, text='Contact Details')
        contact_details_label.grid(row=5,column=1)
        self.contact_details_input = tk.Entry(self.asset_ledger_frame)
        self.contact_details_input.grid(row=5,column=2)

        address_label = tk.Label(self.asset_ledger_frame, text='Address')
        address_label.grid(row=6,column=1)
        self.address_details_input = tk.Entry(self.asset_ledger_frame)
        self.address_details_input.grid(row=6,column=2)

        phone_number_label = tk.Label(self.asset_ledger_frame, text='Phone Number')
        phone_number_label.grid(row=7,column=1)
        self.phone_number_input = tk.Entry(self.asset_ledger_frame)
        self.phone_number_input.grid(row=7,column=2)

        gst_label = tk.Label(self.asset_ledger_frame,text='GST Type')
        gst_label.grid(row=8,column=1)
        
        types_of_gst = [
            'Regular',
            'Composition',
            'Unregistered',
            'Customer'
            ]

        self.gst_clicked = tk.StringVar()
        self.gst_clicked.set(types_of_gst[0])
        self.gst_options = ttk.Combobox(self.asset_ledger_frame, values=types_of_gst, textvariable=self.gst_clicked)
        self.gst_options.grid(row=8,column=2)
        self.gst_clicked.trace('w', self.assets_controller.gst_onchange)

        gst_no_label = tk.Label(self.asset_ledger_frame, text='Gst No')
        gst_no_label.grid(row=9,column=1)
        self.gst_no_input = tk.Entry(self.asset_ledger_frame)
        self.gst_no_input.grid(row=9,column=2)

        bank_name = tk.Label(self.asset_ledger_frame, text='Bank Name')
        bank_name.grid(row=10,column=1)

        self.bank_input = tk.Entry(self.asset_ledger_frame)
        self.bank_input.grid(row=10,column=2)

        bank_address = tk.Label(self.asset_ledger_frame, text='Bank Address')
        bank_address.grid(row=11,column=1)
        self.bank_address_input = tk.Entry(self.asset_ledger_frame)
        self.bank_address_input.grid(row=11,column=2)

        account_no = tk.Label(self.asset_ledger_frame, text='Bank A/C No')
        account_no.grid(row=12,column=1)
        self.account_no_input = tk.Entry(self.asset_ledger_frame)
        self.account_no_input.grid(row=12,column=2)

        ifsc_code = tk.Label(self.asset_ledger_frame, text='IFSC CODE')
        ifsc_code.grid(row=13,column=1)
        self.ifsc_code_input = tk.Entry(self.asset_ledger_frame)
        self.ifsc_code_input.grid(row=13,column=2)

        branch_no = tk.Label(self.asset_ledger_frame, text='Branch No')
        branch_no.grid(row=14,column=1)
        self.branch_no_input = tk.Entry(self.asset_ledger_frame)
        self.branch_no_input.grid(row=14,column=2)

        branch_code = tk.Label(self.asset_ledger_frame, text='Branch Code')
        branch_code.grid(row=15,column=1)
        self.branch_code_input = tk.Entry(self.asset_ledger_frame)
        self.branch_code_input.grid(row=15,column=2)

        bank_contact_label = tk.Label(self.asset_ledger_frame, text='Bank Contact No')
        bank_contact_label.grid(row=16,column=1)
        self.bank_contact_input = tk.Entry(self.asset_ledger_frame)
        self.bank_contact_input.grid(row=16,column=2)

        cheque_no_label = tk.Label(self.asset_ledger_frame, text='Cheque No')
        cheque_no_label.grid(row=17,column=1)
        self.cheque_no_input = tk.Entry(self.asset_ledger_frame)
        self.cheque_no_input.grid(row=17,column=2)

        opening_balance_label = tk.Label(self.asset_ledger_frame, text='Opening Balance')
        opening_balance_label.grid(row=18,column=1)
        self.opening_balance_input = tk.Entry(self.asset_ledger_frame)
        self.opening_balance_input.grid(row=18,column=2)

        self.ledger_add_btn = tk.Button(self.asset_ledger_frame,text='ADD',command=self.assets_controller.add_to_ledger)
        self.ledger_add_btn.grid(row=21,column=2)

    def cash_in_hand_forms(self):
        ledger_name_label = tk.Label(self.asset_ledger_frame,text='Ledger Name')
        ledger_name_label.grid(row=3,column=1)
        self.ledger_name_input = tk.Entry(self.asset_ledger_frame)
        self.ledger_name_input.grid(row=3,column=2)

        type_label = tk.Label(self.asset_ledger_frame,text='Type')
        type_label.grid(row=3,column=3)
        self.type_input = tk.Entry(self.asset_ledger_frame)
        self.type_input.insert('end', 'Debit')
        self.type_input.configure(state='disabled')
        self.type_input.grid(row=3,column=4)
   

        contact_details_label = tk.Label(self.asset_ledger_frame,text='Contact Details')
        contact_details_label.grid(row=5,column=1)
        self.contact_details_input = tk.Entry(self.asset_ledger_frame)
        self.contact_details_input.grid(row=5,column=2)

        address_details_label = tk.Label(self.asset_ledger_frame,text='Address')
        address_details_label.grid(row=6,column=1)
        self.address_details_input = tk.Entry(self.asset_ledger_frame)
        self.address_details_input.grid(row=6,column=2)

        phone_number_label = tk.Label(self.asset_ledger_frame,text='Phone Number')
        phone_number_label.grid(row=7,column=1)
        self.phone_number_input = tk.Entry(self.asset_ledger_frame)
        self.phone_number_input.grid(row=7,column=2)

        opening_balance_label = tk.Label(self.asset_ledger_frame,text='Opening Balance')
        opening_balance_label.grid(row=11,column=1)
        self.opening_balance_input = tk.Entry(self.asset_ledger_frame)
        self.opening_balance_input.grid(row=11,column=2)

        self.ledger_add_btn = tk.Button(self.asset_ledger_frame,text='ADD',command=self.assets_controller.add_to_ledger)
        self.ledger_add_btn.grid(row=12,column=2)

    def petty_cash_forms(self):
        ledger_name_label = tk.Label(self.asset_ledger_frame,text='Ledger Name')
        ledger_name_label.grid(row=3,column=1)
        self.ledger_name_input = tk.Entry(self.asset_ledger_frame)
        self.ledger_name_input.grid(row=3,column=2)

        type_label = tk.Label(self.asset_ledger_frame,text='Type')
        type_label.grid(row=3,column=3)
        self.type_input = tk.Entry(self.asset_ledger_frame)
        self.type_input.insert('end', 'Debit')
        self.type_input.configure(state='disabled')
        self.type_input.grid(row=3,column=4)

        contact_details_label = tk.Label(self.asset_ledger_frame,text='Contact Details')
        contact_details_label.grid(row=5,column=1)
        self.contact_details_input = tk.Entry(self.asset_ledger_frame)
        self.contact_details_input.grid(row=5,column=2)

        address_details_label = tk.Label(self.asset_ledger_frame,text='Address')
        address_details_label.grid(row=6,column=1)
        self.address_details_input = tk.Entry(self.asset_ledger_frame)
        self.address_details_input.grid(row=6,column=2)

        phone_number_label = tk.Label(self.asset_ledger_frame,text='Phone Number')
        phone_number_label.grid(row=7,column=1)
        self.phone_number_input = tk.Entry(self.asset_ledger_frame)
        self.phone_number_input.grid(row=7,column=2)

        opening_balance_label = tk.Label(self.asset_ledger_frame,text='Opening Balance')
        opening_balance_label.grid(row=11,column=1)
        self.opening_balance_input = tk.Entry(self.asset_ledger_frame)
        self.opening_balance_input.grid(row=11,column=2)

        self.ledger_add_btn = tk.Button(self.asset_ledger_frame,text='ADD',command=self.assets_controller.add_to_ledger)
        self.ledger_add_btn.grid(row=12,column=2)

    def sundry_debtor_forms(self):
        ledger_name_label = tk.Label(self.asset_ledger_frame,text='Ledger Name')
        ledger_name_label.grid(row=3,column=1)
        self.ledger_name_input = tk.Entry(self.asset_ledger_frame)
        self.ledger_name_input.grid(row=3,column=2)

        type_label = tk.Label(self.asset_ledger_frame,text='Type')
        type_label.grid(row=3,column=3)
        self.type_input = tk.Entry(self.asset_ledger_frame)
        self.type_input.insert('end', 'Debit')
        self.type_input.configure(state='disabled')
        self.type_input.grid(row=3,column=4)

        credit_period_label = tk.Label(self.asset_ledger_frame,text='Credit Period')
        credit_period_label.grid(row=4,column=1)
        self.credit_period_input = tk.Entry(self.asset_ledger_frame)
        self.credit_period_input.grid(row=4,column=2)

        contact_details_label = tk.Label(self.asset_ledger_frame,text='Contact Details')
        contact_details_label.grid(row=6,column=1)
        self.contact_details_input = tk.Entry(self.asset_ledger_frame)
        self.contact_details_input.grid(row=6,column=2)

        address_details_label = tk.Label(self.asset_ledger_frame,text='Address')
        address_details_label.grid(row=7,column=1)
        self.address_details_input = tk.Entry(self.asset_ledger_frame)
        self.address_details_input.grid(row=7,column=2)

        phone_number_label = tk.Label(self.asset_ledger_frame,text='Phone Number')
        phone_number_label.grid(row=8,column=1)
        self.phone_number_input = tk.Entry(self.asset_ledger_frame)
        self.phone_number_input.grid(row=8,column=2)

        pan_no_label = tk.Label(self.asset_ledger_frame,text='Pan No')
        pan_no_label.grid(row=9,column=1)
        self.pan_no_input = tk.Entry(self.asset_ledger_frame)
        self.pan_no_input.grid(row=9,column=2)

        gst_label = tk.Label(self.asset_ledger_frame,text='GST Type')
        gst_label.grid(row=10,column=1)
        
        types_of_gst = [
            'Regular',
            'Composition',
            'Unregistered',
            'Customer'
            ]

        self.gst_clicked = tk.StringVar()
        self.gst_clicked.set(types_of_gst[0])
        self.gst_options = ttk.Combobox(self.asset_ledger_frame, values=types_of_gst, textvariable=self.gst_clicked)
        self.gst_options.grid(row=10,column=2)
        self.gst_clicked.trace('w', self.assets_controller.gst_onchange)

        gst_no_label = tk.Label(self.asset_ledger_frame, text='Gst No')
        gst_no_label.grid(row=11,column=1)
        self.gst_no_input = tk.Entry(self.asset_ledger_frame)
        self.gst_no_input.grid(row=11,column=2)

        opening_balance_label = tk.Label(self.asset_ledger_frame,text='Opening Balance')
        opening_balance_label.grid(row=12,column=1)
        self.opening_balance_input = tk.Entry(self.asset_ledger_frame)
        self.opening_balance_input.grid(row=12,column=2)

        self.ledger_add_btn = tk.Button(self.asset_ledger_frame,text='ADD',command=self.assets_controller.add_to_ledger)
        self.ledger_add_btn.grid(row=13,column=2)

    def bills_recivable_forms(self):
        ledger_name_label = tk.Label(self.asset_ledger_frame,text='Ledger Name')
        ledger_name_label.grid(row=3,column=1)
        self.ledger_name_input = tk.Entry(self.asset_ledger_frame)
        self.ledger_name_input.grid(row=3,column=2)

        type_label = tk.Label(self.asset_ledger_frame,text='Type')
        type_label.grid(row=3,column=3)
        self.type_input = tk.Entry(self.asset_ledger_frame)
        self.type_input.insert('end', 'Debit')
        self.type_input.configure(state='disabled')
        self.type_input.grid(row=3,column=4)

        credit_period_label = tk.Label(self.asset_ledger_frame,text='Credit Period')
        credit_period_label.grid(row=5,column=1)
        self.credit_period_input = tk.Entry(self.asset_ledger_frame)
        self.credit_period_input.grid(row=5,column=2)

        contact_details_label = tk.Label(self.asset_ledger_frame,text='Contact Details')
        contact_details_label.grid(row=6,column=1)
        self.contact_details_input = tk.Entry(self.asset_ledger_frame)
        self.contact_details_input.grid(row=6,column=2)

        address_details_label = tk.Label(self.asset_ledger_frame,text='Address')
        address_details_label.grid(row=7,column=1)
        self.address_details_input = tk.Entry(self.asset_ledger_frame)
        self.address_details_input.grid(row=7,column=2)

        phone_number_label = tk.Label(self.asset_ledger_frame,text='Phone Number')
        phone_number_label.grid(row=8,column=1)
        self.phone_number_input = tk.Entry(self.asset_ledger_frame)
        self.phone_number_input.grid(row=8,column=2)

        pan_no_label = tk.Label(self.asset_ledger_frame,text='Pan No')
        pan_no_label.grid(row=9,column=1)
        self.pan_no_input = tk.Entry(self.asset_ledger_frame)
        self.pan_no_input.grid(row=9,column=2)

        gst_label = tk.Label(self.asset_ledger_frame,text='GST Type')
        gst_label.grid(row=10,column=1)
        
        types_of_gst = [
            'Regular',
            'Composition',
            'Unregistered',
            'Customer'
            ]

        self.gst_clicked = tk.StringVar()
        self.gst_clicked.set(types_of_gst[0])
        self.gst_options = ttk.Combobox(self.asset_ledger_frame, values=types_of_gst, textvariable=self.gst_clicked)
        self.gst_options.grid(row=10,column=2)
        self.gst_clicked.trace('w', self.assets_controller.gst_onchange)

        gst_no_label = tk.Label(self.asset_ledger_frame, text='Gst No')
        gst_no_label.grid(row=11,column=1)
        self.gst_no_input = tk.Entry(self.asset_ledger_frame)
        self.gst_no_input.grid(row=11,column=2)

        opening_balance_label = tk.Label(self.asset_ledger_frame,text='Opening Balance')
        opening_balance_label.grid(row=12,column=1)
        self.opening_balance_input = tk.Entry(self.asset_ledger_frame)
        self.opening_balance_input.grid(row=12,column=2)

        self.ledger_add_btn = tk.Button(self.asset_ledger_frame,text='ADD',command=self.assets_controller.add_to_ledger)
        self.ledger_add_btn.grid(row=13,column=2)

    def prepaid_expense_forms(self):
        ledger_name_label = tk.Label(self.asset_ledger_frame,text='Ledger Name')
        ledger_name_label.grid(row=3,column=1)
        self.ledger_name_input = tk.Entry(self.asset_ledger_frame)
        self.ledger_name_input.grid(row=3,column=2)

        type_label = tk.Label(self.asset_ledger_frame,text='Type')
        type_label.grid(row=3,column=3)
        self.type_input = tk.Entry(self.asset_ledger_frame)
        self.type_input.insert('end', 'Debit')
        self.type_input.configure(state='disabled')
        self.type_input.grid(row=3,column=4)

        contact_details_label = tk.Label(self.asset_ledger_frame,text='Contact Details')
        contact_details_label.grid(row=5,column=1)
        self.contact_details_input = tk.Entry(self.asset_ledger_frame)
        self.contact_details_input.grid(row=5,column=2)

        address_details_label = tk.Label(self.asset_ledger_frame,text='Address')
        address_details_label.grid(row=6,column=1)
        self.address_details_input = tk.Entry(self.asset_ledger_frame)
        self.address_details_input.grid(row=6,column=2)

        phone_number_label = tk.Label(self.asset_ledger_frame,text='Phone Number')
        phone_number_label.grid(row=7,column=1)
        self.phone_number_input = tk.Entry(self.asset_ledger_frame)
        self.phone_number_input.grid(row=7,column=2)

        opening_balance_label = tk.Label(self.asset_ledger_frame,text='Opening Balance')
        opening_balance_label.grid(row=12,column=1)
        self.opening_balance_input = tk.Entry(self.asset_ledger_frame)
        self.opening_balance_input.grid(row=12,column=2)

        self.ledger_add_btn = tk.Button(self.asset_ledger_frame,text='ADD',command=self.assets_controller.add_to_ledger)
        self.ledger_add_btn.grid(row=13,column=2)

    def accured_income_forms(self):
        ledger_name_label = tk.Label(self.asset_ledger_frame,text='Ledger Name')
        ledger_name_label.grid(row=3,column=1)
        self.ledger_name_input = tk.Entry(self.asset_ledger_frame)
        self.ledger_name_input.grid(row=3,column=2)

        type_label = tk.Label(self.asset_ledger_frame,text='Type')
        type_label.grid(row=3,column=3)
        self.type_input = tk.Entry(self.asset_ledger_frame)
        self.type_input.insert('end', 'Debit')
        self.type_input.configure(state='disabled')
        self.type_input.grid(row=3,column=4)

        contact_details_label = tk.Label(self.asset_ledger_frame,text='Contact Details')
        contact_details_label.grid(row=5,column=1)
        self.contact_details_input = tk.Entry(self.asset_ledger_frame)
        self.contact_details_input.grid(row=5,column=2)

        address_details_label = tk.Label(self.asset_ledger_frame,text='Address')
        address_details_label.grid(row=6,column=1)
        self.address_details_input = tk.Entry(self.asset_ledger_frame)
        self.address_details_input.grid(row=6,column=2)

        phone_number_label = tk.Label(self.asset_ledger_frame,text='Phone Number')
        phone_number_label.grid(row=7,column=1)
        self.phone_number_input = tk.Entry(self.asset_ledger_frame)
        self.phone_number_input.grid(row=7,column=2)

        pan_no_label = tk.Label(self.asset_ledger_frame,text='Pan No')
        pan_no_label.grid(row=8,column=1)
        self.pan_no_input = tk.Entry(self.asset_ledger_frame)
        self.pan_no_input.grid(row=8,column=2)

        opening_balance_label = tk.Label(self.asset_ledger_frame,text='Opening Balance')
        opening_balance_label.grid(row=11,column=1)
        self.opening_balance_input = tk.Entry(self.asset_ledger_frame)
        self.opening_balance_input.grid(row=11,column=2)

        self.ledger_add_btn = tk.Button(self.asset_ledger_frame,text='ADD',command=self.assets_controller.add_to_ledger)
        self.ledger_add_btn.grid(row=12,column=2)
