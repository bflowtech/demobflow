from src.views.base_view import BaseView
import tkinter as tk
from tkinter import ttk


class CompanyView(BaseView):
    def __init__(self,company_view_controller):
        super(CompanyView,self).__init__()
        self.title('Company View')
        self.geometry('500x500')
        self.company_frame = tk.Frame(self)
        self.company_frame.pack(pady=10)
        self.controller = company_view_controller
        self.set_layout()

    def set_layout(self):
        company_name_label = tk.Label(self.company_frame,text='Company Name')
        company_name_label.grid(row=0,column=0,padx=10,pady=10)
        self.company_name_entry = tk.Entry(self.company_frame)
        self.company_name_entry.grid(row=0,column=1,padx=10,pady=10)

        address_label = tk.Label(self.company_frame,text='Address')
        address_label.grid(row=1,column=0,padx=10,pady=10)
        self.address_entry = tk.Entry(self.company_frame)
        self.address_entry.grid(row=1,column=1)

        city_label = tk.Label(self.company_frame,text='City')
        city_label.grid(row=2,column=0,padx=10,pady=10)
        self.city_entry = tk.Entry(self.company_frame)
        self.city_entry.grid(row=2,column=1,padx=10,pady=10)

        state_label = tk.Label(self.company_frame,text='State')
        state_label.grid(row=3,column=0,padx=10,pady=10)
        self.state_entry = tk.Entry(self.company_frame)
        self.state_entry.grid(row=3,column=1,padx=10,pady=10)

        country_label = tk.Label(self.company_frame,text='Country')
        country_label.grid(row=4,column=0,padx=10,pady=10)
        self.country_entry = tk.Entry(self.company_frame)
        self.country_entry.grid(row=4,column=1,padx=10,pady=10)

        pincode_label = tk.Label(self.company_frame,text='Pincode')
        pincode_label.grid(row=5,column=0,padx=10,pady=10)
        self.pincode_entry = tk.Entry(self.company_frame)
        self.pincode_entry.grid(row=5,column=1,padx=10,pady=10)

        gst_no_label = tk.Label(self.company_frame,text='GST NO')
        gst_no_label.grid(row=6,column=0,padx=10,pady=10)
        self.gst_no_entry = tk.Entry(self.company_frame)
        self.gst_no_entry.grid(row=6,column=1,padx=10,pady=10)

        phone_no_label = tk.Label(self.company_frame,text='Phone No')
        phone_no_label.grid(row=0,column=2,padx=10,pady=10)
        self.phone_no_entry = tk.Entry(self.company_frame)
        self.phone_no_entry.grid(row=0,column=3,padx=10,pady=10)

        mobile_no_label = tk.Label(self.company_frame,text='Mobile No')
        mobile_no_label.grid(row=1,column=2,padx=10,pady=10)
        self.mobile_no_entry = tk.Entry(self.company_frame)
        self.mobile_no_entry.grid(row=1,column=3,padx=10,pady=10)

        fax_no_label = tk.Label(self.company_frame,text='Fax No')
        fax_no_label.grid(row=2,column=2,padx=10,pady=10)
        self.fax_no_entry = tk.Entry(self.company_frame)
        self.fax_no_entry.grid(row=2,column=3,padx=10,pady=10)
        
        email_label = tk.Label(self.company_frame,text='Email')
        email_label.grid(row=3,column=2,padx=10,pady=10)
        self.email_entry = tk.Entry(self.company_frame)
        self.email_entry.grid(row=3,column=3,padx=10,pady=10)

        website_label = tk.Label(self.company_frame,text='Website')
        website_label.grid(row=4,column=2,padx=10,pady=10)
        self.website_entry = tk.Entry(self.company_frame)
        self.website_entry.grid(row=4,column=3,padx=10,pady=10)

        submit_Btn = tk.Button(self.company_frame,text='Save',command=self.controller.company_submit_btn)
        submit_Btn.grid(row=7,column=2,padx=10,pady=10)
