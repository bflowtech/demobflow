import tkinter as tk
from tkinter import ttk
from src.views.base_view import BaseView

class SubcategoryView(BaseView):
    def __init__(self,subcategory_controller):
        super(SubcategoryView, self).__init__()
        self.title('Sub-Category')
        self.sub_category_frame = tk.Frame(self)
        self.subcategory_controller = subcategory_controller
        self.set_layout()


    def set_layout(self):
        self.sub_category_label = tk.Label(self.sub_category_frame, text='SubCategory', font='50')
        self.sub_category_label.grid(row=0, column=2)
        sub_category_label = tk.Label(self.sub_category_frame,text='Sub Category Name')
        sub_category_label.grid(row=1,column=1)
        self.sub_category_input = tk.Entry(self.sub_category_frame)
        self.sub_category_input.grid(row=1, column=2, padx=10, pady=10)
        self.sub_category_add_btn = tk.Button(self.sub_category_frame, text='Add',command=self.subcategory_controller.add_item)
        self.sub_category_add_btn.grid(row=2, column=3, padx=10, pady=10)

        # # Combobox creation
        self.sub_category_name = tk.StringVar()
        self.sub_category_id = tk.IntVar()
        category_list_label = tk.Label(self.sub_category_frame,text='Select Category')
        category_list_label.grid(row=2,column=1)
        self.category_list = ttk.Combobox(self.sub_category_frame, width=10)
        self.category_list.bind("<<ComboboxSelected>>",self.subcategory_controller.select_category)

        self.category_list.grid(row=2, column=2)

        # define Tree Widget
        self.sub_category_list_view = ttk.Treeview(self.sub_category_frame)

        # format columns
        self.sub_category_list_view['columns'] = ('ID', 'Sub-Category', 'Category', 'Department')
        self.sub_category_list_view.column('#0', width=0, stretch='no')
        self.sub_category_list_view.column('ID', anchor='w', width=120)
        self.sub_category_list_view.column('Sub-Category', anchor='w', width=120)
        self.sub_category_list_view.column('Category', anchor='w', width=120)
        self.sub_category_list_view.column('Department', anchor='w', width=120)

        # create Headings
        self.sub_category_list_view.heading('#0', text='', anchor='w')
        self.sub_category_list_view.heading('ID', text='ID', anchor='w')
        self.sub_category_list_view.heading('Sub-Category', text='Sub-Category', anchor='w')
        self.sub_category_list_view.heading('Category', text='Category', anchor='w')
        self.sub_category_list_view.heading('Department', text='Department', anchor='w')


        self.sub_category_list_view.grid(row=3, column=2)

        self.sub_category_list_view.bind("<ButtonRelease-1>", self.subcategory_controller.select_item)

        self.sub_category_update_btn = tk.Button(self.sub_category_frame, text='Update',command=self.subcategory_controller.update_item)
        self.sub_category_update_btn.grid_forget()

        self.sub_category_edit_btn = tk.Button(self.sub_category_frame, text='Edit',command=self.subcategory_controller.edit_item)
        self.sub_category_edit_btn.grid(row=3, column=3)

        self.sub_category_delete_btn = tk.Button(self.sub_category_frame, text='Delete',command=self.subcategory_controller.delete_category)
        self.sub_category_delete_btn.grid(row=4, column=3)

        # search_box = Entry(sub_category_frame)
        # search_box.grid(row=4, column=2)
        # search_box.bind('<KeyRelease>', serach_sub_category)

        self.sub_category_frame.pack(pady=10)