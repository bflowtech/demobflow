import tkinter as tk
from tkinter import ttk
from src.views.base_view import BaseView

class PurchaseOrderView(BaseView):
    def __init__(self,purchase_order_controller):
        super(PurchaseOrderView,self).__init__()
        self.title('Purchase Order')
        self.purchase_order_frame = tk.Frame(self,width=100,height=200)
        self.controller = purchase_order_controller

        self.purchase_order_frame.pack()
        self.set_layout()

    def set_layout(self):
        tree_frame = self.purchase_order_frame
        header_view = tk.LabelFrame(self.purchase_order_frame,text='Header view')
        header_view.pack(side='top',fill='x')

        order_no_label = tk.Label(header_view,text='Order No')
        order_no_label.grid(row=0,column=0,padx=10,pady=10)
        self.order_no_entry = tk.Entry(header_view)
        self.order_no_entry.grid(row=0,column=1,padx=10,pady=10)


        party_account_label = tk.Label(header_view,text='Party A/C Details')
        party_account_label.grid(row=1,column=0,padx=10,pady=10)
        self.party_account_id = tk.IntVar()
        self.party_account_name = tk.StringVar()
        self.party_account_type = ttk.Combobox(header_view)
        self.party_account_type.grid(row=1,column=1,padx=10,pady=10)
        self.party_account_type.bind("<<ComboboxSelected>>",self.controller.party_account_selected)

        reference_no_label = tk.Label(header_view,text='Reference No')
        reference_no_label.grid(row=2,column=0,padx=10,pady=10)
        self.reference_no_entry = tk.Entry(header_view)
        self.reference_no_entry.grid(row=2,column=1,padx=10,pady=10)




        style = ttk.Style()
        style.theme_use('default')
        style.configure('Treeview',
        background = '#D3D3D3',
        foreground = 'black',
        rowheight = 25,
        fieldbackground = '#D3D3D3'
        )

        style.map('Treeview',
        background=[('selected','#347083')])

        tree_scroll = tk.Scrollbar(tree_frame)
        tree_scroll.pack(side='right',fill='y')

        self.my_tree = ttk.Treeview(tree_frame,yscrollcommand=tree_scroll.set,selectmode='extended')
        self.my_tree.pack()

        tree_scroll.config(command=self.my_tree.yview)

        self.my_tree['columns'] = ('Sl No','Barcode','Batch No','Name','HSN Code','Quantity','Rate','Total Amount')

        self.my_tree.column('#0', width=0, stretch='no')
        self.my_tree.column('Sl No',anchor='w',width=140)
        self.my_tree.column('Barcode',anchor='w',width=140)
        self.my_tree.column('Batch No',anchor='center',width=140)
        self.my_tree.column('Name',anchor='center',width=140)
        self.my_tree.column('HSN Code',anchor='center',width=140)
        self.my_tree.column('Quantity',anchor='center',width=140)
        self.my_tree.column('Rate',anchor='center',width=140)
        self.my_tree.column('Total Amount',anchor='center',width=140,stretch='yes')

        self.my_tree.heading('#0', text='', anchor='w')
        self.my_tree.heading('Sl No',text="Serial No",anchor='w')
        self.my_tree.heading('Barcode',text="Barcode",anchor='w')
        self.my_tree.heading('Batch No',text="Batch No",anchor='center')
        self.my_tree.heading('Name',text="Name Of Item",anchor='center')
        self.my_tree.heading('HSN Code',text="HSN Code",anchor='center')
        self.my_tree.heading('Quantity',text="Quantity",anchor='center')
        self.my_tree.heading('Rate',text="Rate",anchor='center')
        self.my_tree.heading('Total Amount',text="Total Amount",anchor='w')

        self.data = []
        
        self.my_tree.tag_configure('oddrow',background='white')
        self.my_tree.tag_configure('evenrow',background='lightblue')

        #Input
        data_input_frame = tk.LabelFrame(tree_frame,text='Input Data')
        data_input_frame.pack(fill='x',expand='yes',padx=20)

        name_label = tk.Label(data_input_frame,text='Item Name')
        name_label.grid(row=0,column=2,padx=10,pady=10)

        self.item_clicked = tk.StringVar()
        self.item_clicked_id = tk.IntVar()
        self.items_combobox = ttk.Combobox(data_input_frame)
        self.items_combobox.bind('<<ComboboxSelected>>',self.controller.item_selected)
        self.items_combobox.bind('KeyRelease',self.controller.item_onchage)
        self.items_combobox.grid(row=0,column=3,padx=10,pady=10)

        barcode_label = tk.Label(data_input_frame,text='Barcode')
        barcode_label.grid(row=0,column=4,padx=10,pady=10)
        self.barcode_entry = tk.Entry(data_input_frame)
        self.barcode_entry.grid(row=0,column=5,padx=10,pady=10)

        batch_no_label = tk.Label(data_input_frame,text='Batch No')
        batch_no_label.grid(row=0,column=6,padx=10,pady=10)
        self.batch_no_entry = tk.Entry(data_input_frame)
        self.batch_no_entry.grid(row=0,column=7,padx=10,pady=10)


        hsn_code_label = tk.Label(data_input_frame,text='HSN CODE')
        hsn_code_label.grid(row=0,column=8,padx=10,pady=10)
        self.hsn_entry = tk.Entry(data_input_frame)
        self.hsn_entry.grid(row=0,column=9,padx=10,pady=10)

        rate_label = tk.Label(data_input_frame,text='Rate')
        rate_label.grid(row=0,column=10,padx=10,pady=10)
        self.rate_entry = tk.Entry(data_input_frame)
        self.rate_entry.grid(row=0,column=11,padx=10,pady=10)

        qty_label = tk.Label(data_input_frame,text='Quantity')
        qty_label.grid(row=0,column=12,padx=10,pady=10)
        self.qty_entry = tk.Entry(data_input_frame)
        self.qty_entry.grid(row=0,column=13,padx=10,pady=10)
        self.qty_entry.bind("<KeyRelease>", self.controller.on_qty_changed)
        

        total_amount_label = tk.Label(data_input_frame,text='Total Amount')
        total_amount_label.grid(row=1,column=2,padx=10,pady=10)
        self.total_amount_entry = tk.Entry(data_input_frame)
        self.total_amount_entry.grid(row=1,column=3,padx=10,pady=10)
        

        add_button = tk.Button(data_input_frame,text='Add',command=self.controller.add_to_order_list)
        add_button.grid(row=1,column=12,padx=10,pady=10)

        remove_button = tk.Button(data_input_frame,text='Remove',command=self.controller.remove_from_order_list)
        remove_button.grid(row=1,column=13,padx=10,pady=10)

        generate_orderbtn = tk.Button(self.purchase_order_frame,text='Generate Order',command=self.controller.create_order)
        generate_orderbtn.pack(side='right')










