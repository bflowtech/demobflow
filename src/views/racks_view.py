import tkinter as tk
from tkinter import ttk
from src.views.base_view import BaseView

class RacksView(BaseView):
    def __init__(self,racks_controller):
        super(RacksView, self).__init__()
        self.title('Racks')
        self.racks_frame = tk.Frame(self)
        self.racks_controller = racks_controller
        self.set_layout()

    def set_layout(self):
        self.racks_label = tk.Label(self.racks_frame, text='RACK', font='50')
        self.racks_label.grid(row=0, column=2)

        rack_name_label = tk.Label(self.racks_frame,text='Rack Name')
        rack_name_label.grid(row=1,column=1)
        
        self.racks_name_input = tk.Entry(self.racks_frame)
        self.racks_name_input.grid(row=1,column=2)

        rack_starts_label = tk.Label(self.racks_frame,text='Rack Starts')
        rack_starts_label.grid(row=2,column=1)

        self.rack_starts_input = tk.Entry(self.racks_frame)
        self.rack_starts_input.grid(row=2,column=2)
        
        rack_ends_label = tk.Label(self.racks_frame,text='Rack Ends')
        rack_ends_label.grid(row=3,column=1)

        self.rack_ends_input = tk.Entry(self.racks_frame)
        self.rack_ends_input.grid(row=3,column=2)

        self.racks_add_btn = tk.Button(self.racks_frame, text='Add',command=self.racks_controller.add_item)
        self.racks_add_btn.grid(row=2, column=3)

        # define Tree Widget
        self.racks_list_view = ttk.Treeview(self.racks_frame)

        # format columns
        self.racks_list_view['columns'] = ('ID', 'Rack Name', 'Starts', 'Ends')
        self.racks_list_view.column('#0', width=0, stretch='no')
        self.racks_list_view.column('ID', anchor='w', width=120)
        self.racks_list_view.column('Rack Name', anchor='w', width=120)
        self.racks_list_view.column('Starts', anchor='w', width=120)
        self.racks_list_view.column('Ends', anchor='w', width=120)

        # create Headings
        self.racks_list_view.heading('#0', text='', anchor='w')
        self.racks_list_view.heading('ID', text='ID', anchor='w')
        self.racks_list_view.heading('Rack Name', text='Rack Name', anchor='w')
        self.racks_list_view.heading('Starts', text='Starts', anchor='w')
        self.racks_list_view.heading('Ends', text='Ends', anchor='w')

        self.racks_list_view.grid(row=5, column=2,pady=10)

        self.racks_list_view.bind("<ButtonRelease-1>", self.racks_controller.select_item)

        self.racks_update_btn = tk.Button(self.racks_frame, text='Update',command=self.racks_controller.update_item)
        self.racks_update_btn.grid_forget()

        self.racks_edit_btn = tk.Button(self.racks_frame, text='Edit',command=self.racks_controller.edit_item)
        self.racks_edit_btn.grid(row=5, column=3)

        self.racks_delete_btn = tk.Button(self.racks_frame, text='Delete',command=self.racks_controller.delete_item)
        self.racks_delete_btn.grid(row=6, column=3)

        # self.search_box = tk.Entry(self.racks_frame)
        # self.search_box.grid(row=4, column=2)

        # search_box.bind('<KeyRelease>', serach_units)

        self.racks_frame.pack(pady=10)