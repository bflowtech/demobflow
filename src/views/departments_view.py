import tkinter as tk
from tkinter import ttk
from src.views.base_view import BaseView

class DepartmentsView(BaseView):
    def __init__(self,departments_controller):
        super(DepartmentsView, self).__init__()
        self.title('Departments')
        self.departments_controller = departments_controller
        self.departments_frame = tk.Frame(self)
        self.set_layout()

    def set_layout(self):
        department_label = tk.Label(self.departments_frame, text='Department Management', font='50')
        department_label.grid(row=0, column=2)

        department_name_label = tk.Label(self.departments_frame,text='Department Name')
        department_name_label.grid(row=1,column=1)

        self.department_input = tk.Entry(self.departments_frame)
        self.department_input.grid(row=1, column=2, padx=10, pady=10)
        self.department_add_btn = tk.Button(self.departments_frame, text='Add',command=lambda :self.departments_controller.add_item())
        self.department_add_btn.grid(row=1, column=3, padx=10, pady=10)

        # define Tree Widget
        self.department_list_view = ttk.Treeview(self.departments_frame)

        # format columns
        self.department_list_view['columns'] = ('ID', 'Department Name')
        self.department_list_view.column('#0', width=0, stretch='no')
        self.department_list_view.column('ID', anchor='w', width=120)
        self.department_list_view.column('Department Name', anchor='w', width=120)
        # create Headings
        self.department_list_view.heading('#0', text='', anchor='w')
        self.department_list_view.heading('ID', text='ID', anchor='w')
        self.department_list_view.heading('Department Name', text='Department Name', anchor='w')


        self.department_list_view.grid(row=2, column=2)

        self.department_list_view.bind("<ButtonRelease-1>", self.departments_controller.select_item)

        self.department_update_btn = tk.Button(self.departments_frame, text='Update',command=self.departments_controller.update_item)
        self.department_update_btn.grid_forget()

        self.department_edit_btn = tk.Button(self.departments_frame, text='Edit',command=self.departments_controller.edit_item)
        self.department_edit_btn.grid(row=2, column=3)

        self.department_delete_btn = tk.Button(self.departments_frame, text='Delete',command=self.departments_controller.delete_item)
        self.department_delete_btn.grid(row=3, column=3)

        self.departments_frame.pack(pady=10)

        self.search_box = tk.Entry(self.departments_frame)
        self.search_box.grid(row=3, column=2)
        self.search_box.bind('<KeyRelease>', self.departments_controller.serach_departments)