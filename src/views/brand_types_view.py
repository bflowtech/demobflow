from src.views.base_view import BaseView
import tkinter as tk
from tkinter import ttk

class BrandTypesView(BaseView):
    def __init__(self,brands_types_controller):
        super(BrandTypesView, self).__init__()
        self.title('Brand Types')
        self.brands_types_frame = tk.Frame(self)
        self.brands_types_controller = brands_types_controller
        self.set_layout()


    def set_layout(self):
        self.brand_name_label = tk.Label(self.brands_types_frame, text='Brand Types', font='50')
        self.brand_name_label.grid(row=0, column=2)

        brand_name_label = tk.Label(self.brands_types_frame,text='BrandTypes Name')
        brand_name_label.grid(row=1,column=1)
        self.brands_type_input = tk.Entry(self.brands_types_frame)
        self.brands_type_input.grid(row=1, column=2)
        self.brands_type_add_btn = tk.Button(self.brands_types_frame,text='Add',command=self.brands_types_controller.add_item)
        self.brands_type_add_btn.grid(row=1, column=3, padx=10, pady=10)

        # define Tree Widget
        self.brands_type_list_view = ttk.Treeview(self.brands_types_frame )

        # format columns

        self.brands_type_list_view['columns'] = ('ID', 'Brand Type')
        self.brands_type_list_view.column('#0', width=0, stretch='no')
        self.brands_type_list_view.column('ID', anchor='w', width=120)
        self.brands_type_list_view.column('Brand Type', anchor='w', width=120)

        # create Headings

        self.brands_type_list_view.heading('#0', text='', anchor='w')
        self.brands_type_list_view.heading('ID', text='ID', anchor='w')
        self.brands_type_list_view.heading('Brand Type', text='Brand Type', anchor='w')


        self.brands_type_list_view.grid(row=2, column=2)

        self.brands_type_list_view.bind("<ButtonRelease-1>",self.brands_types_controller.select_item)

        self.brands_type_update_btn = tk.Button(self.brands_types_frame,text='Update',command=self.brands_types_controller.update_item)
        self.brands_type_update_btn.grid_forget()

        self.brands_type_edit_btn = tk.Button(self.brands_types_frame ,text='Edit',command=self.brands_types_controller.edit_item)
        self.brands_type_edit_btn.grid(row=2, column=3)

        self.brands_type_delete_btn = tk.Button(self.brands_types_frame,text='Delete',command=self.brands_types_controller.delete_item)
        self.brands_type_delete_btn.grid(row=3, column=3)

        self.search_box = tk.Entry(self.brands_types_frame)
        self.search_box.grid(row=3, column=2)
        # self.search_box.bind('<KeyRelease>', self.brands_controller.serach_brands)

        
        
        self.brands_types_frame.pack(pady=10)

        

