import tkinter as tk
from tkinter import ttk
from src.views.base_view import BaseView

class GstMasterView(BaseView):
    def __init__(self, gst_master_controller):
        super(GstMasterView, self).__init__()
        self.title('GstMaster')
        self.gst_master_frame = tk.Frame(self)
        self.gst_master_controller = gst_master_controller
        self.set_layout()

    def set_layout(self):
        gst_master_label = tk.Label(self.gst_master_frame, text='GST MASTER', font='50')
        gst_master_label.grid(row=0, column=2)

        gst_name_label = tk.Label(self.gst_master_frame,text='Enter Gst Name')
        gst_name_label.grid(row=1,column=1)

        self.gst_name_input = tk.Entry(self.gst_master_frame)
        self.gst_name_input.grid(row=1, column=2)
        
        
        gst_percentage_label = tk.Label(self.gst_master_frame,text='Enter Gst %')
        gst_percentage_label.grid(row=2,column=1)
        
        self.gst_percentage_input = tk.Entry(self.gst_master_frame)
        self.gst_percentage_input.grid(row=2, column=2)

        self.gst_add_btn = tk.Button(self.gst_master_frame, text='Add',command=self.gst_master_controller.add_item)
        self.gst_add_btn.grid(row=1, column=3, padx=10, pady=10)

        # define Tree Widget
        self.gst_list_view = ttk.Treeview(self.gst_master_frame)

        # format columns
        self.gst_list_view['columns'] = ('ID', 'GST Name', 'GST %')

        self.gst_list_view.column('#0', width=0, stretch='no')
        self.gst_list_view.column('ID', anchor='w', width=120)
        self.gst_list_view.column('GST Name', anchor='w', width=120)
        self.gst_list_view.column('GST %', anchor='w', width=120)
        # create Headings
        self.gst_list_view.heading('#0', text='', anchor='w')
        self.gst_list_view.heading('ID', text='ID', anchor='w')
        self.gst_list_view.heading('GST Name', text='GST Name', anchor='w')
        self.gst_list_view.heading('GST %', text='GST %', anchor='w')

        self.gst_list_view.grid(row=4, column=2)

        self.gst_list_view.bind("<ButtonRelease-1>", self.gst_master_controller.select_item)

        self.gst_update_btn = tk.Button(self.gst_master_frame, text='Update',command=self.gst_master_controller.update_item)
        self.gst_update_btn.grid_forget()

        self.gst_edit_btn = tk.Button(self.gst_master_frame, text='Edit',command=self.gst_master_controller.edit_item)
        self.gst_edit_btn.grid(row=3, column=3)

        self.gst_delete_btn = tk.Button(self.gst_master_frame, text='Delete',command=self.gst_master_controller.delete_item)
        self.gst_delete_btn.grid(row=4, column=3)


        # search_box = tk.Entry(department_frame)
        # search_box.grid(row=3, column=2)
        # search_box.bind('<KeyRelease>', serach_departments)

        self.gst_master_frame.pack(pady=10)
