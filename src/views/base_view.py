from abc import ABC,abstractmethod
import tkinter as tk

class BaseView(tk.Tk,ABC):
    def __init__(self):
        super(BaseView, self).__init__()
        self.geometry('1000x1080')
        self.bind('<Motion>',self.detect_motion)


    def detect_motion(self,event):
        x, y = event.x, event.y
        # print('{}, {}'.format(x, y))
        



    @abstractmethod
    def set_layout(self):
        pass


