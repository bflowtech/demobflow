from src.views.base_view import BaseView
import tkinter as tk
from tkinter import ttk

class AccountsView(BaseView):
    def __init__(self, accounts_controller):
        super(AccountsView, self).__init__()
        self.title('Accounts Group')
        self.accounts_frame = tk.Frame(self)
        self.accounts_controller = accounts_controller
        self.set_layout()

    
    def set_layout(self):
        asset_btn = tk.Button(self.accounts_frame, text='ASSET', height=6,width=10,command=self.accounts_controller.switch_to_assets_view)
        asset_btn.grid(row=1, column=1, padx=6, pady=6)

        liablity_btn = tk.Button(self.accounts_frame, text='LIABLITY', height=6,width=10,command=self.accounts_controller.switch_to_liablity_view)
        liablity_btn.grid(row=1, column=2, padx=6, pady=6)

        income_btn = tk.Button(self.accounts_frame, text='INCOME', height=6,width=10,command=self.accounts_controller.switch_to_income_view)
        income_btn.grid(row=2, column=1, padx=6, pady=6)

        expense_btn = tk.Button(self.accounts_frame, text='EXPENSE', height=6,width=10,command=self.accounts_controller.switch_to_expense_view)
        expense_btn.grid(row=2, column=2, padx=6, pady=6)

        ledger_btn = tk.Button(self.accounts_frame, text='LEDGER', height=6,width=10,command=self.accounts_controller.switch_to_ledger_view)
        ledger_btn.grid(row=3, column=1, padx=6, pady=6)

        self.accounts_frame.pack(pady=10)
        