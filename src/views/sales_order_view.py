import tkinter as tk
from tkinter import ttk
from src.views.base_view import BaseView

class SalesOrderView(BaseView):
    def __init__(self,sales_order_controller):
        super(SalesOrderView,self).__init__()
        self.title('Sales Order View')
        self.sales_order_frame = tk.Frame(self)
        self.controller = sales_order_controller
        self.sales_order_frame.pack()

        self.set_layout()

    def set_layout(self):
        tree_frame = self.sales_order_frame

        header_view = tk.LabelFrame(self.sales_order_frame,text='Header view')
        header_view.pack(side='top',fill='x')

        sales_voucher_no_label = tk.Label(header_view,text='Sales Voucher No')
        sales_voucher_no_label.grid(row=0,column=0,padx=10,pady=10)
        self.sales_voucher_entry = tk.Entry(header_view)
        self.sales_voucher_entry.grid(row=0,column=1,padx=10,pady=10)

        party_account_label = tk.Label(header_view,text='Party A/C Details')
        party_account_label.grid(row=1,column=0,padx=10,pady=10)
        self.party_account_type = ttk.Combobox(header_view)
        self.party_account_type.grid(row=1,column=1,padx=10,pady=10)

        reference_no_label = tk.Label(header_view,text='Reference No')
        reference_no_label.grid(row=2,column=0,padx=10,pady=10)
        self.reference_no_entry = tk.Entry(header_view)
        self.reference_no_entry.grid(row=2,column=1,padx=10,pady=10)

        order_no_label = tk.Label(header_view,text='Order No')
        order_no_label.grid(row=0,column=2)
        self.order_no_entry = tk.Entry(header_view)
        self.order_no_entry.grid(row=0,column=3)


        bill_type_label = tk.Label(header_view,text='Bill Type')
        bill_type_label.grid(row=1,column=2)
        self.bill_type = ttk.Combobox(header_view)
        self.bill_type.grid(row=1,column=3)




        style = ttk.Style()
        style.theme_use('default')
        style.configure('Treeview',
        background='#D3D3D3',
        foreground='black',
        rowheight=25,
        fieldbackground='#D3D3D3'
        )

        style.map('Treeview',
        background=[('selected','#347083')])

        tree_scroll = tk.Scrollbar(tree_frame)
        tree_scroll.pack(side='right',fill='y')

        self.sales_order_view = ttk.Treeview(tree_frame,yscrollcommand=tree_scroll.set,selectmode='extended')
        self.sales_order_view.pack()

        tree_scroll.config(command=self.sales_order_view.yview)

        self.sales_order_view['columns'] = ('Sl No','Barcode','Batch No','Name','HSN Code','Quantity','Rate',
                                            'Discount','Taxable','Cgst','Sgst','Igst','Cess','Total Amount')

        self.sales_order_view.column('#0', width=0, stretch='no')
        self.sales_order_view.column('Sl No',anchor='w',width=140)
        self.sales_order_view.column('Barcode',anchor='w',width=140)
        self.sales_order_view.column('Batch No',anchor='center',width=140)
        self.sales_order_view.column('Name',anchor='center',width=140)
        self.sales_order_view.column('HSN Code',anchor='center',width=140)
        self.sales_order_view.column('Quantity',anchor='center',width=140)
        self.sales_order_view.column('Rate',anchor='center',width=140)
        self.sales_order_view.column('Discount',anchor='center',width=140)
        self.sales_order_view.column('Taxable',anchor='center',width=140)
        self.sales_order_view.column('Cgst',anchor='center',width=100)
        self.sales_order_view.column('Sgst',anchor='center',width=100)
        self.sales_order_view.column('Igst',anchor='center',width=100)
        self.sales_order_view.column('Cess',anchor='center',width=100)
        self.sales_order_view.column('Total Amount',anchor='center',width=140,stretch='yes')

        self.sales_order_view.heading('#0', text='', anchor='w')
        self.sales_order_view.heading('Sl No',text="Serial No",anchor='w')
        self.sales_order_view.heading('Barcode',text="Barcode",anchor='w')
        self.sales_order_view.heading('Batch No',text="Batch No",anchor='center')
        self.sales_order_view.heading('Name',text="Name Of Item",anchor='center')
        self.sales_order_view.heading('HSN Code',text="HSN Code",anchor='center')
        self.sales_order_view.heading('Quantity',text="Quantity",anchor='center')
        self.sales_order_view.heading('Rate',text="Rate",anchor='center')
        self.sales_order_view.heading('Discount',text="Discount",anchor='center')
        self.sales_order_view.heading('Taxable',text="Taxble",anchor='center')
        self.sales_order_view.heading('Cgst',text="CGST",anchor='center')
        self.sales_order_view.heading('Sgst',text="SGST",anchor='center')
        self.sales_order_view.heading('Igst',text="IGST",anchor='center')
        self.sales_order_view.heading('Cess',text="CESS",anchor='center')
        self.sales_order_view.heading('Total Amount',text="Total Amount",anchor='w')


        self.data = []


        self.sales_order_view.tag_configure('oddrow',background='white')
        self.sales_order_view.tag_configure('evenrow',background='lightblue')


        #Input
        data_input_frame = tk.LabelFrame(tree_frame,text='Input Data')
        data_input_frame.pack(fill='x',expand='yes',padx=20)

        name_label = tk.Label(data_input_frame,text='Item Name')
        name_label.grid(row=0,column=0,padx=10,pady=10)

        self.item_clicked = tk.StringVar()
        self.item_clicked_id = tk.IntVar()
        self.items_combobox = ttk.Combobox(data_input_frame)
        self.items_combobox.bind('<KeyRelease>',self.controller.item_onchage)
        self.items_combobox.bind("<<ComboboxSelected>>", self.controller.item_selected)
        self.items_combobox.grid(row=0,column=1,padx=10,pady=10)

        barcode_label = tk.Label(data_input_frame,text='Barcode')
        barcode_label.grid(row=0,column=2,padx=10,pady=10)
        self.barcode_entry = tk.Entry(data_input_frame)
        self.barcode_entry.grid(row=0,column=3,padx=10,pady=10)

        batch_no_label = tk.Label(data_input_frame,text='Batch No')
        batch_no_label.grid(row=0,column=4,padx=10,pady=10)
        self.batch_no_entry = tk.Entry(data_input_frame)
        self.batch_no_entry.grid(row=0,column=5,padx=10,pady=10)


        hsn_code_label = tk.Label(data_input_frame,text='HSN CODE')
        hsn_code_label.grid(row=0,column=6,padx=10,pady=10)
        self.hsn_entry = tk.Entry(data_input_frame)
        self.hsn_entry.grid(row=0,column=7,padx=10,pady=10)

        rate_label = tk.Label(data_input_frame,text='Rate')
        rate_label.grid(row=0,column=8,padx=10,pady=10)
        self.rate_entry = tk.Entry(data_input_frame)
        self.rate_entry.grid(row=0,column=9,padx=10,pady=10)

        qty_label = tk.Label(data_input_frame,text='Quantity')
        qty_label.grid(row=0,column=10,padx=10,pady=10)
        self.qty_entry = tk.Entry(data_input_frame)
        self.qty_entry.grid(row=0,column=11,padx=10,pady=10)
        self.qty_entry.bind("<KeyRelease>", self.controller.on_qty_changed)
        
        disc_label = tk.Label(data_input_frame,text='Discount')
        disc_label.grid(row=0,column=12,padx=10,pady=10)
        self.disc_entry = tk.Entry(data_input_frame)
        self.disc_entry.grid(row=0,column=13,padx=10,pady=10)
        # self.disc_entry.bind("<KeyRelease>", self.controller.on_disc_changed)

        taxable_label = tk.Label(data_input_frame,text='Taxable Amount')
        taxable_label.grid(row=1,column=0,padx=10,pady=10)
        self.taxable_entry = tk.Entry(data_input_frame)
        self.taxable_entry.grid(row=1,column=1,padx=10,pady=10)

        cgst_label = tk.Label(data_input_frame,text='CGST')
        cgst_label.grid(row=1,column=2,padx=10,pady=10)
        self.cgst_entry = tk.Entry(data_input_frame)
        self.cgst_entry.grid(row=1,column=3,padx=10,pady=10)

        sgst_label = tk.Label(data_input_frame,text='SGST')
        sgst_label.grid(row=1,column=4,padx=10,pady=10)
        self.sgst_entry = tk.Entry(data_input_frame)
        self.sgst_entry.grid(row=1,column=5,padx=10,pady=10)

        igst_label = tk.Label(data_input_frame,text='IGST')
        igst_label.grid(row=1,column=6,padx=10,pady=10)
        self.igst_entry = tk.Entry(data_input_frame)
        self.igst_entry.grid(row=1,column=7,padx=10,pady=10)

        
        cess_label = tk.Label(data_input_frame,text='CESS')
        cess_label.grid(row=1,column=8,padx=10,pady=10)
        self.cess_entry = tk.Entry(data_input_frame)
        self.cess_entry.grid(row=1,column=9,padx=10,pady=10)

        total_amount_label = tk.Label(data_input_frame,text='Total Amount')
        total_amount_label.grid(row=1,column=10,padx=10,pady=10)
        self.total_amount_entry = tk.Entry(data_input_frame)
        self.total_amount_entry.grid(row=1,column=11,padx=10,pady=10)
        

        add_button = tk.Button(data_input_frame,text='Add',command=self.controller.add_to_sale_order_list)
        add_button.grid(row=1,column=12,padx=10,pady=10)

        remove_button = tk.Button(data_input_frame,text='Remove',command=self.controller.remove_from_sales_order_list)
        remove_button.grid(row=1,column=13,padx=10,pady=10)

        generate_orderbtn = tk.Button(self.sales_order_frame,text='Generate Order',command=self.controller.create_sale_order)
        generate_orderbtn.pack(side='right')
