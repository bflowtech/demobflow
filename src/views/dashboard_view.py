import tkinter as tk
from src.views.base_view import BaseView

class DashboardView(BaseView):
    def __init__(self,dashboard_controller):
        super(DashboardView, self).__init__()
        self.title('Dashboard')
        self.dashbord_frame = tk.Frame(self)
        self.controller = dashboard_controller
        self.set_layout()


    def set_layout(self):
        dashboard_label = tk.Label(self.dashbord_frame, text='DASHBOARD', font='50')
        dashboard_label.grid(row=0, columnspan=13)

        brand_types_btn = tk.Button(self.dashbord_frame, text='Brand Types', height=6, width=10,command=self.controller.switch_to_brand_types)
        brand_types_btn.grid(row=2, column=1, padx=6, pady=6)

        brands_btn = tk.Button(self.dashbord_frame, text='Brands', height=6, width=10,command=self.controller.swith_to_brands)
        brands_btn.grid(row=2, column=2, padx=6, pady=6)

        department_btn = tk.Button(self.dashbord_frame, text='Department', height=6, width=10,command=self.controller.switch_to_departments)
        department_btn.grid(row=2, column=3, padx=6, pady=6)

        category_btn = tk.Button(self.dashbord_frame, text='Category', height=6, width=10,command=self.controller.switch_to_category)
        category_btn.grid(row=2, column=4, padx=6, pady=6)

        sub_category_btn = tk.Button(self.dashbord_frame, text='Sub-Category', height=6, width=10,command=self.controller.switch_to_sub_category)
        sub_category_btn.grid(row=2, column=5, padx=6, pady=6)

        unit_btn = tk.Button(self.dashbord_frame, text='Unit', height=6, width=10,command=self.controller.switch_to_units)
        unit_btn.grid(row=3, column=2, padx=6, pady=6)

        rack_btn = tk.Button(self.dashbord_frame, text='Rack', height=6, width=10,command=self.controller.switch_to_racks)
        rack_btn.grid(row=3, column=3, padx=6, pady=6)

        gstmaster_btn = tk.Button(self.dashbord_frame, text='GST Master', height=6, width=10,command=self.controller.switch_to_gst_master)
        gstmaster_btn.grid(row=3, column=4, padx=6, pady=6)

        item_master_btn = tk.Button(self.dashbord_frame, text='Item Master', height=6, width=10,command=self.controller.switch_to_item_master)
        item_master_btn.grid(row=3, column=5, padx=6, pady=6)


        cess_master_btn = tk.Button(self.dashbord_frame,text='Cess Master',height=6, width=10,command=self.controller.switch_to_cess_master)
        cess_master_btn.grid(row=4, column=2, padx=6, pady=6)

        purchase_btn = tk.Button(self.dashbord_frame,text='Purchase Order',height=6, width=15,command=self.controller.switch_to_purchase_btn)
        purchase_btn.grid(row=4, column=3, padx=6, pady=6)

        purchase_voucher_btn = tk.Button(self.dashbord_frame,text='Purchase Voucher',height=6, width=15,command=self.controller.switch_to_purchase_voucher)
        purchase_voucher_btn.grid(row=4, column=4, padx=6, pady=6)

        sales_order_btn = tk.Button(self.dashbord_frame,text='Sales Order',height=6, width=15,command=self.controller.switch_to_sales_order)
        sales_order_btn.grid(row=5,column=3,padx=6,pady=6)

        sales_voucher_btn = tk.Button(self.dashbord_frame,text='Sales Voucher',height=6, width=15,command=self.controller.switch_to_sales_voucher)
        sales_voucher_btn.grid(row=5,column=4,padx=6,pady=6)

        accounts_btn = tk.Button(self.dashbord_frame,text='Accounts',height=6, width=10,command=self.controller.switch_to_accounts)
        accounts_btn.grid(row=6, column=4, padx=6, pady=6)

        reports_btn = tk.Button(self.dashbord_frame,text='Stock Reports',height=6, width=10,command=self.controller.switch_to_reports)
        reports_btn.grid(row=6,column=3,padx=6, pady=6)

        company_btn = tk.Button(self.dashbord_frame,text='Company \n Details',height=6, width=10,command=self.controller.switch_to_company_details)
        company_btn.grid(row=6,column=1,padx=6,pady=6)


        self.dashbord_frame.pack(pady=10)


        




