from src.views.base_view import BaseView
import tkinter as tk
from tkinter import ttk

class BrandsView(BaseView):
    def __init__(self,brands_controller):
        super(BrandsView, self).__init__()
        self.title('Brands')
        self.brands_frame = tk.Frame(self)
        self.controller = brands_controller
        self.set_layout()
    

    def set_layout(self):
        self.brand_name_label = tk.Label(self.brands_frame, text='Brand Management', font='50')
        self.brand_name_label.grid(row=0, column=2)

        brand_name_label = tk.Label(self.brands_frame,text='Brand Name')
        brand_name_label.grid(row=1,column=1)
        self.brands_input = tk.Entry(self.brands_frame)
        self.brands_input.grid(row=1, column=2)
        self.brands_add_btn = tk.Button(self.brands_frame,text='Add', command=lambda :self.controller.add_item(self.brands_input.get()))
        self.brands_add_btn.grid(row=1, column=4, padx=10, pady=10)

        # define Tree Widget
        self.brands_list_view = ttk.Treeview(self.brands_frame)

        # format columns
        self.brands_list_view['columns'] = ('ID', 'Brand Name','Type')
        self.brands_list_view.column('#0', width=0, stretch='no')
        self.brands_list_view.column('ID', anchor='w', width=120)
        self.brands_list_view.column('Brand Name', anchor='w', width=120)
        self.brands_list_view.column('Type', anchor='w', width=120)

        # create Headings
        self.brands_list_view.heading('#0', text='', anchor='w')
        self.brands_list_view.heading('ID', text='ID', anchor='w')
        self.brands_list_view.heading('Brand Name', text='Brand Name', anchor='w')
        self.brands_list_view.heading('Type', text='Brand Type', anchor='w')


        self.brands_list_view.grid(row=2, column=2)

        self.brands_list_view.bind("<ButtonRelease-1>",self.controller.select_item)

        self.brands_update_btn = tk.Button(self.brands_frame,text='Update',command=self.controller.update_item)
        self.brands_update_btn.grid_forget()

        self.brands_edit_btn = tk.Button(self.brands_frame ,text='Edit',command=self.controller.edit_item)
        self.brands_edit_btn.grid(row=2, column=3)

        self.brands_delete_btn = tk.Button(self.brands_frame,text='Delete',command=self.controller.delete_item)
        self.brands_delete_btn.grid(row=3, column=3)

        self.search_box = tk.Entry(self.brands_frame)
        self.search_box.grid(row=3, column=2)
        self.search_box.bind('<KeyRelease>', self.controller.serach_brands)


        # Combobox creation
        self.brandtype_clicked = tk.StringVar()
        self.brandtype_clicked_id = tk.IntVar()
        self.brand_types_dropdown = ttk.Combobox(self.brands_frame, width=20, textvariable=self.brandtype_clicked)
        self.brand_types_dropdown.bind("<<ComboboxSelected>>", self.controller.brand_type_selected)
        self.brand_types_dropdown.grid(row=1, column=3)
        self.brands_frame.pack(pady=10)
