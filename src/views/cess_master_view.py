import tkinter as tk
from tkinter import ttk

from src.views.base_view import BaseView

class CessMasterView(BaseView):
    def __init__(self,cess_master_controller):
        super(CessMasterView,self).__init__()
        self.title('Cess Master')
        self.cess_master_frame = tk.Frame(self)
        self.cess_master_controller = cess_master_controller
        self.set_layout()

    
    def set_layout(self):
        cess_master_label = tk.Label(self.cess_master_frame, text='CESS MASTER', font='50')
        cess_master_label.grid(row=0, column=2)

        cess_name_label = tk.Label(self.cess_master_frame,text='Enter Cess Name')
        cess_name_label.grid(row=1,column=1)

        self.cess_name_input = tk.Entry(self.cess_master_frame)
        self.cess_name_input.grid(row=1, column=2)
        
        
        cess_percentage_label = tk.Label(self.cess_master_frame,text='Enter Cess %')
        cess_percentage_label.grid(row=2,column=1)
        
        self.cess_percentage_input = tk.Entry(self.cess_master_frame)
        self.cess_percentage_input.grid(row=2, column=2)

        self.cess_add_btn = tk.Button(self.cess_master_frame, text='Add',command=self.cess_master_controller.add_item)
        self.cess_add_btn.grid(row=1, column=3, padx=10, pady=10)

        # define Tree Widget
        self.cess_list_view = ttk.Treeview(self.cess_master_frame)

        # format columns
        self.cess_list_view['columns'] = ('ID', 'Cess Name', 'Cess %')

        self.cess_list_view.column('#0', width=0, stretch='no')
        self.cess_list_view.column('ID', anchor='w', width=120)
        self.cess_list_view.column('Cess Name', anchor='w', width=120)
        self.cess_list_view.column('Cess %', anchor='w', width=120)
        # create Headings
        self.cess_list_view.heading('#0', text='', anchor='w')
        self.cess_list_view.heading('ID', text='ID', anchor='w')
        self.cess_list_view.heading('Cess Name', text='Cess Name', anchor='w')
        self.cess_list_view.heading('Cess %', text='Cess %', anchor='w')

        self.cess_list_view.grid(row=5, column=2)

        self.cess_list_view.bind("<ButtonRelease-1>", self.cess_master_controller.select_item)

        self.cess_update_btn = tk.Button(self.cess_master_frame, text='Update',command=self.cess_master_controller.update_item)
        self.cess_update_btn.grid_forget()

        self.cess_edit_btn = tk.Button(self.cess_master_frame, text='Edit',command=self.cess_master_controller.edit_item)
        self.cess_edit_btn.grid(row=5, column=3)

        self.cess_delete_btn = tk.Button(self.cess_master_frame, text='Delete',command=self.cess_master_controller.delete_item)
        self.cess_delete_btn.grid(row=6, column=3)


        # search_box = tk.Entry(department_frame)
        # search_box.grid(row=3, column=2)
        # search_box.bind('<KeyRelease>', serach_departments)

        self.cess_master_frame.pack(pady=10)
