import tkinter as tk
from tkinter import ttk
from src.views.base_view import BaseView
from tkcalendar import DateEntry

from src.controllers.cess_master_controller import CessMasterController
from src.views.cess_master_view import CessMasterView
from src.models.cess_master_model import CessMasterModel


class ItemMasterView(BaseView):
    def __init__(self,item_master_controller):
        super(ItemMasterView, self).__init__()
        self.title('Item Master')
        self.item_master_frame = tk.Frame(self)
        self.controller = item_master_controller

    

        self.set_layout()


    def set_layout(self):
        item_master_label = tk.Label(self.item_master_frame,text='Item Master',font=50)
        item_master_label.grid(row=1,column=2)

        # self.barcode_generateimg= tk.PhotoImage(file='barcode.png')
        barcode_label = tk.Label(self.item_master_frame,text='Barcode')
        barcode_label.grid(row=2,column=1)
        self.barcode = tk.Entry(self.item_master_frame,state='readonly')
        self.barcode.grid(row=2,column=2)
        barcode_generate_btn = tk.Button(self.item_master_frame,text='gen',borderwidth=0,command=self.controller.generate_barcode)
        barcode_generate_btn.grid(row=2,column=3)

        # self.add_btn_image = tk.PhotoImage(file='add_btn.png')

        item_code_label = tk.Label(self.item_master_frame,text='Item Code')
        item_code_label.grid(row=4,column=1)
        self.item_code = tk.Entry(self.item_master_frame)
        self.item_code.grid(row=4,column=2)

        hsn_code_label = tk.Label(self.item_master_frame,text='HSN CODE')
        hsn_code_label.grid(row=5,column=1)
        self.hsn_code = tk.Entry(self.item_master_frame)
        self.hsn_code.grid(row=5,column=2)

        item_name_label = tk.Label(self.item_master_frame,text='Item Name')
        item_name_label.grid(row=6,column=1)
        self.item_name = tk.Entry(self.item_master_frame)
        self.item_name.grid(row=6,column=2)

        unit_label = tk.Label(self.item_master_frame,text='Unit')
        unit_label.grid(row=7,column=1)
        self.unit_clicked = tk.StringVar()
        self.unit_clicked_id = tk.IntVar()
        self.unit_input = ttk.Combobox(self.item_master_frame)
        self.unit_input.grid(row=7,column=2)
        self.unit_input.bind('<KeyRelease>', self.controller.units_onchange)
        self.unit_input.bind("<<ComboboxSelected>>", self.controller.units_selected)

        add_unit_btn = tk.Button(self.item_master_frame,command=self.unit_popup,borderwidth=0)
        add_unit_btn.grid(row=7,column=3)
        

        brand_label = tk.Label(self.item_master_frame, text='Brands')
        brand_label.grid(row=8, column=1)
        self.brand_clicked = tk.StringVar()
        self.brand_clicked_id = tk.IntVar()
        self.brand_list = ttk.Combobox(self.item_master_frame)
        self.brand_list.grid(row=8, column=2)
        self.brand_list.bind('<KeyRelease>', self.controller.brands_onchange)
        self.brand_list.bind("<<ComboboxSelected>>", self.controller.brands_selected)
        
        add_brand_btn = tk.Button(self.item_master_frame,command=self.brands_popup,borderwidth=0)
        add_brand_btn.grid(row=8, column=3)

        department_label = tk.Label(self.item_master_frame, text='Department')
        department_label.grid(row=9, column=1)
        self.department_clicked = tk.StringVar()
        self.department_clicked_id = tk.IntVar()
        self.department_combobox = ttk.Combobox(self.item_master_frame, textvariable=self.department_clicked)
        self.department_combobox.grid(row=9,column=2)
        self.department_combobox.bind('<KeyRelease>', self.controller.departments_onchange)
        self.department_combobox.bind("<<ComboboxSelected>>", self.controller.departments_selected)

        add_dept_btn = tk.Button(self.item_master_frame,command=self.department_popup,borderwidth=0)
        add_dept_btn.grid(row=9, column=3)

        category_label = tk.Label(self.item_master_frame, text='Category')
        category_label.grid(row=10, column=1)
        self.category_clicked = tk.StringVar()
        self.category_clicked_id = tk.IntVar()
        self.category_list = ttk.Combobox(self.item_master_frame,textvariable=self.category_clicked)
        self.category_list.grid(row=10,column=2)
        self.category_list.bind('<KeyRelease>', self.controller.category_onchange)
        self.category_list.bind("<<ComboboxSelected>>", self.controller.category_selected)


        add_category_btn = tk.Button(self.item_master_frame,command=self.category_popup,borderwidth=0)
        add_category_btn.grid(row=10, column=3)

        sub_category_label = tk.Label(self.item_master_frame,text='Sub-Category')
        sub_category_label.grid(row=11,column=1)
        self.sub_category_clicked = tk.StringVar()
        self.sub_category_clicked_id = tk.IntVar()
        self.sub_category_list = ttk.Combobox(self.item_master_frame,textvariable=self.sub_category_clicked)
        self.sub_category_list.grid(row=11,column=2)
        self.sub_category_list.bind('<KeyRelease>', self.controller.sub_category_onchange)
        self.sub_category_list.bind("<<ComboboxSelected>>", self.controller.sub_category_selected)

        add_sub_category_btn = tk.Button(self.item_master_frame,command=self.sub_category_popup,borderwidth=0)
        add_sub_category_btn.grid(row=11, column=3)

        alternative_unit_label = tk.Label(self.item_master_frame, text='Alternative Unit')
        alternative_unit_label.grid(row=12,column=1)

        self.alternative_unit_clicked = tk.StringVar()
        self.alternative_unit_clicked_id = tk.IntVar()
        self.alternative_unit_input = ttk.Combobox(self.item_master_frame, textvariable=self.alternative_unit_clicked)
        self.alternative_unit_input.grid(row=12,column=2)
        # self.alternative_unit_clicked.trace("w", self.controller.onchange_alternative_units)

        self.alternative_label = tk.Label(self.item_master_frame)
        self.alternative_label.grid(row=13,column=1)
        self.alternative_unit_entry = tk.Entry(self.item_master_frame)
        self.alternative_unit_entry.grid(row=13,column=2)


        expiry_label = tk.Label(self.item_master_frame,text='Expiry')
        expiry_label.grid(row=2,column=4,padx=10,pady=10)
        self.expiry_input = DateEntry(self.item_master_frame,selectmode='day',date_pattern='dd/mm/y')
        self.expiry_input.grid(row=2,column=5)


        cost_price_label = tk.Label(self.item_master_frame,text='Cost-Price')
        cost_price_label.grid(row=3,column=4,padx=10,pady=10)
        self.cost_price = tk.Entry(self.item_master_frame)
        self.cost_price.bind('<KeyRelease>', self.controller.cost_price_onchange)
        self.cost_price.grid(row=3,column=5)


        gst_percentage_label = tk.Label(self.item_master_frame,text='GST %')
        gst_percentage_label.grid(row=4,column=4,padx=10,pady=10)
        self.gst_clicked = tk.StringVar()
        self.gst_clicked_id = tk.IntVar()
        self.gst_percentage_input = ttk.Combobox(self.item_master_frame, textvariable=self.gst_clicked)
        self.gst_percentage_input.grid(row=4,column=5,padx=10,pady=10)
        self.gst_percentage_input.bind('<KeyRelease>', self.controller.gst_onchange)
        self.gst_percentage_input.bind("<<ComboboxSelected>>", self.controller.gst_selected)

        gst_amount_label = tk.Label(self.item_master_frame,text='GST AMOUNT')
        gst_amount_label.grid(row=5,column=4,padx=10,pady=10)
        self.gst_amount_input = tk.Entry(self.item_master_frame,state='readonly')
        self.gst_amount_input.grid(row=5,column=5)

        cess_percentage_label = tk.Label(self.item_master_frame,text='Cess %')
        cess_percentage_label.grid(row=7,column=4,padx=10,pady=10)
        self.cess_clicked = tk.StringVar()
        self.cess_clicked_id = tk.IntVar()
        self.cess_percentage_input = ttk.Combobox(self.item_master_frame, textvariable=self.cess_clicked)
        self.cess_percentage_input.grid(row=7,column=5)

        add_cess_btn = tk.Button(self.item_master_frame,borderwidth=0,command=self.cess_popup)
        add_cess_btn.grid(row=7,column=8,padx=10,pady=10)
        
        self.cess_percentage_input.bind('<KeyRelease>', self.controller.cess_onchange)
        self.cess_percentage_input.bind("<<ComboboxSelected>>", self.controller.cess_selected)

        cess_amount_label = tk.Label(self.item_master_frame,text='Cess AMOUNT')
        cess_amount_label.grid(row=8,column=4,padx=10,pady=10)
        self.cess_amount_input = tk.Entry(self.item_master_frame,state='readonly')
        self.cess_amount_input.grid(row=8,column=5)


        margin_label = tk.Label(self.item_master_frame,text='Margin')
        margin_label.grid(row=9,column=4,padx=10,pady=10)
        self.margin_amount_input = tk.Entry(self.item_master_frame)
        self.margin_amount_input.grid(row=9,column=5)
        self.margin_amount_input.bind('<KeyRelease>', self.controller.margin_amount_onchange)

        margin_percentage_label = tk.Label(self.item_master_frame,text='Margin %')
        margin_percentage_label.grid(row=10,column=4,padx=10,pady=10)
        self.margin_percentage = tk.Entry(self.item_master_frame)
        self.margin_percentage.grid(row=10,column=5)
        self.margin_percentage.bind('<KeyRelease>', self.controller.margin_percentage_onchange)

        selling_price_label = tk.Label(self.item_master_frame,text='Selling Price')
        selling_price_label.grid(row=11,column=4,padx=10,pady=10)
        self.selling_price_input = tk.Entry(self.item_master_frame)
        self.selling_price_input.grid(row=11,column=5)
        self.selling_price_input.bind('<KeyRelease>', self.controller.selling_price_onchange)
        

        sale_output_tax_label = tk.Label(self.item_master_frame,text='Sale Output Tax')
        sale_output_tax_label.grid(row=12,column=4,padx=10,pady=10)
        self.sale_output_tax = tk.Entry(self.item_master_frame)
        self.sale_output_tax.grid(row=12,column=5)



        mrp_price_label = tk.Label(self.item_master_frame,text='MRP')
        mrp_price_label.grid(row=13,column=4,padx=10,pady=10)
        self.mrp_price_input = tk.Entry(self.item_master_frame)
        self.mrp_price_input.grid(row=13,column=5)

        rack_no_label = tk.Label(self.item_master_frame,text='Rack No')
        rack_no_label.grid(row=14,column=4,padx=10,pady=10)
        self.rack_clicked = tk.StringVar()
        self.rack_clicked_id = tk.IntVar()

        self.rack_no_input = ttk.Combobox(self.item_master_frame,textvariable=self.rack_clicked)
        self.rack_no_input.grid(row=14,column=5)
        self.rack_no_input.bind('<KeyRelease>', self.controller.racks_onchange)
        self.rack_no_input.bind("<<ComboboxSelected>>", self.controller.rack_selected)

        opening_qty_label = tk.Label(self.item_master_frame,text='Opening Qty')
        opening_qty_label.grid(row=15,column=4,padx=10,pady=10)
        self.opening_qty_input = tk.Entry(self.item_master_frame)
        self.opening_qty_input.grid(row=15,column=5)
        


        self.additem_btn = tk.Button(self.item_master_frame,text='Add Item',command=self.controller.add_item_btn)
        self.additem_btn.grid(row=16,column=3)
        

        List_btn = tk.Button(self.item_master_frame,text='List',command=self.items_list_popup)
        List_btn.grid(row=17,column=4)

        self.item_master_frame.pack(pady=10)



    def items_list_popup(self):
        self.items_list_window = tk.Toplevel(self.item_master_frame)
        self.items_list_window.geometry('500x600')
        self.items_list_window.title('Items List')

        # define Tree Widget
        style = ttk.Style()
        style.theme_use('default')
        style.configure('Treeview',
        background='#D3D3D3',
        foreground='black',
        rowheight=25,
        fieldbackground='#D3D3D3'
        )

        style.map('Treeview',
        background=[('selected','#347083')])

        tree_scroll = tk.Scrollbar(self.items_list_window)
        tree_scroll.pack(side='right',fill='y')

        self.item_master_list_view = ttk.Treeview(self.items_list_window,yscrollcommand=tree_scroll.set,selectmode='extended')
        self.item_master_list_view.pack()

        tree_scroll.config(command=self.item_master_list_view.yview)

        self.item_master_list_view['columns'] = ('id','name','barcode','hsn_code','batch_no','brand_name','department_name','category_name',
                                                'units','cost_price','gst','cess','margin','selling_price','mrp','rack_no','opening_qty','expiry')

        self.item_master_list_view.column('#0', width=0, stretch='no')
        self.item_master_list_view.column('id',anchor='w',width=100)
        self.item_master_list_view.column('name',anchor='w',width=100)
        self.item_master_list_view.column('barcode',anchor='center',width=100)
        self.item_master_list_view.column('hsn_code',anchor='center',width=100)
        self.item_master_list_view.column('batch_no',anchor='center',width=100)
        self.item_master_list_view.column('brand_name',anchor='center',width=100)
        self.item_master_list_view.column('department_name',anchor='center',width=100)
        self.item_master_list_view.column('category_name',anchor='center',width=100)
        self.item_master_list_view.column('units',anchor='center',width=100)
        self.item_master_list_view.column('cost_price',anchor='center',width=100)
        self.item_master_list_view.column('gst',anchor='center',width=100)
        self.item_master_list_view.column('cess',anchor='center',width=100)
        self.item_master_list_view.column('margin',anchor='center',width=100)
        self.item_master_list_view.column('selling_price',anchor='center',width=100)
        self.item_master_list_view.column('mrp',anchor='center',width=100)
        self.item_master_list_view.column('rack_no',anchor='center',width=100)
        self.item_master_list_view.column('opening_qty',anchor='center',width=100)
        self.item_master_list_view.column('expiry',anchor='center',width=100,stretch='yes')

        self.item_master_list_view.heading('#0', text='', anchor='w')
        self.item_master_list_view.heading('id',text="ID",anchor='w')
        self.item_master_list_view.heading('name',text="Item Name",anchor='w')
        self.item_master_list_view.heading('barcode',text="Barcode",anchor='center')
        self.item_master_list_view.heading('hsn_code',text="HSN Code",anchor='center')
        self.item_master_list_view.heading('batch_no',text="Batch No",anchor='center')
        self.item_master_list_view.heading('brand_name',text="Brand Name",anchor='center')
        self.item_master_list_view.heading('department_name',text="Department Name",anchor='center')
        self.item_master_list_view.heading('category_name',text="Category Name",anchor='center')
        self.item_master_list_view.heading('units',text="Units",anchor='center')
        self.item_master_list_view.heading('cost_price',text="Cost Price",anchor='center')
        self.item_master_list_view.heading('gst',text="GST",anchor='center')
        self.item_master_list_view.heading('cess',text="Cess",anchor='center')
        self.item_master_list_view.heading('margin',text='Margin',anchor='center')
        self.item_master_list_view.heading('selling_price',text="Selling Price",anchor='center')
        self.item_master_list_view.heading('mrp',text='MRP',anchor='center')
        self.item_master_list_view.heading('rack_no',text='Rack No',anchor='center')
        self.item_master_list_view.heading('opening_qty',text='Opening Qty',anchor='center')
        self.item_master_list_view.heading('expiry',text="Expiry",anchor='w')


        self.data = []


        self.item_master_list_view.tag_configure('oddrow', background='white')
        self.item_master_list_view.tag_configure('evenrow', background='lightblue')

        self.controller.update_item_master_treeview()


    def cess_popup(self):
        CessMasterController(CessMasterView,CessMasterModel)



    def unit_popup(self):
        self.unit_window = tk.Toplevel(self.item_master_frame)
        self.unit_window.geometry("250x110")
        self.unit_window.title("Create Brand")
        unit_label = tk.Label(self.unit_window,text="Create Unit")
        unit_label.grid(row=1,column=2)

        unit_label = tk.Label(self.unit_window,text='Unit Name')
        unit_label.grid(row=1,column=1)

        self.units_input = tk.Entry(self.unit_window)
        self.units_input.grid(row=1, column=2, padx=10, pady=10)
        
        decimal_label = tk.Label(self.unit_window,text='No:Of Decimal')
        decimal_label.grid(row=2,column=1)

        self.decimal_input = tk.Entry(self.unit_window)
        self.decimal_input.grid(row=2, column=2, padx=10, pady=10)

        self.units_add_btn = tk.Button(self.unit_window, text='Add',command=self.controller.add_unit)
        self.units_add_btn.grid(row=3, column=2)


    def brands_popup(self):
        self.brand_window = tk.Toplevel(self.item_master_frame)
        self.brand_window.geometry("250x110")
        self.brand_window.title("Create Brand")
        unit_label = tk.Label(self.brand_window,text="Create Brand")
        unit_label.grid(row=1,column=2)

        self.brand_types_option = self.controller.get_brand_types()

        self.brandtype_clicked = tk.StringVar()
        self.brandtype_clicked_id = tk.IntVar()

        self.brandtype_clicked_id.set(self.brand_types_option[0][0])
        self.brandtype_clicked.set(self.brand_types_option[0][1])

        self.brand_types_dropdown = ttk.Combobox(self.brand_window, width=20, textvariable=self.brandtype_clicked,values=self.brand_types_option)

        self.brandtype_clicked.trace("w", self.controller.brand_on_changes)

        self.brand_types_dropdown.grid(row=1, column=2)

        brand_name_label = tk.Label(self.brand_window,text='Brand Name')
        brand_name_label.grid(row=2,column=1)
        self.brands_input = tk.Entry(self.brand_window)
        self.brands_input.grid(row=2, column=2)
        self.brands_add_btn = tk.Button(self.brand_window,text='Add',command=self.controller.add_brand)
        self.brands_add_btn.grid(row=3, column=2)





    def department_popup(self):
        self.department_window = tk.Toplevel(self.item_master_frame)
        self.department_window.geometry("250x110")
        self.department_window.title("Create Department")

        department_label = tk.Label(self.department_window, text="Create Department")
        department_label.grid(row=1,column=2)

        self.department_input = tk.Entry(self.department_window)
        self.department_input.grid(row=3, column=2, padx=10, pady=10)
 
        self.department_add_btn = tk.Button(self.department_window, text='Add', command=self.controller.add_department)
        self.department_add_btn.grid(row=4, column=3, padx=10, pady=10)


    def category_popup(self):
        self.category_window = tk.Toplevel(self.item_master_frame)
        self.category_window.geometry("250x110")
        self.category_window.title("Create Category")
        category_label = tk.Label(self.category_window, text="Create Category")
        category_label.grid(row=1,column=2)

        self.department_options = self.controller.get_departments()
        self.department_clicked = tk.StringVar()
        self.department_clicked_id = tk.IntVar()
        self.department_clicked_id.set(self.department_options[0][0])
        self.department_clicked.set(self.department_options[0][1])

        self.departments_dropdown = ttk.Combobox(self.category_window, width=20, textvariable=self.department_clicked, values=self.department_options)
        self.departments_dropdown.grid(row=2,column=2)

        self.department_clicked.trace("w", self.controller.department_on_changes)

        self.category_input = tk.Entry(self.category_window)
        self.category_input.grid(row=3, column=2, padx=10, pady=10)
 
        self.category_add_btn = tk.Button(self.category_window, text='Add', command=self.controller.add_category)
        self.category_add_btn.grid(row=4, column=2)


    def sub_category_popup(self):
        self.sub_category_window = tk.Toplevel(self.item_master_frame)
        self.sub_category_window.geometry("250x110")
        self.sub_category_window.title("Create Category")

        sub_category_label = tk.Label(self.sub_category_window, text="Create Sub-Category")
        sub_category_label.grid(row=1,column=2)

        self.category_options = self.controller.get_category()

        self.category_clicked_popup = tk.StringVar()
        self.category_clicked_id_pop = tk.IntVar()

        self.category_clicked_id_pop.set(self.category_options[0][0])
        self.category_clicked_popup.set(self.category_options[0][1])

        self.category_dropdown = ttk.Combobox(self.sub_category_window, width=20, textvariable=self.category_clicked_popup, values=self.category_options)
        self.category_dropdown.grid(row=2,column=2)

        self.category_clicked_popup.trace("w", self.controller.category_on_change)

        self.sub_category_input = tk.Entry(self.sub_category_window)
        self.sub_category_input.grid(row=3, column=2)
 
        self.sub_category_add_btn = tk.Button(self.sub_category_window, text='Add', command=self.controller.add_sub_category)
        self.sub_category_add_btn.grid(row=4, column=2)

        
        

        









