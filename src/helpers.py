import re
def get_id_from_string(text):
    id = re.findall(r'\d+', text)
    return id[0]