from src.controllers.base_controller import BaseController

class ExpenseController(BaseController):
    def __init__(self,ExpenseView,ExpenseModel):
        super(ExpenseController, self).__init__()
        self.expense_model = ExpenseModel()
        self.expense_view = ExpenseView(self)
        self.update_treeview()


    def clear_input_boxes(self):
        self.expense_view.expense_group_input.delete(0, 'end')
        

    def select_item(self, e):
        self.clear_input_boxes()
        selected = self.expense_view.expense_group_view.focus()
        values = self.expense_view.expense_group_view.item(selected, 'values')
        self.expense_view.expense_group_input.insert(0, values[1])
        
    def enable_buttons(self):
        self.expense_view.expense_group_delete_btn.config(state='active')
        self.expense_view.expense_group_add.config(state='active')
        self.expense_view.expense_group_update_btn.grid_forget()
        self.expense_view.expense_group_edit_btn.grid(row=4, column=2)

    def disable_buttons(self):
        self.expense_view.expense_group_delete_btn.config(state='disabled')
        self.expense_view.expense_group_add.config(state='disabled')

    def update_treeview(self):
        self.expense_view.expense_group_view.delete(*self.expense_view.expense_group_view.get_children())
        records = self.expense_model.fetch_all()
        count = 0
        for record in records:
            expense_group_id = record.get('id')
            expense_group_name = record.get('name')
            self.expense_view.expense_group_view.insert(parent='', index='end', iid=count, text='', values=(expense_group_id, expense_group_name))
            count += 1
    
    def add_item(self):
        expense_input = self.expense_view.expense_group_input.get()
        self.expense_model.insert_data(expense_input)
        self.update_treeview()

    def edit_item(self):
        self.disable_buttons()
        self.expense_view.expense_group_edit_btn.grid_forget()
        self.expense_view.expense_group_update_btn.grid(row=4, column=2)

    def update_item(self):
        expense_group_name = self.expense_view.expense_group_input.get()
        selected = self.expense_view.expense_group_view.focus()
        expense_group_id = self.expense_view.expense_group_view.item(selected, 'values')[0]
        self.expense_model.update_data(expense_group_name,str(expense_group_id))
        self.expense_view.expense_group_view.item(selected, text="", values=(expense_group_id, expense_group_name))
        self.enable_buttons()

    def delete_item(self):
        try:
            selected = self.expense_view.expense_group_view.focus()
            expense_id = self.expense_view.expense_group_view.item(selected, 'values')[0]
            self.expense_model.delete_data(str(expense_id))
            self.expense_view.expense_group_view.delete(selected)
            self.clear_input_boxes()
        except IndexError:
            print('select please')
        finally:
            print('deleted')


    def add_to_ledger(self):
        ledger_name = self.expense_view.ledger_name_input.get()
        ledger_type = self.expense_view.type_input.get()
        gst_percentage = self.expense_view.gst_percentage_input.get()
        opening_balance = self.expense_view.opening_balance_input.get()
        selected = self.expense_view.expense_group_view.focus()
        account_group_id,account_group_name = self.expense_view.expense_group_view.item(selected, 'values')
        self.expense_model.insert_to_ledger(ledger_name,ledger_type,gst_percentage,opening_balance,account_group_id)
