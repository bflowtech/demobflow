from src.controllers.base_controller import BaseController
from tkinter import messagebox


class CompanyMasterController(BaseController):
    def __init__(self,CompanyMasterView,CompanyMasterModel):
        super(CompanyMasterController, self).__init__()
        self.model = CompanyMasterModel()
        self.view = CompanyMasterView(self)
        self._set_data()


    def clear_input_boxes(self):
        pass

    def select_item(self, e):
        pass

    def enable_buttons(self):
        pass

    def disable_buttons(self):
        pass
    
    def _set_data(self):
        company_data = self.model.fetch_all()
        self.view.company_name_entry.insert(0, company_data.get('name'))
        self.view.address_entry.insert(0, company_data.get('address'))
        self.view.city_entry.insert(0, company_data.get('city'))
        self.view.state_entry.insert(0, company_data.get('state'))
        self.view.country_entry.insert(0, company_data.get('country'))
        self.view.pincode_entry.insert(0, company_data.get('pincode'))
        self.view.gst_no_entry.insert(0, company_data.get('gst_no'))
        self.view.phone_no_entry.insert(0, company_data.get('phone_no'))
        self.view.mobile_no_entry.insert(0, company_data.get('mobile_no'))
        self.view.fax_no_entry.insert(0, company_data.get('fax_no'))
        self.view.email_entry.insert(0, company_data.get('email'))
        self.view.website_entry.insert(0, company_data.get('website'))

    def company_submit_btn(self):
        company_name = self.view.company_name_entry.get()
        address = self.view.address_entry.get()
        city = self.view.city_entry.get()
        state = self.view.state_entry.get()
        country = self.view.country_entry.get()
        pincode = self.view.pincode_entry.get()
        gst_no = self.view.gst_no_entry.get()
        phone_no = self.view.phone_no_entry.get()
        mobile_no = self.view.mobile_no_entry.get()
        fax_no = self.view.fax_no_entry.get()
        email = self.view.email_entry.get()
        website = self.view.website_entry.get()

        company_data = self.model.fetch_all()

        if company_data:
            company_id = company_data[0]
            self.model.update_data(company_name,address,city,state,country,pincode,gst_no,phone_no,mobile_no,fax_no,email,website,company_id)
            messagebox.showinfo("Success",'Company Updated')
        else:
            self.model.insert_data(company_name,address,city,state,country,pincode,
                                    gst_no,phone_no,mobile_no,fax_no,email,website)
            messagebox.showinfo("Success", 'Company Created')