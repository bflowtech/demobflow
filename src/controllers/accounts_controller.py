from src.controllers.assets_controller import AssetsController
from src.views.assets_view import AssetsView
from src.models.assets_model import AssetsModel

from src.controllers.liablity_controller import LiablityController
from src.views.liablity_view import LiablityView
from src.models.liablity_model import LiablityModel


from src.controllers.income_controller import IncomeController
from src.views.income_view import IncomeView
from src.models.income_model import IncomeModel

from src.controllers.expense_controller import ExpenseController
from src.views.expense_view import ExpenseView
from src.models.expense_model import ExpenseModel

from src.controllers.ledger_controller import LedgerController
from src.views.ledger_view import LedgerView
from src.models.ledger_model import LedgerModel

class AccountsController:
    def __init__(self,AccountsView,AccountsModel):
        super(AccountsController, self).__init__()
        self.accounts_model = AccountsModel()
        self.accounts_view = AccountsView(self)

    def switch_to_assets_view(self):
        self.accounts_view.destroy()
        AssetsController(AssetsView,AssetsModel)

    def switch_to_liablity_view(self):
        self.accounts_view.destroy()
        LiablityController(LiablityView,LiablityModel)

    def switch_to_income_view(self):
        self.accounts_view.destroy()
        IncomeController(IncomeView,IncomeModel)

    def switch_to_expense_view(self):
        self.accounts_view.destroy()
        ExpenseController(ExpenseView,ExpenseModel)

    def switch_to_ledger_view(self):
        self.accounts_view.destroy()
        LedgerController(LedgerView,LedgerModel)