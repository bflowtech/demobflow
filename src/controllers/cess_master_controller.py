from src.controllers.base_controller import BaseController

class CessMasterController(BaseController):
    def __init__(self,CessMasterView,CessMasterModel):
        super(CessMasterController, self).__init__()
        self.model = CessMasterModel()
        self.view = CessMasterView(self)
        self.update_cess_master_treeview()


    def clear_input_boxes(self):
        self.view.cess_name_input.delete(0, 'end')
        self.view.cess_percentage_input.delete(0, 'end')

    def select_item(self, event):
        self.clear_input_boxes()
        selected = self.view.cess_list_view.focus()
        values = self.view.cess_list_view.item(selected, 'values')
        self.view.cess_name_input.insert(0, values[1])
        self.view.cess_percentage_input.insert(0, values[2])

    def enable_buttons(self):
        self.view.cess_delete_btn.config(state='active')
        self.view.cess_add_btn.config(state='active')
        

    def disable_buttons(self):
        self.view.cess_delete_btn.config(state='disabled')
        self.view.cess_add_btn.config(state='disabled')


    def add_item(self):
        cess_name = self.view.cess_name_input.get()
        cess_percentage = self.view.cess_percentage_input.get()
        self.model.insert_data(cess_name,cess_percentage)
        self.clear_input_boxes()
        self.update_cess_master_treeview()


    def edit_item(self):
        self.disable_buttons()
        self.view.cess_edit_btn.grid_forget()
        self.view.cess_update_btn.grid(row=2, column=3)

    def update_item(self):
        cess_name = self.view.cess_name_input.get()
        cess_percentage = self.view.cess_percentage_input.get()
        selected = self.view.cess_list_view.focus()
        cess_id = self.view.cess_list_view.item(selected,'values')[0]
        self.model.update_data(cess_name,str(cess_percentage),str(cess_id))
        self.update_cess_master_treeview()
        self.enable_buttons()
        self.view.cess_edit_btn.grid(row=2, column=3)
        self.view.cess_update_btn.grid_forget()
        self.clear_input_boxes()
        

    def delete_item(self):
        try:
            selected = self.view.cess_list_view.focus()
            cess_id = self.view.cess_list_view.item(selected, 'values')[0]
            self.model.delete_data(str(cess_id))
            self.view.cess_list_view.delete(selected)
            self.clear_input_boxes()
            self.update_cess_master_treeview()
        except:
            print('select a value')
        finally:
            print('Congrats')
    
    def update_cess_master_treeview(self):
        self.view.cess_list_view.delete(*self.view.cess_list_view.get_children())
        records = self.model.fetch_all()
        count = 0
        for record in records:
            cess_id = record.get('cess_id')
            cess_name = record.get('cess_name')
            cess_percentage = record.get('percentage')

            self.view.cess_list_view.insert(parent='', index='end', iid=count, text='', values=(cess_id, cess_name,cess_percentage))
            count += 1


        