from src.controllers.base_controller import BaseController

class GstMasterController(BaseController):
    def __init__(self,GstMasterView,GstMasterModel):
        super(GstMasterController, self).__init__()
        self.gst_master_model = GstMasterModel()
        self.gst_master_view = GstMasterView(self)

        self.update_gst_treeview()

    def clear_input_boxes(self):
        self.gst_master_view.gst_name_input.delete(0,'end')
        self.gst_master_view.gst_percentage_input.delete(0,'end')

    def select_item(self,e):
        self.clear_input_boxes()
        selected = self.gst_master_view.gst_list_view.focus()
        values = self.gst_master_view.gst_list_view.item(selected, 'values')
        self.gst_master_view.gst_name_input.insert(0, values[1])
        self.gst_master_view.gst_percentage_input.insert(0, values[2])

    def enable_buttons(self):
        self.gst_master_view.gst_delete_btn.config(state='active')
        self.gst_master_view.gst_add_btn.config(state='active')

    def disable_buttons(self):
        self.gst_master_view.gst_delete_btn.config(state='disabled')
        self.gst_master_view.gst_add_btn.config(state='disabled')

    def add_item(self):
        gst_name = self.gst_master_view.gst_name_input.get()
        gst_percentage = self.gst_master_view.gst_percentage_input.get()
        self.gst_master_model.insert_data(gst_name,str(gst_percentage))
        self.update_gst_treeview()
        self.clear_input_boxes()

    def edit_item(self):
        self.disable_buttons()
        self.gst_master_view.gst_edit_btn.grid_forget()
        self.gst_master_view.gst_update_btn.grid(row=2, column=3)

    def update_item(self):
        gst_name = self.gst_master_view.gst_name_input.get()
        gst_percentage = self.gst_master_view.gst_percentage_input.get()
        selected = self.gst_master_view.gst_list_view.focus()
        gst_id = self.gst_master_view.gst_list_view.item(selected,'values')[0]
        self.gst_master_model.update_data(gst_name,str(gst_percentage),str(gst_id))
        self.update_gst_treeview()
        self.enable_buttons()
        self.gst_master_view.gst_edit_btn.grid(row=2, column=3)
        self.gst_master_view.gst_update_btn.grid_forget()
        self.clear_input_boxes()


    def delete_item(self):
        try:
            selected = self.gst_master_view.gst_list_view.focus()
            gst_id = self.gst_master_view.gst_list_view.item(selected, 'values')[0]
            self.gst_master_model.delete_data(str(gst_id))
            self.gst_master_view.gst_list_view.delete(selected)
            self.clear_input_boxes()
            self.update_gst_treeview()
        except:
            print('select a value')
        finally:
            print('Congrats')

    def update_gst_treeview(self):
        self.gst_master_view.gst_list_view.delete(*self.gst_master_view.gst_list_view.get_children())
        records = self.gst_master_model.fetch_all()
        count = 0
        for record in records:
            gst_id = record.get('gst_id')
            gst_name = record.get('gst_name')
            gst_percentage = record.get('percentage')
            self.gst_master_view.gst_list_view.insert(parent='', index='end', iid=count, text='',
                                                    values=(gst_id, gst_name,gst_percentage)
                                                    )
            count += 1
