from src.controllers.base_controller import BaseController

class SalesOrderController(BaseController):
    def __init__(self, SalesOrderView, SalesOrderModel):
        super(SalesOrderController, self).__init__()
        self.model = SalesOrderModel()
        self.view = SalesOrderView(self)
        self.set_items()


    def clear_input_boxes(self):
        self.view.barcode_entry.delete(0, 'end')
        self.view.batch_no_entry.delete(0, 'end')
        self.view.hsn_entry.delete(0, 'end')
        self.view.qty_entry.delete(0, 'end')
        self.view.disc_entry.delete(0, 'end')
        self.view.taxable_entry.delete(0, 'end')
        self.view.cgst_entry.delete(0, 'end')
        self.view.sgst_entry.delete(0, 'end')
        self.view.igst_entry.delete(0, 'end')
        self.view.cess_entry.delete(0, 'end')
        self.view.total_amount_entry.delete(0, 'end')
        self.view.rate_entry.delete(0, 'end')

    def select_item(self, e):
        pass


    def enable_buttons(self):
        pass

    def disable_buttons(self):
        pass
    
    def set_items(self):
        self.items_options = self.model.get_items()
        self.party_accounts = self.model.get_party_accounts()
        self.view.items_combobox['values'] = [item.get('item_name') for item in self.items_options]    
        self.view.party_account_type['values'] = [supplier.get('ledger_name') for supplier in self.party_accounts]
        self.view.bill_type['values'] = ('IntraState','InterState')

    def item_onchage(self,*args):
        item_name = self.view.items_combobox.get()
        print(item_name)


    def update_sale_order_list(self,records):
        global count
        count = 0
        self.view.sales_order_view.delete(*self.view.sales_order_view.get_children())
        for record in records:
            if count % 2 == 0:
                self.view.sales_order_view.insert(parent='',index='end',iid=count,values=(
                    count,record[0],record[1],record[2],record[3],record[4],record[5],record[6],record[7],record[8],record[9],record[10],record[11],record[12]),
                tags=('evenrow',)
                )
            else:
                self.view.sales_order_view.insert(parent='',index='end',iid=count,values=(
                    count,record[0],record[1],record[2],record[3],record[4],record[5],record[6],record[7],record[8],record[9],record[10],record[11],record[12]),
                tags=('evenrow',)
                )
            count += 1


    def add_to_sale_order_list(self):
        barcode = self.view.barcode_entry.get()
        batch_no = self.view.batch_no_entry.get()
        hsn_no = self.view.hsn_entry.get()
        quantity = self.view.qty_entry.get()
        rate = self.view.rate_entry.get()
        discount = self.view.disc_entry.get()
        taxable_amount = self.view.taxable_entry.get()
        cgst = self.view.cgst_entry.get()
        sgst = self.view.sgst_entry.get()
        igst = self.view.igst_entry.get()
        cess = self.view.cess_entry.get()
        total_amount = self.view.total_amount_entry.get()
        item_name = self.view.item_clicked.get()
        self.view.data.append([barcode,batch_no,item_name,hsn_no,quantity,rate,discount,taxable_amount,cgst,sgst,igst,cess,total_amount],)
        self.update_sale_order_list(self.view.data)

    def remove_from_sales_order_list(self):
        selected_item = self.view.sales_order_view.focus()
        self.view.data.pop(int(selected_item))
        self.update_sale_order_list(self.view.data)

    def on_qty_changed(self,event):
        quantity = int(self.view.qty_entry.get())
        rate = float(self.view.rate_entry.get())
        total_amount = quantity * rate
        self.view.total_amount_entry.delete(0,'end')
        self.view.total_amount_entry.insert(0,total_amount)


    def create_sale_order(self):
        voucher_no = self.view.sales_voucher_entry.get()
        party_account_type = self.view.party_account_type.get()
        reference_no = self.view.reference_no_entry.get()
        order_no = self.view.order_no_entry.get()
        bill_type = self.view.bill_type.get()
        sales_order_id = self.model.insert_data(voucher_no,party_account_type,reference_no,order_no,bill_type)
        items_list = self.view.data
        self.model.insert_items_data(items_list,sales_order_id)

    def item_selected(self,event):
        index = self.view.items_combobox.current()
        item_id = self.items_options[index].get('item_id')
        item_name = self.items_options[index].get('item_name')

        item_details = self.model.get_item_details(str(item_id))
    
        self.view.barcode_entry.insert(0,item_details.get('barcode'))
        self.view.hsn_entry.insert(0,item_details.get('hsn_code'))
        self.view.rate_entry.insert(0,item_details.get('selling_price'))

        self.view.item_clicked.set(item_name)
        self.view.item_clicked_id.set(item_id)

        