from src.controllers.base_controller import BaseController

class RacksController(BaseController):
    def __init__(self, RacksView, RacksModel):
        super(RacksController, self).__init__()
        self.racks_model = RacksModel()
        self.racks_view = RacksView(self)

        self.update_racks_treeview()

    def clear_input_boxes(self):
        self.racks_view.racks_name_input.delete(0, 'end')
        self.racks_view.rack_starts_input.delete(0, 'end')
        self.racks_view.rack_ends_input.delete(0, 'end')

    def select_item(self, e):
        self.clear_input_boxes()
        selected = self.racks_view.racks_list_view.focus()
        values = self.racks_view.racks_list_view.item(selected, 'values')
        self.racks_view.racks_name_input.insert(0, values[1])
        self.racks_view.rack_starts_input.insert(0, values[2])
        self.racks_view.rack_ends_input.insert(0, values[3])


    def enable_buttons(self):
        self.racks_view.racks_delete_btn.config(state='active')
        self.racks_view.racks_add_btn.config(state='active')

    def disable_buttons(self):
        self.racks_view.racks_delete_btn.config(state='disabled')
        self.racks_view.racks_add_btn.config(state='disabled')

    def update_racks_treeview(self):
        self.racks_view.racks_list_view.delete(*self.racks_view.racks_list_view.get_children())
        records = self.racks_model.fetch_all()
        count = 0
        for record in records:
            rack_id = record.get('rack_id')
            rack_name = record.get('rack_name')
            starts = record.get('starts')
            ends = record.get('ends')
            self.racks_view.racks_list_view.insert(parent='', index='end', iid=count, text='',
                                                   values=(rack_id, rack_name, starts, ends))
            count += 1



    def add_item(self):
        rack_name = self.racks_view.racks_name_input.get()
        rack_starts = self.racks_view.rack_starts_input.get()
        rack_ends = self.racks_view.rack_ends_input.get()
        self.racks_model.insert_data(rack_name, str(rack_starts), str(rack_ends))
        self.update_racks_treeview()
        self.clear_input_boxes()

    def delete_item(self):
        try:
            selected = self.racks_view.racks_list_view.focus()
            rack_id = self.racks_view.racks_list_view.item(selected, 'values')[0]
            self.racks_model.delete_data(str(rack_id))
            self.racks_view.racks_list_view.delete(selected)
            self.clear_input_boxes()
            self.update_racks_treeview()
        except IndexError:
            print('Nothing Selected')
        finally:
            print('congrats')

    def edit_item(self):
        self.disable_buttons()
        self.racks_view.racks_edit_btn.grid_forget()
        self.racks_view.racks_update_btn.grid(row=3, column=3)

    def update_item(self):
        rack_name = self.racks_view.racks_name_input.get()
        selected = self.racks_view.racks_list_view.focus()
        rack_id = self.racks_view.racks_list_view.item(selected, 'values')[0]
        starts = self.racks_view.rack_starts_input.get()
        ends = self.racks_view.rack_ends_input.get()
        self.racks_model.update_data(rack_name, str(starts), str(ends), str(rack_id))
        self.update_racks_treeview()
        self.enable_buttons()
        self.racks_view.racks_edit_btn.grid(row=3, column=3)
        self.racks_view.racks_update_btn.grid_forget()
        self.clear_input_boxes()
