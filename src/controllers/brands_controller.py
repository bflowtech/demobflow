from src.controllers.base_controller import BaseController


class BrandsController(BaseController):
    def __init__(self,BrandsView,BrandsModel):
        super(BrandsController, self).__init__()
        self.model = BrandsModel()
        self.view = BrandsView(self)
        self.update_brands_treeview()
        self.set_items()

    def set_items(self):
        self.brand_type_options = self.model.fetch_brand_types()
        self.view.brand_types_dropdown['values'] = [x['name'] for x in self.brand_type_options]
        
    def clear_input_boxes(self):
        self.view.brands_input.delete(0, 'end')

    def select_item(self,e):
        self.clear_input_boxes()
        selected = self.view.brands_list_view.focus()
        values = self.view.brands_list_view.item(selected, 'values')
        self.view.brands_input.insert(0, values[1])

    def enable_buttons(self):
        self.view.brands_delete_btn.config(state='active')
        self.view.brands_add_btn.config(state='active')
        self.view.brands_edit_btn.grid(row=2, column=3)
        self.view.brands_update_btn.grid_forget()

    def disable_buttons(self):
        self.view.brands_delete_btn.config(state='disabled')
        self.view.brands_add_btn.config(state='disabled')


    def update_brands_treeview(self):
        self.view.brands_list_view.delete(*self.view.brands_list_view.get_children())
        records = self.model.fetch_all()
        count = 0
        for record in records:
            brand_id = record.get('brand_id')
            brand_name = record.get('brand_name')
            brand_type = record.get('brand_type')
            self.view.brands_list_view.insert(parent='', index='end', iid=count, text='', values=(brand_id,brand_name,brand_type))
            count += 1

    def add_item(self,brand_name):
        brand_type_id = self.view.brandtype_clicked_id.get()
        self.model.insert_data(brand_name,str(brand_type_id))
        self.update_brands_treeview()


    def edit_item(self):
        self.disable_buttons()
        self.view.brands_edit_btn.grid_forget()
        self.view.brands_update_btn.grid(row=2, column=3)

    def update_item(self):
        brand_name = self.view.brands_input.get()
        selected = self.view.brands_list_view.focus()
        brand_id = self.view.brands_list_view.item(selected, 'values')[0]
        brand_type_id = self.view.brandtype_clicked_id.get()
        self.model.update_data(brand_name,str(brand_type_id),brand_id)
        brand_type = self.view.brandtype_clicked.get()
        self.view.brands_list_view.item(selected, text="", values=(brand_id, brand_name,brand_type))
        self.enable_buttons()
        self.clear_input_boxes()


    def delete_item(self):
        try:
            selected = self.view.brands_list_view.focus()
            brand_id = self.view.brands_list_view.item(selected, 'values')[0]
            self.model.delete_data(str(brand_id))
            self.view.brands_list_view.delete(selected)
            self.clear_input_boxes()
        except IndexError:
            print('nothing selected')
        finally:
            print('Thankyou')

    def serach_brands(self,e):
        brand_name = self.view.search_box.get()
        records = self.model.serach_brand(brand_name)
        self.view.brands_list_view.delete(*self.view.brands_list_view.get_children())
        count = 0
        for record in records:
            self.view.brands_list_view.insert(parent='', index='end', iid=count, text='', values=(record[0],record[1],record[2]))
            count += 1

    def brand_type_selected(self,event):
        index = self.view.brand_types_dropdown.current()
        brand_type_id = self.brand_type_options[index].get('id')
        brand_type_name = self.brand_type_options[index].get('name')
        self.view.brandtype_clicked_id.set(brand_type_id)
        self.view.brandtype_clicked.set(brand_type_name)

        
        