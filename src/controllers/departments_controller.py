import re
from src.controllers.base_controller import BaseController

class DepartmentsController(BaseController):
    def __init__(self,DepartmentView,DepartmentsModel):
        super(DepartmentsController, self).__init__()
        self.department_model = DepartmentsModel()
        self.department_view = DepartmentView(self)
        self.update_departments_treeview()

    def clear_input_boxes(self):
        self.department_view.department_input.delete(0, 'end')

    def select_item(self, e):
        self.clear_input_boxes()
        selected = self.department_view.department_list_view.focus()
        values = self.department_view.department_list_view.item(selected, 'values')
        self.department_view.department_input.insert(0, values[1])

    def enable_buttons(self):
        self.department_view.department_delete_btn.config(state='active')
        self.department_view.department_add_btn.config(state='active')

    def disable_buttons(self):
        self.department_view.department_delete_btn.config(state='disabled')
        self.department_view.department_add_btn.config(state='disabled')

    def update_departments_treeview(self):
        self.department_view.department_list_view.delete(*self.department_view.department_list_view.get_children())
        records = self.department_model.fetch_all()
        count = 0
        for record in records:
            department_id = record.get('department_id')
            department_name = record.get('department_name')
            self.department_view.department_list_view.insert(parent='', index='end', iid=count, text='', values=(department_id, department_name))
            count += 1

    def add_item(self):
        department_name = self.department_view.department_input.get()
        self.department_model.insert_data(department_name)
        self.clear_input_boxes()
        self.update_departments_treeview()

    def delete_item(self):
        selected = self.department_view.department_list_view.focus()
        department_id = self.department_view.department_list_view.item(selected, 'values')[0]
        self.department_model.delete_data(department_id)
        self.department_view.department_list_view.delete(selected)
        self.clear_input_boxes()

    def edit_item(self):
        self.disable_buttons()
        self.department_view.department_edit_btn.grid_forget()
        self.department_view.department_update_btn.grid(row=2, column=3)

    def update_item(self):
        department_name = self.department_view.department_input.get()
        selected = self.department_view.department_list_view.focus()
        department_id = self.department_view.department_list_view.item(selected, 'values')[0]
        self.department_model.update_data(department_name,department_id)
        self.department_view.department_list_view.item(selected, text="", values=(department_id, department_name))
        self.enable_buttons()
        self.department_view.department_edit_btn.grid(row=2, column=3)
        self.department_view.department_update_btn.grid_forget()
        self.clear_input_boxes()

    def serach_departments(self,e):
        department_name = self.department_view.search_box.get()
        records = self.department_model.serach_department(department_name)
        self.department_view.department_list_view.delete(*self.department_view.department_list_view.get_children())
        count = 0
        for record in records:
            self.department_view.department_list_view.insert(parent='', index='end', iid=count, text='', values=(record[0],record[1]) )
            count += 1
