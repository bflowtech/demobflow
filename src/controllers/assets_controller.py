from sys import path
from src.controllers.base_controller import BaseController


class AssetsController(BaseController):
    def __init__(self,AssetsView,AssetsModel):
        super(AssetsController, self).__init__()
        self.assets_model = AssetsModel()
        self.assets_view = AssetsView(self)
        self.update_treeview()


    def clear_input_boxes(self):
        self.assets_view.asset_group_input.delete(0, 'end')
        

    def select_item(self, e):
        self.clear_input_boxes()
        selected = self.assets_view.asset_group_view.focus()
        self.group_id,self.group_name = self.assets_view.asset_group_view.item(selected, 'values')
        asset_id = self.group_id
        self.assets_view.asset_group_input.insert(0, self.group_name)
        


    def enable_buttons(self):
        self.assets_view.asset_group_add.config(state='active')
        self.assets_view.asset_group_delete_btn.config(state='active')
        self.assets_view.asset_group_edit_btn.grid(row=4, column=2)
        self.assets_view.asset_group_update_btn.grid_forget()


    def disable_buttons(self):
        self.assets_view.asset_group_add.config(state='disabled')
        self.assets_view.asset_group_delete_btn.config(state='disabled')
    

    def update_treeview(self):
        records = self.assets_model.fetch_all()
        self.assets_view.asset_group_view.delete(*self.assets_view.asset_group_view.get_children())
        count = 0
        for record in records:
            asset_group_id = record.get('id')
            asset_group_name = record.get('name')
            self.assets_view.asset_group_view.insert(parent='', index='end', iid=count, text='', values=(asset_group_id,asset_group_name))
            count += 1


    def add_item(self):
        asset_group_name = self.assets_view.asset_group_input.get()
        self.assets_model.insert_data(asset_group_name)
        self.update_treeview()


    def edit_item(self):
        self.disable_buttons()
        self.assets_view.asset_group_edit_btn.grid_forget()
        self.assets_view.asset_group_update_btn.grid(row=4, column=2)
    
    def update_item(self):
        asset_group_name = self.assets_view.asset_group_input.get()
        selected = self.assets_view.asset_group_view.focus()
        asset_group_id = self.assets_view.asset_group_view.item(selected, 'values')[0]
        self.assets_model.update_data(asset_group_name,asset_group_id)
        self.assets_view.asset_group_view.item(selected, text="", values=(asset_group_id, asset_group_name))
        self.enable_buttons()


    def delete_item(self):
        try:
            selected = self.assets_view.asset_group_view.focus()
            gst_id = self.assets_view.asset_group_view.item(selected, 'values')[0]
            self.assets_model.delete_data(str(gst_id))
            self.assets_view.asset_group_view.delete(selected)
            self.clear_input_boxes()
            self.update_treeview()
        except:
            print('select a value')
        finally:
            print('Congrats')


    #ledger

    def gst_onchange(self,*args):
        gst_type = self.assets_view.gst_clicked.get()
        if 'Unregistered' == gst_type:
            self.assets_view.gst_no_input.configure(state='disabled')
        elif 'Customer' == gst_type:
            self.assets_view.gst_no_input.configure(state='disabled')
        else:
            self.assets_view.gst_no_input.configure(state='normal')

    def add_to_ledger(self):
            ledger_name = self.assets_view.ledger_name_input.get()
            ledger_type = self.assets_view.type_input.get()
            contact_details = self.assets_view.contact_details_input.get()
            address = self.assets_view.address_details_input.get()
            phone_number = self.assets_view.phone_number_input.get()
            opening_balance = self.assets_view.opening_balance_input.get()

            try:
                pan_no = self.assets_view.pan_no_input.get()
            except AttributeError:
                pan_no = 'NULL'
            try:
                gst_type = self.assets_view.gst_clicked.get()
            except AttributeError:
                gst_type = 'NULL'
            try:
                gst_no = self.assets_view.gst_no_input.get()
            except AttributeError:
                gst_no = 'NULL'

            try:
                credit_period = self.assets_view.credit_period_input.get()
            except AttributeError:
                credit_period = 'NULL'

            try:
                gst_percentage = self.assets_view.gst_percentage_input.get()
            except AttributeError:
                gst_percentage = 'NULL'
                
            # bank
            try:
                bank_name = self.assets_view.bank_input.get()
                bank_address = self.assets_view.bank_address_input.get()
                account_no = self.assets_view.account_no_input.get()
                ifsc_code = self.assets_view.ifsc_code_input.get()
                branch_no = self.assets_view.branch_no_input.get()
                branch_code = self.assets_view.branch_code_input.get()
                bank_contact = self.assets_view.bank_contact_input.get()
                cheque_no = self.assets_view.cheque_no_input.get()
            except AttributeError:
                bank_name = 'NULL'
                bank_address = 'NULL'
                account_no = 'NULL'
                ifsc_code = 'NULL'
                branch_no = 'NULL'
                branch_code = 'NULL'
                bank_contact = 'NULL'
                cheque_no = 'NULL'
            finally:
                account_group_id = self.group_id

            self.assets_model.insert_to_ledger(ledger_name,ledger_type,credit_period,contact_details,address,
                                                phone_number,pan_no,gst_type,gst_no,gst_percentage,
                                                opening_balance,bank_name,bank_address,account_no,ifsc_code,
                                                branch_no,branch_code,bank_contact,cheque_no,account_group_id)