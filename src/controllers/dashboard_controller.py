from src.controllers.brands_controller import BrandsController
from src.views.brands_view import BrandsView
from src.models.brands_model import BrandsModel

from src.controllers.departments_controller import DepartmentsController
from src.views.departments_view import DepartmentsView
from src.models.departments_model import DepartmentsModel

from src.controllers.category_controller import CategoryController
from src.views.category_view import CategoryView
from src.models.category_model import CategoryModel

from src.controllers.sub_category_controller import SubCategoryController
from src.views.sub_category import SubcategoryView
from src.models.sub_category_model import SubCategoryModel

from src.controllers.units_controller import UnitsController
from src.views.units_view import UnitsView
from src.models.units_model import UnitsModel

from src.controllers.racks_controller import RacksController
from src.views.racks_view import RacksView
from src.models.racks_model import RacksModel

from src.controllers.gst_master_controller import GstMasterController
from src.views.gst_master_view import GstMasterView
from src.models.gst_master_model import GstMasterModel

from src.controllers.Item_master_controller import ItemMasterController
from src.views.item_master_view import ItemMasterView
from src.models.Item_master_model import ItemMasterModel

from src.controllers.cess_master_controller import CessMasterController
from src.views.cess_master_view import CessMasterView
from src.models.cess_master_model import CessMasterModel

from src.controllers.brand_types_controller import BrandsTypesController
from src.views.brand_types_view import BrandTypesView
from src.models.brand_types_model import BrandTypesModel

from src.views.purchase_voucher_view import PurchaseVoucherView
from src.controllers.purchase_voucher_controller import PurchaseController
from src.models.purchase_voucher_model import PurchaseModel

from src.views.purchase_order_view import PurchaseOrderView
from src.controllers.purchase_order_controller import PurchaseOrderController
from src.models.purchase_order_model import PurchaseOrderModel

from src.views.accounts_view import AccountsView
from src.controllers.accounts_controller import AccountsController
from src.models.accounts_model import AccountsModel

from src.views.stocks_report_view import StocksReportView
from src.controllers.stocks_report_controller import StocksReportController
from src.models.stocks_report_model import StocksReportModel

from src.views.sales_order_view import SalesOrderView
from src.controllers.sales_order_controller import SalesOrderController
from src.models.sales_order_model import SalesOrderModel


from src.views.sales_voucher_view import SalesVoucherView
from src.controllers.sales_voucher_controller import SalesVoucherController
from src.models.sales_voucher_model import SalesVoucherModel


from src.views.company_master_view import CompanyView
from src.controllers.company_master_controller import CompanyMasterController
from src.models.company_master_model import CompanyModel




class DashboardController:
    def __init__(self, DashboardView):
        super(DashboardController, self).__init__()
        self.dashboard_view = DashboardView(self)

    def switch_to_brand_types(self):
        self.dashboard_view.destroy()
        BrandsTypesController(BrandTypesView,BrandTypesModel)

    def swith_to_brands(self):
        self.dashboard_view.destroy()
        BrandsController(BrandsView, BrandsModel)

    def switch_to_departments(self):
        self.dashboard_view.destroy()
        DepartmentsController(DepartmentsView, DepartmentsModel)

    def switch_to_category(self):
        self.dashboard_view.destroy()
        CategoryController(CategoryView, CategoryModel)

    def switch_to_sub_category(self):
        self.dashboard_view.destroy()
        SubCategoryController(SubcategoryView, SubCategoryModel)

    def switch_to_units(self):
        self.dashboard_view.destroy()
        UnitsController(UnitsView, UnitsModel)

    def switch_to_racks(self):
        self.dashboard_view.destroy()
        RacksController(RacksView, RacksModel)

    def switch_to_gst_master(self):
        self.dashboard_view.destroy()
        GstMasterController(GstMasterView,GstMasterModel)

    def switch_to_item_master(self):
        self.dashboard_view.destroy()
        ItemMasterController(ItemMasterView,ItemMasterModel)

    def switch_to_cess_master(self):
        self.dashboard_view.destroy()
        CessMasterController(CessMasterView,CessMasterModel)


    def switch_to_purchase_btn(self):
        self.dashboard_view.destroy()
        PurchaseOrderController(PurchaseOrderView,PurchaseOrderModel)

    def switch_to_purchase_voucher(self):
        self.dashboard_view.destroy()
        PurchaseController(PurchaseVoucherView,PurchaseModel)

    def switch_to_accounts(self):
        self.dashboard_view.destroy()
        AccountsController(AccountsView,AccountsModel)

    def switch_to_reports(self):
        self.dashboard_view.destroy()
        StocksReportController(StocksReportView,StocksReportModel)
        
    def switch_to_sales_order(self):
        self.dashboard_view.destroy()
        SalesOrderController(SalesOrderView,SalesOrderModel)

    
    def switch_to_sales_voucher(self):
        self.dashboard_view.destroy()
        SalesVoucherController(SalesVoucherView,SalesVoucherModel)


    def switch_to_company_details(self):
        CompanyMasterController(CompanyView,CompanyModel)


    def run(self):
        self.dashboard_view.mainloop()
        
