
from src.controllers.base_controller import BaseController

class LedgerController(BaseController):
    def __init__(self,LedgerView,LedgerModel):
        super(LedgerController, self).__init__()
        self.model = LedgerModel()
        self.view = LedgerView(self)
        self.load_accounts()

    def clear_input_boxes(self):
        pass

    def select_item(self, e):
        pass

    def enable_buttons(self):
        pass

    def disable_buttons(self):
        pass
    
    def load_accounts(self,*args):
        group_name = self.view.account_option.get()

        self.group_options = self.model.get_accounts_groups(group_name)
        
        self.view.group_option['values'] = [group.get('name') for group in self.group_options]
        

    def selected_account(self,event):
        self.view.group_option.delete(0, 'end')


    def on_change_groups(self,*args):
        group_name = self.view.account_option.get()
        self.group_options = self.model.get_accounts_groups(group_name)
        

    def selected_group(self,event):        
        index = self.view.group_option.current()
        group_id = self.group_options[index].get('id')
        group_name = self.group_options[index].get('name')
        self.view.group_clicked_id.set(group_id)
        self.view.group_clicked.set(group_name)


    def gst_onchange(self,*args):
        if 'Unregistered' == self.view.gst_clicked.get() :
            self.view.gst_no_input.grid_forget()
        elif 'Customer' == self.view.gst_clicked.get() :
            self.view.gst_no_input.grid_remove()
        else:
            self.view.gst_no_input.grid(row=10,column=2)

    def add_ledger(self):
        print(self.view.gst_clicked.get())

        
    