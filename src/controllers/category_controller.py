from src.helpers import get_id_from_string
from src.controllers.base_controller import BaseController


class CategoryController(BaseController):
    def __init__(self,CategoryView,CategoryModel):
        super(CategoryController, self).__init__()
        self.model = CategoryModel()
        self.view = CategoryView(self)
        self.set_items()
        

    def set_items(self):
        self.department_options = self.model.get_departments()
        self.view.departments_list['values'] = [department.get('department_name') for department in self.department_options]
        self.update_category_treeview()

    def clear_input_boxes(self):
        self.view.category_input.delete(0, 'end')

    def select_item(self, e):
        self.clear_input_boxes()
        selected = self.view.category_list_view.focus()
        values = self.view.category_list_view.item(selected, 'values')
        self.view.category_input.insert(0, values[1])

    def enable_buttons(self):
        self.view.category_delete_btn.config(state='active')
        self.view.category_add_btn.config(state='active')

    def disable_buttons(self):
        self.view.category_delete_btn.config(state='disabled')
        self.view.category_add_btn.config(state='disabled')

    def update_category_treeview(self):
        self.view.category_list_view.delete(*self.view.category_list_view.get_children())
        records = self.model.fetch_all()
        count = 0
        for record in records:
            category_id = record.get('category_id')
            category_name = record.get('category_name')
            department_name = record.get('department_name')

            self.view.category_list_view.insert(parent='',index='end',iid=count,text='',
                                                values=(category_id,category_name,department_name)
                                                )
            count += 1

    def add_item(self):
        category_name = self.view.category_input.get()
        department_id = self.view.department_id.get()
        self.model.insert_data(category_name,str(department_id))
        self.update_category_treeview()
        self.clear_input_boxes()

    def edit_item(self):
        self.disable_buttons()
        self.view.category_edit_btn.grid_forget()
        self.view.category_update_btn.grid(row=3, column=3)

    def update_item(self):
        category_name = self.view.category_input.get()
        selected = self.view.category_list_view.focus()
        category_id = self.view.category_list_view.item(selected, 'values')[0]
        self.model.update_data(category_name,category_id)
        self.update_category_treeview()
        self.enable_buttons()
        self.view.category_edit_btn.grid(row=3, column=3)
        self.view.category_update_btn.grid_forget()


    def delete_category(self):
        try:
            selected = self.view.category_list_view.focus()
            category_id = self.view.category_list_view.item(selected, 'values')[0]
            self.model.delete_data(category_id)
            self.view.category_list_view.delete(selected)
            self.clear_input_boxes()
        except IndexError:
            print('select')
        finally:
            print('congrats')

    def select_department(self,event):
        index = self.view.departments_list.current()
        department_id = self.department_options[index].get('department_id')
        department_name = self.department_options[index].get('department_name')
        self.view.department_id.set(department_id)
        self.view.department_name.set(department_name)
        

    def serach_category(self,event):
        category_name = self.view.search_box.get()
        records = self.model.search_category(category_name)
        self.view.category_list_view.delete(*self.view.category_list_view.get_children())
        count = 0
        for record in records:
            self.view.category_list_view.insert(parent='',index='end', iid=count, text='',
                                                        values = (record[0], record[1],record[2])
                                                        )
            count += 1