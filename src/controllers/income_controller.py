from src.controllers.base_controller import BaseController

class IncomeController(BaseController):
    def __init__(self,IncomeView,IncomeModel):
        super(IncomeController, self).__init__()
        self.income_model = IncomeModel()
        self.income_view = IncomeView(self)
        self.update_treeview()

    def clear_input_boxes(self):
        self.income_view.income_group_input.delete(0,'end')


    def select_item(self, e):
        self.clear_input_boxes()
        selected = self.income_view.income_group_view.focus()
        self.group_id,self.group_name = self.income_view.income_group_view.item(selected, 'values')
        asset_id = self.group_id
        self.income_view.income_group_input.insert(0, self.group_name)


    def enable_buttons(self):
        self.income_view.income_group_add.config(state='active')
        self.income_view.income_group_delete_btn.config(state='active')
        self.income_view.income_group_edit_btn.grid(row=4, column=2)
        self.income_view.income_group_update_btn.grid_forget()

    def disable_buttons(self):
        self.income_view.income_group_add.config(state='disabled')
        self.income_view.income_group_delete_btn.config(state='disabled')
    

    
    def update_treeview(self):
        records = self.income_model.fetch_all()
        self.income_view.income_group_view.delete(*self.income_view.income_group_view.get_children())

        count = 0
        for record in records:
            income_group_id = record.get('id')
            income_group_name = record.get('name')
            self.income_view.income_group_view.insert(parent='', index='end', iid=count, text='',
                values=(income_group_id,income_group_name))
            count += 1

    def add_item(self):
        income_group_name = self.income_view.income_group_input.get()
        self.income_model.insert_data(income_group_name)
        self.update_treeview()

    def edit_item(self):
        self.disable_buttons()
        self.income_view.income_group_edit_btn.grid_forget()
        self.income_view.income_group_update_btn.grid(row=4, column=2)

    def update_item(self):
        income_group_name = self.income_view.income_group_input.get()
        selected = self.income_view.income_group_view.focus()
        income_id = self.income_view.income_group_view.item(selected,'values')[0]
        self.income_model.update_data(income_group_name,str(income_id))
        self.update_treeview()
        self.enable_buttons()
        self.income_view.income_group_edit_btn.grid(row=4, column=2)
        self.income_view.income_group_update_btn.grid_forget()
        self.clear_input_boxes()

    def delete_item(self):
        try:
            selected = self.income_view.income_group_view.focus()
            income_id = self.income_view.income_group_view.item(selected, 'values')[0]
            self.income_model.delete_data(str(income_id))
            self.income_view.income_group_view.delete(selected)
            self.clear_input_boxes()
        except:
            print('select a value')
        finally:
            print('Congrats')


    def add_to_ledger(self):
        ledger_name = self.income_view.ledger_name_input.get()
        ledger_type = self.income_view.type_input.get()
        gst_percentage = self.income_view.gst_percentage_input.get()
        opening_balance = self.income_view.opening_balance_input.get()

        self.income_model.insert_to_ledger(ledger_name,ledger_type,gst_percentage,opening_balance,self.group_id)




