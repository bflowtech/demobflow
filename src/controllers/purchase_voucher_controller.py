from src.controllers.base_controller import BaseController
from tkinter import messagebox


from src.views.item_master_view import ItemMasterView
from src.models.Item_master_model import ItemMasterModel
from src.controllers.Item_master_controller import ItemMasterController

class PurchaseController(BaseController):
    def __init__(self,PurchaseView,PurchaseModel):
        super(PurchaseController, self).__init__()
        self.model = PurchaseModel()
        self.view = PurchaseView(self)
        self.set_items()

    def clear_input_boxes(self):
        self.view.barcode_entry.delete(0, 'end')
        self.view.batch_no_entry.delete(0, 'end')
        self.view.hsn_entry.delete(0, 'end')
        self.view.qty_entry.delete(0, 'end')
        self.view.disc_entry.delete(0, 'end')
        self.view.taxable_entry.delete(0, 'end')
        self.view.cgst_entry.delete(0, 'end')
        self.view.sgst_entry.delete(0, 'end')
        self.view.igst_entry.delete(0, 'end')
        self.view.cess_entry.delete(0, 'end')
        self.view.item_netamount_entry.delete(0, 'end')
        self.view.rate_entry.delete(0, 'end')

    def clear_items_list(self):
        self.view.my_tree.delete(*self.view.my_tree.get_children())
        self.view.data.clear()
   
    def select_item(self, e):
        pass

    def enable_buttons(self):
        pass

    def disable_buttons(self):
        pass
    
    def set_items(self):
        self.items_options = self.model.get_items('')
        self.party_account_options = self.model.get_party_accounts()
        self.purchase_ledgers = self.model.get_purchase_ledgers()
        self.cess_options = self.model.get_cess_details()

        self.view.items_combobox['values'] = [item.get('item_name') for item in self.items_options]
        self.view.party_account_type['values'] = [party_account.get('ledger_name') for party_account in self.party_account_options]
        self.view.gst_type['values'] = ('IntraState','InterState')
        self.view.ledger_combobox['values'] = [ledger.get('ledger_name') for ledger in self.purchase_ledgers]
        self.view.cess_entry['values'] = [cess.get('cess_name') for cess in self.cess_options]

    def cess_selected(self,event):
        index = self.view.cess_entry.current()
        cess_id = self.cess_options[index].get('cess_id')
        cess_name = self.cess_options[index].get('cess_name')

        self.view.cess_clicked.set(cess_name)
        self.view.cess_clicked_id.set(cess_id)

        taxable_amount = self.view.taxable_entry.get()
        cess_percentage = int(self.model.get_cess_percentage(cess_id))

        cess_amount = float(taxable_amount) * cess_percentage / 100

        net_amount = cess_amount + self.net_amount
        self.view.item_netamount_entry.delete(0,'end')
        self.view.item_netamount_entry.insert(0,net_amount)

    def supplier_selected(self,event):
        index = self.view.party_account_type.current()
        ledger_id = self.party_account_options[index].get('ledger_id')

        supplier_gst_number = self.model.get_supplier_gst_number(ledger_id)
        company_gst_number = self.model.get_company_gst_number()
    
        if not company_gst_number[0:2] == supplier_gst_number[0:2]:
            self.view.gst_type_value.set('IntraState')
            self.view.cgst_entry.delete(0,'end')
            self.view.sgst_entry.delete(0,'end')
            self.view.cgst_entry.insert(0,0)
            self.view.sgst_entry.insert(0,0)
            self.view.cgst_entry.config(state='disable')
            self.view.sgst_entry.config(state='disable')
            self.view.igst_entry.config(state='normal')
            self.view.igst_entry.delete(0,'end')
        else:
            self.view.gst_type_value.set('InterState')
            self.view.igst_entry.delete(0,'end')
            self.view.igst_entry.insert(0,0)
            self.view.igst_entry.config(state='disable')
            self.view.cgst_entry.config(state='normal')
            self.view.sgst_entry.config(state='normal')
            self.view.cgst_entry.delete(0,'end')
            self.view.sgst_entry.delete(0,'end')

    def lpo_changed(self,event):
        lpo_no = self.view.lpo_entry.get()
        self.view.data = self.model.lpo_fetch(lpo_no)
        records = self.view.data
        self.treeview_update(records)
        if records:
            date = records[0][-1]
            gst_type = records[0][-2]
            party_ac_details = records[0][-3]

            self.view.party_ac_value.set(party_ac_details)
            self.view.gst_type_value.set(gst_type)
            self.view.party_account_date.delete(0,'end')
            self.view.party_account_date.insert(0,date)
        
    def treeview_update(self,records):
        count = 1
        self.view.my_tree.delete(*self.view.my_tree.get_children())
        for record in records:
            barcode = record.get('barcode')
            batch_no = record.get('batch_no')
            item_name = record.get('item').get('name')
            hsn_no = record.get('hsn_no')
            quantity = record.get('quantity')
            rate = record.get('rate')
            discount = record.get('discount')
            taxable_amount = record.get('taxable_amount')
            cgst = record.get('cgst')
            sgst = record.get('sgst')
            igst = record.get('igst')
            cess = record.get('cess').get('name')
            ledger_name = record.get('ledger').get('name')
            total_amount = record.get('total_amount')
            mfg_date = record.get('mfg_date').replace('/', '-')
            expiry_date = record.get('expiry_date').replace('/', '-')

            if count % 2 == 0:
                self.view.my_tree.insert(parent='',index='end',iid=count,values=(
                    count,barcode,batch_no,item_name,hsn_no,quantity,rate,discount,taxable_amount,cgst,sgst,igst,cess,ledger_name,mfg_date,expiry_date,total_amount),
                tags=('evenrow',)
                )
            else:
                self.view.my_tree.insert(parent='',index='end',iid=count,values=(
                    count,barcode,batch_no,item_name,hsn_no,quantity,rate,discount,taxable_amount,cgst,sgst,igst,cess,ledger_name,mfg_date,expiry_date,total_amount),
                tags=('evenrow',)
                )
            count += 1

    def ledger_clicked(self,event):
        index = self.view.ledger_combobox.current()
    
        ledger_id = self.purchase_ledgers[index].get('ledger_id')
        ledger_name = self.purchase_ledgers[index].get('ledger_name')

        self.view.ledger_clicked_id.set(ledger_id)
        self.view.ledger_clicked.set(ledger_name)
        
        gst_percentage = int(self.model.get_ledger_gst_percentage(ledger_id))
        self.view.gst_entry.delete(0,'end')
        self.view.gst_entry.insert(0,gst_percentage)

        total_amount = float(self.view.item_netamount_entry.get())
        self.gst_amount = total_amount*gst_percentage/100

        if self.view.gst_type.get() == "InterState":
            self.view.cgst_entry.delete(0,'end')
            self.view.sgst_entry.delete(0,'end')
            self.view.cgst_entry.insert(0,self.gst_amount/2)
            self.view.sgst_entry.insert(0,self.gst_amount/2)
        else:
            self.view.igst_entry.delete(0,'end')
            self.view.igst_entry.insert(0,self.gst_amount)
        
        self.net_amount = total_amount + self.gst_amount
        self.view.item_netamount_entry.delete(0,'end')
        self.view.item_netamount_entry.insert(0,self.net_amount)

    def add_to_purchase_list(self):
        barcode = self.view.barcode_entry.get()
        batch_no = self.view.batch_no_entry.get()
        hsn_no = self.view.hsn_entry.get()
        quantity = self.view.qty_entry.get()
        rate = self.view.rate_entry.get()
        discount = self.view.disc_entry.get()
        taxable_amount = self.view.taxable_entry.get()
        cgst = self.view.cgst_entry.get()
        sgst = self.view.sgst_entry.get()
        igst = self.view.igst_entry.get()
        cess_name = self.view.cess_clicked.get()
        cess_id = self.view.cess_clicked_id.get() 
        total_amount = self.view.item_netamount_entry.get()
        item_name = self.view.item_clicked.get()
        item_id = self.view.item_clicked_id.get()
        ledger_id = self.view.ledger_clicked_id.get()
        ledger_name = self.view.ledger_clicked.get()
        mfg_date = self.view.manufacturing_date_entry.get()
        expiry_date = self.view.expiry_date_entry.get()

        purchase_data = {
                        'barcode':barcode,
                        'batch_no':batch_no,
                        'item':{'name':item_name,'id':item_id},
                        'hsn_no':hsn_no,
                        'quantity':quantity,
                        'rate':rate,
                        'discount':discount,
                        'taxable_amount':taxable_amount,
                        'cgst':cgst,
                        'sgst':sgst,
                        'igst':igst,
                        'cess':{'id':cess_id,'name':cess_name},
                        'ledger':{'name':ledger_name,'id':ledger_id},
                        'total_amount':total_amount,
                        'mfg_date':mfg_date,
                        'expiry_date':expiry_date
                        }

        self.view.data.append(purchase_data)
        self.treeview_update(self.view.data)

    def remove_from_purchase_list(self):
        selected_item = self.view.my_tree.focus()
        self.view.data.pop(int(selected_item)-int(1))
        self.treeview_update(self.view.data)

    def selected_item(self,event):
        index = self.view.items_combobox.current()
        item_id = self.items_options[index].get('item_id')
        item_name = self.items_options[index].get('item_name')
        self.view.item_clicked_id.set(item_id)
        self.view.item_clicked.set(item_name)
        self.clear_input_boxes()
        item_details = self.model.get_item_details(str(item_id))
        self.view.barcode_entry.insert(0, item_details.get('barcode'))
        self.view.hsn_entry.insert(0, item_details.get('hsn_code'))
        self.view.rate_entry.insert(0, item_details.get('selling_price'))

    def item_onchage(self,*args):
        item_name = self.view.items_combobox.get()
        self.view.items_combobox['values'] = self.model.get_items(item_name)
        if not self.model.get_items(item_name):
            item_create_dialogbox = messagebox.askyesno('No Item Available','Do You Want To Create An Item')
            if item_create_dialogbox:
                self.item_master_ctrl = ItemMasterController(ItemMasterView,ItemMasterModel)
                self.item_master_ctrl.view.protocol('WM_DELETE_WINDOW',self.close_item_master)
            else:
                pass

    def close_item_master(self):
        self.item_master_ctrl.view.destroy()
        self.set_items()

    def on_qty_changed(self,event):
        self.view.item_netamount_entry.delete(0,'end')
        self.view.taxable_entry.delete(0,'end')
        try:
            quantity = int(self.view.qty_entry.get())
            if quantity < 0 :
                self.validate_quantity() 
            rate = self.view.rate_entry.get()
            total_rate = int(quantity) * float(rate)
            self.view.item_netamount_entry.insert(0,total_rate)
            self.view.taxable_entry.insert(0,total_rate)
        except ValueError:
            messagebox.showinfo("Warning", 'Enter An Number')
            self.view.qty_entry.delete(0,'end')

    def on_disc_changed(self,event):
        taxable_amount = self.view.taxable_entry.get()
        discount_amount = self.view.disc_entry.get()
        item_net_amount = float(self.view.item_netamount_entry.get())
        gst_percentage = self.view.gst_entry.get()

        if event.keysym == "BackSpace":
            if not gst_percentage == "":
                print(gst_percentage)
                taxable_amount = item_net_amount - self.gst_amount
                try:
                    final_taxable_amount = taxable_amount - float(discount_amount)
                except ValueError:
                    discount_amount = 0
                    final_taxable_amount = taxable_amount - float(discount_amount)
            else:
                try:
                    final_taxable_amount = item_net_amount - float(discount_amount)
                except ValueError:
                    discount_amount = 0
                    final_taxable_amount = item_net_amount - discount_amount
        else:
            try:
                final_taxable_amount = float(taxable_amount) - float(discount_amount)
            except ValueError:
                discount_amount = 0
                final_taxable_amount = float(taxable_amount) - float(discount_amount)
        
        self.view.taxable_entry.delete(0,'end')
        self.view.taxable_entry.insert(0,final_taxable_amount)

    def validate_quantity(self):
        quantity = int(self.view.qty_entry.get())
        messagebox.showinfo("Warning", 'Quantity Doesnot Less Than Zero')
        if quantity < 0:
            self.view.qty_entry.delete(0,'end')
            
    def create_purchase(self,*events):
        purchase_voucher = self.view.purchase_voucher_entry.get()
        party_account_type = self.view.party_account_type.get()
        invoice_no = self.view.invoice_no_entry.get()
        party_account_date = self.view.party_account_date.get().replace('/','-')
        invoice_date = self.view.invoice_date.get().replace('/','-')
        gst_type = self.view.gst_type.get()

        purchase_voucher_id = self.model.insert_data(invoice_no,purchase_voucher,party_account_type,gst_type,party_account_date,invoice_date)
        self.model.insert_items_data(self.view.data,str(purchase_voucher_id))
        self.clear_input_boxes()
        self.clear_items_list()
        messagebox.showinfo("Purchased", 'Purchase Completed')

    def purchase_list_transaction(self):
        records = self.model.get_purchase_transactions()
        count = 1
        for record in records:
            count = count + 1
            self.view.purchase_voucher_view.insert(parent='',index='end',iid=count,
            values=(record[0],record[1],record[2],record[3],
            record[4],record[5],record[6]))