from src.controllers.base_controller import BaseController

class UnitsController(BaseController):
    def __init__(self,UnitsView,UnitsModel):
        super(UnitsController, self).__init__()
        self.units_model = UnitsModel()
        self.units_view = UnitsView(self)
        self.update_units_treeview()

    def update_units_treeview(self):
        self.units_view.units_list_view.delete(*self.units_view.units_list_view.get_children())
        records = self.units_model.fetch_all()
        count = 1
        for record in records:
            unit_id = record.get('units_id')
            unit_name = record.get('unit_name')
            no_of_decimal = record.get('no_of_decimal')
            self.units_view.units_list_view.insert(parent='', index='end', iid=count, text='',
                                   values=(unit_id, unit_name, no_of_decimal))
            count += 1

    def clear_input_boxes(self):
        self.units_view.units_input.delete(0, 'end')
        self.units_view.decimal_input.delete(0, 'end')

    def select_item(self,e):
        self.clear_input_boxes()
        selected = self.units_view.units_list_view.focus()
        values = self.units_view.units_list_view.item(selected, 'values')
        self.units_view.units_input.insert(0, values[1])
        self.units_view.decimal_input.insert(0, values[2])

    def enable_buttons(self):
        self.units_view.units_delete_btn.config(state='active')
        self.units_view.units_add_btn.config(state='active')

    def disable_buttons(self):
        self.units_view.units_delete_btn.config(state='disabled')
        self.units_view.units_add_btn.config(state='disabled')

    def add_item(self):
        unit_name = self.units_view.units_input.get()
        no_of_decimal = self.units_view.decimal_input.get()
        self.units_model.insert_data(unit_name,no_of_decimal)
        self.clear_input_boxes()
        self.update_units_treeview()

    def edit_item(self):
        self.disable_buttons()
        self.units_view.units_edit_btn.grid_forget()
        self.units_view.units_update_btn.grid(row=3, column=3)

    def update_item(self):
        unit_name =  self.units_view.units_input.get()
        selected =  self.units_view.units_list_view.focus()
        unit_id =  self.units_view.units_list_view.item(selected, 'values')[0]
        no_of_decimal =  self.units_view.decimal_input.get()
        self.units_model.update_data(unit_name,no_of_decimal,unit_id)
        self.update_units_treeview()
        self.enable_buttons()
        self.units_view.units_edit_btn.grid(row=3, column=3)
        self.units_view.units_update_btn.grid_forget()
        self.clear_input_boxes()

    def delete_item(self):
        try:
            selected = self.units_view.units_list_view.focus()
            unit_id = self.units_view.units_list_view.item(selected, 'values')[0]
            self.units_model.delete_data(str(unit_id))
            self.units_view.units_list_view.delete(selected)
            self.clear_input_boxes()
            self.update_units_treeview()
        except IndexError:
            print('Nothing Selected')
        finally:
            print('congrats')