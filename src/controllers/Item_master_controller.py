import re
from src.controllers.base_controller import BaseController
from src.helpers import get_id_from_string
import random
from tkinter import messagebox

class ItemMasterController(BaseController):
    def __init__(self, ItemMasterView, ItemMasterModel):
        super(ItemMasterController, self).__init__()
        self.model = ItemMasterModel()
        self.view = ItemMasterView(self)

        self.brands_onchange('')
        self.departments_onchange('')
        self.category_onchange('')
        self.sub_category_onchange('')
        self.units_onchange('')
        self.load_alternative_units()
        self.racks_onchange('')
        self.cess_onchange('')
        self.gst_onchange('')


    def clear_input_boxes(self):
        self.view.barcode.delete(0, 'end')
        self.view.item_code.delete(0, 'end')
        self.view.hsn_code.delete(0, 'end')
        self.view.item_name.delete(0, 'end')
        self.view.cost_price.delete(0, 'end')
        self.view.margin_amount_input.delete(0, 'end')
        self.view.selling_price_input.delete(0, 'end')
        self.view.mrp_price_input.delete(0, 'end')
        self.view.rack_no_input.delete(0, 'end')
        self.view.opening_qty_input.delete(0, 'end')

    def select_item(self, e):
        pass

    def enable_buttons(self):
        pass

    def disable_buttons(self):
        pass

    def update_item_master_treeview(self):
        self.view.item_master_list_view.delete(*self.view.item_master_list_view.get_children())
        records = self.model.fetch_all()
        count = 0
        for record in records:

            item_id = record.get('item_id')
            item_name = record.get('item_name')
            barcode = record.get('barcode')
            hsn_code = record.get('hsn_code')
            brand_name = record.get('brand_name')
            department_name = record.get('department_name')
            category_name = record.get('category_name')
            sub_category_name = record.get('sub_category_name')
            unit_name = record.get('unit_name')
            cost_price = record.get('cost_price')
            gst_percentage = record.get('gst_percentage')
            cess_percentage = record.get('cess_percentage')
            margin_amount = record.get('margin')
            selling_price = record.get('selling_price')
            mrp = record.get('mrp')
            rack_name = record.get('rack_name')
            opening_qty = record.get('opening_qty')
            expiry = record.get('expiry')

            self.view.item_master_list_view.insert(parent='', index='end', iid=count, text='',
                                                        values=(item_id, item_name,barcode,hsn_code,
                                                                brand_name, department_name, category_name,sub_category_name,
                                                               unit_name, cost_price,gst_percentage,cess_percentage,
                                                               margin_amount, selling_price, mrp,rack_name,
                                                                opening_qty,expiry
                                                                ))
            count += 1

    def racks_onchange(self,event):
        rack_name = self.view.rack_no_input.get()
        self.racks_options = self.model.get_racks(rack_name)
        self.view.rack_no_input['values'] = [rack.get('rack_name') for rack in self.racks_options]

    def rack_selected(self,event):
        index = self.view.rack_no_input.current()
        rack_id = self.racks_options[index].get('rack_id')
        rack_name = self.racks_options[index].get('rack_name')
        self.view.rack_clicked_id.set(rack_id)
        self.view.rack_clicked.set(rack_name)

    def brands_onchange(self,event):
        brand_name = self.view.brand_list.get()
        self.brands_options = self.model.get_brands(brand_name)
        self.view.brand_list['values'] = [brand.get('brand_name') for brand in self.brands_options]

    def brands_selected(self,event):
        index = self.view.brand_list.current()
        brand_id = self.brands_options[index].get('brand_id')
        brand_name = self.brands_options[index].get('brand_name')
        self.view.brand_clicked_id.set(brand_id)
        self.view.brand_clicked.set(brand_name)

    def units_onchange(self,event):
        unit_name = self.view.unit_input.get()
        self.unit_options = self.model.unit_fetch(unit_name)
        self.view.unit_input['values'] = [unit_option.get('unit_name') for unit_option in self.unit_options]
        
    def units_selected(self,event):
        index = self.view.unit_input.current()
        unit_id = self.unit_options[index].get('unit_id')
        unit_name = self.unit_options[index].get('unit_name')
        self.view.unit_clicked_id.set(unit_id)
        self.view.unit_clicked.set(unit_name)
        self.load_alternative_units()

    def departments_onchange(self,event):
        department_name = self.view.department_combobox.get()
        self.department_options = self.model.get_departments(department_name)
        self.view.department_combobox['values'] = [department.get('department_name') for department in self.department_options]

    def departments_selected(self,event):
        index = self.view.department_combobox.current()
        department_id = self.department_options[index].get('department_id')
        department_name = self.department_options[index].get('department_name')

        self.view.department_clicked_id.set(department_id)
        self.view.department_clicked.set(department_name)


    def category_onchange(self,event):
        category_name = self.view.category_list.get()
        self.category_options = self.model.get_category(category_name)
        self.view.category_list['values'] = [category.get('category_name') for category in self.category_options]

    def category_selected(self,event):
        index = self.view.category_list.current()
        category_id = self.category_options[index].get('category_id')
        category_name = self.category_options[index].get('category_name')
        self.view.category_clicked_id.set(category_id)
        self.view.category_clicked.set(category_name)

    def sub_category_onchange(self,event):
        sub_category_name = self.view.sub_category_list.get()
        self.sub_category_options = self.model.get_sub_category(sub_category_name)
        self.view.sub_category_list['values'] = [sub_category.get('sub_category_name') for sub_category in self.sub_category_options]


    def sub_category_selected(self,event):
        index = self.view.sub_category_list.current()
        sub_category_id = self.sub_category_options[index].get('sub_category_id')
        sub_category_name = self.sub_category_options[index].get('sub_category_name')
        self.view.sub_category_clicked_id.set(sub_category_id)
        self.view.sub_category_clicked.set(sub_category_name)

    def load_alternative_units(self):
        selected_unit = self.view.unit_clicked.get()
        self.view.alternative_label.config(text=selected_unit)
        self.alternative_unit_options = self.model.get_alternate_units(selected_unit)
        self.view.alternative_unit_input['values'] = [alternative_unit.get('unit_name') for alternative_unit in self.alternative_unit_options]

    def onchange_alternative_units(self,event):
        selected_unit = self.view.unit_clicked.get()
        alternative_unit = self.view.alternative_unit_entry.get()
        alternative_options = self.model.get_alternate_units(selected_unit)
        self.view.alternative_unit_input['values'] = alternative_options
        index = self.view.alternative_unit_input.current()
        
        self.view.alternative_unit_clicked_id.set(alternative_options[index][0])
        self.view.alternative_unit_clicked.set(alternative_options[index][1])


    def gst_onchange(self,event):
        gst_name = self.view.gst_percentage_input.get()
        self.gst_options = self.model.gst_fetch(gst_name)
        self.view.gst_percentage_input['values'] = [gst.get('gst_name') for gst in self.gst_options]

    def gst_selected(self,*args):
        index = self.view.gst_percentage_input.current()
        gst_id = self.gst_options[index].get('gst_id')
        gst_name = self.gst_options[index].get('gst_name')
        self.view.gst_clicked_id.set(gst_id)
        self.view.gst_clicked.set(gst_name)

        #calculate
        gst_id = self.view.gst_clicked_id.get()
        self.cost_price = self.view.cost_price.get()
        gst_percentage = self.model.get_gst_percentage(str(gst_id))
        

        try:
            self.total_gst_amount = float(self.cost_price)*int(gst_percentage)/100
            self.view.gst_amount_input.config(state='normal')
            self.view.gst_amount_input.delete(0,'end')
            self.view.gst_amount_input.insert(0,self.total_gst_amount)
            self.view.gst_amount_input.config(state='readonly')
        except ValueError:
            messagebox.showinfo("Warning", 'Enter Cost Price')

    
    def cess_onchange(self,*args):
        cess_name = self.view.cess_percentage_input.get()
        self.cess_options = self.model.ces_fetch(cess_name)
        self.view.cess_percentage_input['values'] = [cess.get('cess_name') for cess in self.cess_options]

    def cess_selected(self,event):
        index = self.view.cess_percentage_input.current()
        cess_id = self.cess_options[index].get('cess_id')
        cess_name = self.cess_options[index].get('cess_name')
        self.view.cess_clicked_id.set(cess_id)
        self.view.cess_clicked.set(cess_name)

        #calculate
        cess_id = self.view.cess_clicked_id.get()
        cess_percentage = self.model.get_cess_percentage(str(cess_id))

        try:
            total_cess_amount = float(self.cost_price) * int(cess_percentage) / 100
            self.view.cess_amount_input.config(state='normal')
            self.view.cess_amount_input.delete(0,'end')
            self.view.cess_amount_input.insert(0,total_cess_amount)
            self.view.cess_amount_input.config(state='readonly')
        except ValueError:
            messagebox.showinfo("Warning", 'Enter Cost Price')


    def generate_barcode(self):
        barcode = []
        for i in range(0,12):
            temp = random.randint(5,9)
            barcode.append(temp)
        
        self.view.barcode.config(state='normal')
        self.view.barcode.delete(0, 'end')
        self.view.barcode.insert(0, barcode)
        self.view.barcode.config(state='readonly')



    def add_item_btn(self):
        barcode = self.view.barcode.get()
        item_code = self.view.item_code.get()
        hsn_code = self.view.hsn_code.get()
        item_name = self.view.item_name.get()
        unit_id = str(self.view.unit_clicked_id.get())
        brand_id = str(self.view.brand_clicked_id.get())
        department_id = str(self.view.department_clicked_id.get())
        category_id = str(self.view.category_clicked_id.get())
        sub_category_id = str(self.view.sub_category_clicked_id.get())
        cost_price = self.view.cost_price.get()
        gst_id = str(self.view.gst_clicked_id.get())
        cess_id = str(self.view.cess_clicked_id.get())
        margin_amount = self.view.margin_amount_input.get()
        selling_price = self.view.selling_price_input.get()
        mrp_price = self.view.mrp_price_input.get()
        rack_id = str(self.view.rack_clicked_id.get())
        opening_qty = self.view.opening_qty_input.get()
        alternative_unit_id = str(self.view.alternative_unit_clicked_id.get())
        alternative_unit = self.view.alternative_unit_entry.get()
        expiry_date = self.view.expiry_input.get().replace('/', '-')

        self.model.insert_data(item_name,item_code,hsn_code,barcode,
                                            brand_id,department_id,category_id,sub_category_id,cost_price,
                                            gst_id,cess_id,margin_amount,selling_price,mrp_price,rack_id,
                                            opening_qty,unit_id,alternative_unit_id,alternative_unit,expiry_date)
        print('item addedd and cleared inputboxes')
        self.clear_input_boxes()
        return True


    def get_brand_types(self):
        records = self.model.get_brand_types()
        return records

    def get_brands(self):
        records = self.model.get_brands()
        return records

    def add_unit(self):
        unit_name = self.view.units_input.get()
        no_of_decimal = self.view.decimal_input.get()
        self.model.insert_unit_data(unit_name,no_of_decimal)
        self.load_units()
        self.view.unit_window.destroy()

    def brand_on_changes(self,*args):
        brand_options = self.model.get_brand_types()
        self.view.brand_types_dropdown['values'] = brand_options
        index = self.view.brand_types_dropdown.current()
        self.view.brandtype_clicked_id.set(brand_options[index][0])
        self.view.brandtype_clicked.set(brand_options[index][1])

    def add_brand(self):
        brand_name = self.view.brands_input.get()
        brand_type_id = self.view.brandtype_clicked_id.get()
        self.model.insert_brand_data(brand_name,str(brand_type_id))
        self.load_brands()
        self.view.brand_window.destroy()


    def add_department(self):      
        department_name = self.view.department_input.get()
        self.model.insert_department_data(department_name)
        self.load_departments()
        self.view.department_window.destroy()

    def get_departments(self):
        records = self.model.get_departments()
        return records

    def department_on_changes(self,*args):
        department_options = self.model.get_departments()
        self.view.departments_dropdown['values'] = department_options
        index = self.view.departments_dropdown.current()
        self.view.department_clicked_id.set(department_options[index][0])
        self.view.department_clicked.set(department_options[index][1])


    def add_category(self):
        category_name = self.view.category_input.get()
        department_id = self.view.department_clicked_id.get()
        self.model.insert_category_data(category_name,str(department_id))
        self.load_category()
        self.view.category_window.destroy()

    def get_category(self):
        records = self.model.get_category()
        return records

    def category_on_change(self,*args):
        category_options = self.model.get_category()
        self.view.category_dropdown['values'] = category_options
        index = self.view.category_dropdown.current()
        self.view.category_clicked_id_pop.set(category_options[index][0])
        self.view.category_clicked_popup.set(category_options[index][1])

    def add_sub_category(self):
        sub_category_name = self.view.sub_category_input.get()
        category_id = self.view.category_clicked_id_pop.get()
        self.model.insert_sub_category_data(sub_category_name,str(category_id))
        self.load_sub_category()
        self.view.sub_category_window.destroy()

    
    #Cost summary calculation

    def cost_price_onchange(self,event):
        self.cost_price = self.view.cost_price.get()

    def selling_price_onchange(self,event):
        self.selling_price = self.view.selling_price_input.get()
        gst_id = str(self.view.gst_clicked_id.get())
        gst = int(self.model.get_gst_percentage(gst_id))


        cost_profit = int(self.selling_price)*100/(100+gst)
        sale_output_gst = cost_profit * gst / 100
        margin_amount = cost_profit - int(self.cost_price)
        margint_percentage = margin_amount / int(self.cost_price) * 100
        
        # print('costprice',self.cost_price)
        # print('Gst Percentage',gst)
        # print('sellingPrice',self.selling_price)


        # print('margin',margin_amount)
        # print('Margin %',margint_percentage)
        # print('sale outputGst',sale_output_gst)

        self.view.sale_output_tax.delete(0, 'end')
        self.view.margin_percentage.delete(0,'end')
        self.view.margin_amount_input.delete(0,'end')

        self.view.sale_output_tax.insert(0, sale_output_gst)
        self.view.margin_percentage.insert(0,margint_percentage)
        self.view.margin_amount_input.insert(0,margin_amount)

    def margin_amount_onchange(self,event):
        margin_amount = int(self.view.margin_amount_input.get())
        cost_margin = int(self.cost_price) + int(margin_amount)
        gst_id = str(self.view.gst_clicked_id.get())
        gst = int(self.model.get_gst_percentage(gst_id))

        sale_output_gst = cost_margin * gst / 100
        selling_price = int(self.cost_price) + int(margin_amount) + sale_output_gst
        margin_percentage = margin_amount / int(self.cost_price) * 100


        self.view.selling_price_input.delete(0, 'end')
        self.view.margin_percentage.delete(0, 'end')
        self.view.sale_output_tax.delete(0, 'end')
        
        self.view.selling_price_input.insert(0,selling_price)
        self.view.margin_percentage.insert(0,margin_percentage)
        self.view.sale_output_tax.insert(0,sale_output_gst)

    def margin_percentage_onchange(self,event):
        margin_percentage = int(self.view.margin_percentage.get())
        gst_id = str(self.view.gst_clicked_id.get())
        gst = int(self.model.get_gst_percentage(gst_id))
        margin_amount = int(self.cost_price) * margin_percentage / 100
        sale_output_gst =  (int(self.cost_price) + margin_amount) * gst / 100
        selling_price = int(self.cost_price) + int(margin_amount) + sale_output_gst

        self.view.margin_amount_input.delete(0, 'end')
        self.view.selling_price_input.delete(0, 'end')
        self.view.sale_output_tax.delete(0, 'end')

        self.view.margin_amount_input.insert(0,margin_amount)
        self.view.selling_price_input.insert(0,selling_price)
        self.view.sale_output_tax.insert(0,sale_output_gst)








