from abc import ABC,abstractmethod

class BaseController(ABC):

    @abstractmethod
    def clear_input_boxes(self):
        pass

    @abstractmethod
    def select_item(self, e):
        pass

    @abstractmethod
    def enable_buttons(self):
        pass

    @abstractmethod
    def disable_buttons(self):
        pass
