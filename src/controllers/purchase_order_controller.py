from src.controllers.base_controller import BaseController
from tkinter import messagebox

class PurchaseOrderController(BaseController):
    def __init__(self,PurchaseOrderView,PurchaseOrderModel):
        super(PurchaseOrderController, self).__init__()
        self.model = PurchaseOrderModel()
        self.view = PurchaseOrderView(self)
        self.set_items()

    def clear_input_boxes(self):
        self.view.barcode_entry.delete(0, 'end')
        self.view.batch_no_entry.delete(0, 'end')
        self.view.hsn_entry.delete(0, 'end')
        self.view.qty_entry.delete(0, 'end')
        self.view.total_amount_entry.delete(0, 'end')
        self.view.rate_entry.delete(0, 'end')

    def clear_items_list(self):
        self.view.my_tree.delete(*self.view.my_tree.get_children())
        self.view.data.clear()

    def select_item(self, e):
        pass

    def enable_buttons(self):
        pass

    def disable_buttons(self):
        pass
    
    def set_items(self):
        self.items_options = self.model.get_items()
        self.view.items_combobox['values'] = [item.get('item_name') for item in self.items_options]
        self.party_accounts_option = self.model.get_party_accounts()
        self.view.party_account_type['values'] = [party_account.get('ledger_name') for party_account in self.party_accounts_option]

    def item_onchage(self,*args):
        index = self.view.items_combobox.current()
        item_id,item_name = self.view.items_combobox['values'][index]
        self.view.item_clicked_id.set(item_id)
        self.view.item_clicked.set(item_name)
        self.clear_input_boxes()
        barcode,batch_no,hsn_code,selling_price = self.model.get_item_details(str(item_id))

        self.view.barcode_entry.insert(0,barcode)
        self.view.batch_no_entry.insert(0,batch_no)
        self.view.hsn_entry.insert(0,hsn_code)
        self.view.rate_entry.insert(0,selling_price)

    def update_order_list_view(self,records):
        count = 1
        self.view.my_tree.delete(*self.view.my_tree.get_children())
        for record in records:
            barcode = record.get('barcode')
            batch_no = record.get('batch_no')
            hsn_no = record.get('hsn_no')
            quantity = record.get('quantity')
            rate = record.get('rate')
            total_amount = record.get('total_amount')
            item_name = record.get('item').get('name')
            if count % 2 == 0:
                self.view.my_tree.insert(parent='',index='end',iid=count,values=(
                    count,barcode,batch_no,item_name,hsn_no,quantity,rate,total_amount),
                tags=('oddrow',)
                )
            else:
                self.view.my_tree.insert(parent='',index='end',iid=count,values=(
                    count,barcode,batch_no,item_name,hsn_no,quantity,rate,total_amount),
                tags=('evenrow',)
                )
            count += 1

    def add_to_order_list(self):
        barcode = self.view.barcode_entry.get()
        batch_no = self.view.batch_no_entry.get()
        hsn_no = self.view.hsn_entry.get()
        quantity = self.view.qty_entry.get()
        rate = self.view.rate_entry.get()
        total_amount = self.view.total_amount_entry.get()
        item_name = self.view.item_clicked.get()
        item_id = self.view.item_clicked_id.get()

        orders_list = {
            'barcode':barcode,
            'batch_no':batch_no,
            'hsn_no':hsn_no,
            'quantity':quantity,
            'rate':rate,
            'total_amount':total_amount,
            'item':{'id':item_id,'name':item_name}
        }

        self.view.data.append(orders_list)
        self.update_order_list_view(self.view.data)

    def remove_from_order_list(self):
        selected_item = self.view.my_tree.focus()
        self.view.data.pop(int(selected_item)-1)

        self.update_order_list_view(self.view.data)

    def on_qty_changed(self,event):
        quantity = int(self.view.qty_entry.get())
        rate = float(self.view.rate_entry.get())
        total_amount = quantity * rate
        self.view.total_amount_entry.delete(0,'end')
        self.view.total_amount_entry.insert(0,total_amount)
 
    def create_order(self):
        party_account_type = self.view.party_account_type.get()
        reference_no = self.view.reference_no_entry.get()
        order_no = self.view.order_no_entry.get()

        purchase_order_id = self.model.insert_data(order_no,party_account_type,reference_no)
        items_list = self.view.data
        self.model.insert_items_data(items_list,purchase_order_id)
        
        messagebox.showinfo("Congrats", 'Purchase Order Generated')
        self.clear_input_boxes()
        self.clear_items_list()
    
    def party_account_selected(self,event):
        index = self.view.party_account_type.current()
        party_account_id = self.party_accounts_option[index].get('ledger_id')
        party_account_name = self.party_accounts_option[index].get('ledger_name')
        self.view.party_account_id.set(party_account_id)
        self.view.party_account_name.set(party_account_name)

    def item_selected(self,event):
        index = self.view.items_combobox.current()
        item_id = self.items_options[index].get('item_id')
        item_name = self.items_options[index].get('item_name')
        self.view.item_clicked_id.set(item_id)
        self.view.item_clicked.set(item_name)