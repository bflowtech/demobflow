import re
from src.controllers.base_controller import BaseController
import xlsxwriter
import json
import decimal

class DecimalEncoder(json.JSONEncoder):
    def default(self, o):
        if isinstance(o, decimal.Decimal):
            return str(o)
        return super(DecimalEncoder, self).default(o)


class StocksReportController(BaseController):
    def __init__(self, StocksReportView, StockReportsModel):
        super(StocksReportController, self).__init__()
        self.model = StockReportsModel()
        self.view = StocksReportView(self)
        # self.search_stocks('')


    def clear_input_boxes(self):
        pass

    def select_item(self, e):
        pass

    def enable_buttons(self):
        pass

    def disable_buttons(self):
        pass

    
    def stocks_update(self,records):
        count = 1
        self.view.stock_report_view.delete(*self.view.stock_report_view.get_children())
        for record in records:
            name = record.get('name')
            department_name = record.get('department')
            opening_qty = record.get('opening_qty')
            selling_price = record.get('selling_price')
            outward_qty = record.get('outward_qty')
            purchase_price = record.get('purchase_rate')
            inward_qty = record.get('inward_qty')
            margin_amount = record.get('margin_amount')
            margint_percentage = record.get('margin_percentage')

            closing_stock = record.get('closing_stock')
            closing_value = record.get('closing_value')
            
            self.view.stock_report_view.insert(
            parent='',index='end',iid=count,
            values = (
                    count,name,department_name,opening_qty,inward_qty,purchase_price,outward_qty,selling_price,margin_amount,margint_percentage,closing_stock,closing_value
                    )
                )
        
            count += 1

        
    def search_stocks(self,e):
        item_name = self.view.search_stock_entry.get()
        records = self.model.search_item(item_name)
        self.stocks_update(records)


    def category_selected(self,event):
        category_type = self.view.category_dropdown.get()
        if category_type == 'Brands':
            records = self.model.fetch_brands()
            self.view.stock_report_view.heading('Name', text='Brand Name')
            self.view.stock_report_view.column('Margin Amount',width=0,minwidth=0,stretch='no')
            self.view.stock_report_view.column('Margin %',width=0,minwidth=0,stretch='no')
            self.view.stock_report_view.column('Purchase Price',width=0,minwidth=0,stretch='no')
            self.view.stock_report_view.column('Selling Price',width=0,minwidth=0,stretch='no')
            self.view.stock_report_view.column('Department',width=0,minwidth=0,stretch='no')
        elif category_type == 'Category':
            records = self.model.fetch_category()
            self.view.stock_report_view.heading('Name', text='Category Name')
            self.view.stock_report_view.column('Margin Amount',width=0,minwidth=0,stretch='no')
            self.view.stock_report_view.column('Margin %',width=0,minwidth=0,stretch='no')
            self.view.stock_report_view.column('Purchase Price',width=0,minwidth=0,stretch='no')
            self.view.stock_report_view.column('Selling Price',width=0,minwidth=0,stretch='no')
            self.view.stock_report_view.column('Department',width=0,minwidth=0,stretch='no')
        elif category_type == 'Sub-Category':
            records = self.model.fetch_sub_category()
            self.view.stock_report_view.heading('Name', text='Sub-Category Name')
            self.view.stock_report_view.column('Margin Amount',width=0,minwidth=0,stretch='no')
            self.view.stock_report_view.column('Margin %',width=0,minwidth=0,stretch='no')
            self.view.stock_report_view.column('Purchase Price',width=0,minwidth=0,stretch='no')
            self.view.stock_report_view.column('Selling Price',width=0,minwidth=0,stretch='no')
            self.view.stock_report_view.column('Department',width=0,minwidth=0,stretch='no')
        elif category_type == 'Department':
            records = self.model.fetch_departments()
            self.view.stock_report_view.heading('Name', text='Department Name')
            self.view.stock_report_view.column('Department',width=0,minwidth=0,stretch='no')
            self.view.stock_report_view.column('Margin Amount',width=0,minwidth=0,stretch='no')
            self.view.stock_report_view.column('Margin %',width=0,minwidth=0,stretch='no')
            self.view.stock_report_view.column('Purchase Price',width=0,minwidth=0,stretch='no')
            self.view.stock_report_view.column('Selling Price',width=0,minwidth=0,stretch='no')
        elif category_type == 'BatchWise':
            print("Batch Wisee")
            self.model.fetch_batchs()
        else:
            records = self.model.fetch_all()
            self.view.stock_report_view.heading('Name', text='Item Name')
            self.view.stock_report_view.column('Department',width=100,minwidth=20,stretch='yes')
            self.view.stock_report_view.column('Margin Amount',width=100,minwidth=20,stretch='yes')
            self.view.stock_report_view.column('Margin %',width=100,minwidth=20,stretch='yes')


        # self.stocks_update(records)

    def new_category_selected(self,event):
        pass

    def generate_report(self):
        records = self.model.fetch_all()
        # Workbook() takes one, non-optional, argument
        # which is the filename that we want to create.
        workbook = xlsxwriter.Workbook('Stock Report.xlsx')
        
        # The workbook object is then used to add new
        # worksheet via the add_worksheet() method.
        worksheet = workbook.add_worksheet()
        
        # Use the worksheet object to write
        # data via the write() method.
        worksheet.write('A1', 'item_id')
        worksheet.write('B1', 'Name')
        worksheet.write('C1', 'Barcode')
        worksheet.write('D1', 'HsnCode')
        worksheet.write('E1', 'Quantity')

        count = 1
        for record in records:
            count = count + 1
            worksheet.write('A{}'.format(count), record.get('item_id'))
            worksheet.write('B{}'.format(count), record.get('name'))
            worksheet.write('C{}'.format(count), record.get('barcode'))
            worksheet.write('D{}'.format(count), record.get('hsn_code'))
            worksheet.write('E{}'.format(count), record.get('quantity'))

        
        # Finally, close the Excel file
        # via the close() method.
        workbook.close()
