from src.controllers.base_controller import BaseController

class BrandsTypesController(BaseController):
    def __init__(self,BrandsTypesView,BrandsTypesModel):
        super(BrandsTypesController, self).__init__()
        self.brands_type_model = BrandsTypesModel()
        self.brands_type_view = BrandsTypesView(self)
        self.update_brand_types_treeview()


    def clear_input_boxes(self):
        self.brands_type_view.brands_type_input.delete(0, 'end')

    def select_item(self, e):
        self.clear_input_boxes()
        selected = self.brands_type_view.brands_type_list_view.focus()
        values = self.brands_type_view.brands_type_list_view.item(selected, 'values')
        self.brands_type_view.brands_type_input.insert(0, values[1])

    def enable_buttons(self):
        self.brands_type_view.brands_type_delete_btn.config(state='active')
        self.brands_type_view.brands_type_add_btn.config(state='active')
        self.brands_type_view.brands_type_edit_btn.grid(row=2, column=3)
        self.brands_type_view.brands_type_update_btn.grid_forget()
        

    def disable_buttons(self):
        self.brands_type_view.brands_type_delete_btn.config(state='disabled')
        self.brands_type_view.brands_type_add_btn.config(state='disabled')
        
    
    def update_brand_types_treeview(self):
        self.brands_type_view.brands_type_list_view.delete(*self.brands_type_view.brands_type_list_view.get_children())
        records = self.brands_type_model.fetch_all()
        count = 0
        for record in records:
            brand_type_id = record.get('id')
            brand_name = record.get('name')
            self.brands_type_view.brands_type_list_view.insert(parent='', index='end', iid=count, text='', values=(brand_type_id, brand_name))
            count += 1


    def add_item(self):
        brand_type = self.brands_type_view.brands_type_input.get()
        self.brands_type_model.insert_data(brand_type)
        self.clear_input_boxes()
        self.update_brand_types_treeview()

    def edit_item(self):
        self.disable_buttons()
        self.brands_type_view.brands_type_edit_btn.grid_forget()
        self.brands_type_view.brands_type_update_btn.grid(row=2, column=3)

    def update_item(self):
        brand_type_name = self.brands_type_view.brands_type_input.get()
        selected = self.brands_type_view.brands_type_list_view.focus()
        brand_type_id = self.brands_type_view.brands_type_list_view.item(selected, 'values')[0]
        self.brands_type_model.update_data(brand_type_name,brand_type_id)
        self.brands_type_view.brands_type_list_view.item(selected, text="", values=(brand_type_id, brand_type_name))
        self.enable_buttons()
        self.clear_input_boxes()    

    def delete_item(self):
        try:
            selected = self.brands_type_view.brands_type_list_view.focus()
            brand_type_id = self.brands_type_view.brands_type_list_view.item(selected, 'values')[0]
            self.brands_type_model.delete_data(str(brand_type_id))
            self.brands_type_view.brands_type_list_view.delete(selected)
            self.clear_input_boxes()
        except IndexError:
            print('nothing selected')
        finally:
            print('Thankyou')



