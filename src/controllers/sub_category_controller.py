from src.helpers import get_id_from_string
from src.controllers.base_controller import BaseController


class SubCategoryController(BaseController):
    def __init__(self, SubCategoryView, SubCategoryModel):
        super(SubCategoryController, self).__init__()
        self.model = SubCategoryModel()
        self.view = SubCategoryView(self)
        self.set_items()

    def set_items(self):
        self.category_options = self.model.get_category()
        self.view.category_list['values'] = [sub_category.get('category_name') for sub_category in self.category_options]
        self.update_sub_category_treeview()

    def clear_input_boxes(self):
        self.view.sub_category_input.delete(0, 'end')

    def select_item(self, e):
        self.clear_input_boxes()
        selected = self.view.sub_category_list_view.focus()
        values = self.view.sub_category_list_view.item(selected, 'values')
        self.view.sub_category_input.insert(0, values[1])

    def enable_buttons(self):
        self.view.sub_category_delete_btn.config(state='active')
        self.view.sub_category_add_btn.config(state='active')

    def disable_buttons(self):
        self.view.sub_category_delete_btn.config(state='disabled')
        self.view.sub_category_add_btn.config(state='disabled')

    def update_sub_category_treeview(self):
        self.view.sub_category_list_view.delete(*self.view.sub_category_list_view.get_children())

        records = self.model.fetch_all()
        count = 0
        for record in records:
            sub_categoy_id = record.get('sub_category_id')
            sub_category_name = record.get('sub_category_name')
            category_name = record.get('category_name')
            department_name = record.get('department_name')

            self.view.sub_category_list_view.insert(parent='', index='end', iid=count, text='',
                values=(sub_categoy_id, sub_category_name, category_name, department_name)
                    )
            count += 1

    def select_category(self,event):
        index = self.view.category_list.current()
        category_id = self.category_options[index].get('category_id')
        category_name = self.category_options[index].get('category_name')
        self.view.sub_category_id.set(category_id)
        self.view.sub_category_name.set(category_name)


    def add_item(self):
        sub_category_name = self.view.sub_category_input.get()
        category_id = self.view.sub_category_id.get()
        self.model.insert_data(sub_category_name, str(category_id))

        self.clear_input_boxes()
        self.update_sub_category_treeview()

    def delete_category(self):
        try:
            selected = self.view.sub_category_list_view.focus()
            sub_category_id = self.view.sub_category_list_view.item(selected, 'values')[0]
            self.model.delete_data(str(sub_category_id))
            self.view.sub_category_list_view.delete(selected)
            self.clear_input_boxes()
        except IndexError:
            print('Nothing Selected')
        finally:
            print('congrats')

    def edit_item(self):
        self.disable_buttons()
        self.view.sub_category_edit_btn.grid_forget()
        self.view.sub_category_update_btn.grid(row=3, column=3)

    def update_item(self):
        sub_category_name = self.view.sub_category_input.get()
        selected = self.view.sub_category_list_view.focus()
        sub_category_id = self.view.sub_category_list_view.item(selected, 'values')[0]
        category_id = get_id_from_string(self.view.category_list.get())
        self.model.update_data(sub_category_name, category_id, sub_category_id)
        self.enable_buttons()
        self.update_sub_category_treeview()
        self.view.sub_category_edit_btn.grid(row=3, column=3)
        self.view.sub_category_update_btn.grid_forget()
