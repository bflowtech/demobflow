from src.controllers.base_controller import BaseController


class LiablityController(BaseController):
    def __init__(self,LiablityView,LiablityModel):
        super(LiablityController, self).__init__()
        self.liablity_model = LiablityModel()
        self.liablity_view = LiablityView(self)
        self.update_tree_view()


    def clear_input_boxes(self):
        self.liablity_view.liablity_group_input.delete(0, 'end')

    def select_item(self, e):
        self.clear_input_boxes()
        selected = self.liablity_view.liablity_group_view.focus()
        self.group_id,self.group_name = self.liablity_view.liablity_group_view.item(selected, 'values')
        liablity_id = self.group_id
        self.liablity_view.liablity_group_input.insert(0, self.group_name)
        

    def enable_buttons(self):
        self.liablity_view.liablity_group_add.config(state='active')
        self.liablity_view.liablity_group_delete_btn.config(state='active')
        self.liablity_view.liablity_group_edit_btn.grid(row=4, column=2)
        self.liablity_view.liablity_group_update_btn.grid_forget()

    def disable_buttons(self):
        self.liablity_view.liablity_group_add.config(state='disabled')
        self.liablity_view.liablity_group_delete_btn.config(state='disabled')
        


    def add_item(self):
        liablity_group_name = self.liablity_view.liablity_group_input.get()
        self.liablity_model.insert_data(liablity_group_name)
        self.clear_input_boxes()
        self.update_tree_view()

    
    def edit_item(self):
        self.disable_buttons()
        self.liablity_view.liablity_group_edit_btn.grid_forget()
        self.liablity_view.liablity_group_update_btn.grid(row=4, column=2)
    

    def update_item(self):
        liablity_group_name = self.liablity_view.liablity_group_input.get()
        selected = self.liablity_view.liablity_group_view.focus()
        liablity_group_id = self.liablity_view.liablity_group_view.item(selected, 'values')[0]
        self.liablity_model.update_data(liablity_group_name,liablity_group_id)
        self.liablity_view.liablity_group_view.item(selected, text="", values=(liablity_group_id, liablity_group_name))
        self.enable_buttons()
        self.clear_input_boxes()


    def update_tree_view(self):
        records = self.liablity_model.fetch_all()
        self.liablity_view.liablity_group_view.delete(*self.liablity_view.liablity_group_view.get_children())
        count = 0
        for record in records:
            liablity_group_id = record.get('id')
            liablity_group_name = record.get('name')
            self.liablity_view.liablity_group_view.insert(parent='', index='end', iid=count, text='',
            values=(liablity_group_id,liablity_group_name))
            count += 1

    def delete_item(self):
        try:
            selected = self.liablity_view.liablity_group_view.focus()
            liablity_id = self.liablity_view.liablity_group_view.item(selected, 'values')[0]
            self.liablity_model.delete_data(str(liablity_id))
            self.liablity_view.liablity_group_view.delete(selected)
            self.clear_input_boxes()
        except:
            print('select a value')
        finally:
            print('Congrats')

    def gst_onchange(self,*args):
        gst_type = self.liablity_view.gst_clicked.get()
        if 'Unregistered' == gst_type:
            self.liablity_view.gst_no_input.configure(state='disabled')
        elif 'Customer' == gst_type:
            self.liablity_view.gst_no_input.configure(state='disabled')
        else:
            self.liablity_view.gst_no_input.configure(state='normal')


    def tax_onchange(self,*args):
        tax_type = self.liablity_view.tax_options.get()
        print(tax_type)


    def add_to_ledger(self):
        ledger_name = self.liablity_view.ledger_name_input.get()
        ledger_type = self.liablity_view.type_input.get()
        phone_number = self.liablity_view.phone_number_input.get()
        opening_balance = self.liablity_view.opening_balance_input.get()

        try:
            contact_details = self.liablity_view.contact_details_input.get()
        except AttributeError:
            contact_details = 'NULL'

        try:
            address_details = self.liablity_view.address_details_input.get()
        except AttributeError:
            address_details = 'NULL'

        try:
            gst_type = self.liablity_view.gst_clicked.get()
        except AttributeError:
            gst_type = 'NULL'

        try:
            gst_no = self.liablity_view.gst_no_input.get()
        except AttributeError:
            gst_no = 'NULL'

        try:
            percentage_of_tax = self.liablity_view.percentage_tax_input.get()
        except AttributeError:
            percentage_of_tax = 'NULL'

        try:
            duties_type = self.liablity_view.duties_options.get()
        except AttributeError:
            duties_type = 'NULL'

        try:
            tax_type = self.liablity_view.tax_options.get()
        except:
            tax_type = 'NULL'

        try:
            credit_period = self.liablity_view.credit_period_input.get()
        except AttributeError:
            credit_period = 'NULL'
        try:
            pancard_no = self.liablity_view.pancard_no_input.get()
        except AttributeError:
            pancard_no = 'NULL'


        # bank
        try:
            bank_name = self.liablity_view.bank_input.get()
            bank_address = self.liablity_view.bank_address_input.get()
            account_no = self.liablity_view.account_no_input.get()
            ifsc_code = self.liablity_view.ifsc_code_input.get()
            branch_no = self.liablity_view.branch_no_input.get()
            branch_code = self.liablity_view.branch_code_input.get()
            bank_contact = self.liablity_view.bank_contact_input.get()
        except AttributeError:
            bank_name = 'NULL'
            bank_address = 'NULL'
            account_no = 'NULL'
            ifsc_code = 'NULL'
            branch_no = 'NULL'
            branch_code = 'NULL'
            bank_contact = 'NULL'
        finally:
            account_group_id = self.group_id


        self.liablity_model.insert_into_ledger(ledger_name,ledger_type,contact_details,address_details,
                                                phone_number,gst_type,gst_no,opening_balance,account_group_id,
                                                tax_type,percentage_of_tax,
                                                bank_name,bank_address,account_no,ifsc_code,branch_no,
                                                branch_code,bank_contact,credit_period,pancard_no,duties_type)
