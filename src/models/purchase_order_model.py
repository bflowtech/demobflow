from src.models.base_model import BaseModel

class PurchaseOrderModel(BaseModel):
    def __init__(self):
        super(PurchaseOrderModel, self).__init__()
        self._create_table()

    def _create_table(self):
        self.mycursor.execute("""CREATE TABLE IF NOT EXISTS PURCHASE_ORDERS (id int not null auto_increment,date DATE,party_account_details varchar(200),
                                reference_no varchar(200),order_no varchar(200),primary key(id)
                               )""")

        self.mycursor.execute("""CREATE TABLE IF NOT EXISTS PURCHASE_ORDERS_LIST (id int not null auto_increment,name varchar(200),barcode varchar(200),batch_no varchar(200),
                                hsn_code varchar(200),quantity int,rate float,total_amount float,purchase_order_id int,item_id int,
                                CONSTRAINT purchase_order_id FOREIGN KEY (purchase_order_id) REFERENCES PURCHASE_ORDERS (id),
                                primary key(id) 
                            )""")

    def fetch_all(self):
        pass

    def insert_data(self,party_account_type,reference_no,order_no):
        self.mycursor.execute("INSERT INTO PURCHASE_ORDERS(party_account_details,reference_no,order_no,date) VALUES ('"+party_account_type+"','"+reference_no+"','"+order_no+"',curdate()) ")
        self.save_changes()
        return self.mycursor.lastrowid
        
    def insert_items_data(self,items_list,purchase_order_id):
        for item in items_list:
            barcode = item.get('barcode')
            batch_no = item.get('batch_no')
            hsn_no = item.get('hsn_no')
            quantity = item.get('quantity')
            rate = item.get('rate')
            total_amount = item.get('total_amount')
            item_id = str(item.get('item').get('id'))

            self.mycursor.execute("INSERT INTO PURCHASE_ORDERS_LIST (item_id,barcode,batch_no,hsn_code,quantity,rate,total_amount,purchase_order_id) VALUES ('"+item_id+"','"+barcode+"','"+batch_no+"','"+hsn_no+"','"+quantity+"','"+rate+"','"+total_amount+"','"+str(purchase_order_id)+"') ")
        self.save_changes()

    def update_data(self):
        pass

    def delete_data(self):
        pass

    
    def get_items(self):
        self.mycursor.execute("SELECT id AS item_id ,name AS item_name FROM item_master")
        return self.mycursor.fetchall()

    def get_item_details(self,item_id):
        self.mycursor.execute("SELECT barcode,batch_no,hsn_code,selling_price FROM item_master where id = '"+item_id+"' ")
        return self.mycursor.fetchone()


    def get_party_accounts(self):
        """Fetch all Ledgers Which Comes Under Sundry Credior Group"""
        self.mycursor.execute("select ledger.id AS ledger_id,ledger.name AS ledger_name from ledger INNER JOIN account_groups on ledger.account_group_id = account_groups.id where account_groups.name = 'Sundry Creditors'")
        return self.mycursor.fetchall()
