from src.models.base_model import BaseModel

class GstMasterModel(BaseModel):
    def __init__(self):
        super(GstMasterModel, self).__init__()
        self._create_table()


    def _create_table(self):
        self.mycursor.execute(
                "CREATE TABLE IF NOT EXISTS GST_MASTER (id INT NOT NULL AUTO_INCREMENT,name varchar(200),percentage int,primary key(id) )")

    def fetch_all(self):
        self.mycursor.execute('SELECT id AS gst_id,name AS gst_name,percentage FROM GST_MASTER')
        records = self.mycursor.fetchall()
        return records

    def insert_data(self,gst_name,gst_percentage):
        self.mycursor.execute("INSERT INTO GST_MASTER (name,percentage) VALUES ('"+gst_name+"','"+gst_percentage+"')")
        self.save_changes()

    def update_data(self,gst_name,percentage,gst_id):
        self.mycursor.execute(
            "UPDATE GST_MASTER SET name = '" + gst_name + "', percentage='"+percentage+"' WHERE id ='" + gst_id + "'")
        self.save_changes()

    def delete_data(self,gst_id):
        self.mycursor.execute("DELETE FROM GST_MASTER WHERE ID = '"+gst_id+"'")
        self.save_changes()

