from src.models.base_model import BaseModel


class SalesVoucherModel(BaseModel):
    def __init__(self):
        super(SalesVoucherModel, self).__init__()
        self._create_table()

    def _create_table(self):
        self.mycursor.execute(                              
                            """
                            CREATE TABLE IF NOT EXISTS SALES_VOUCHER (id int not null auto_increment,date DATE,
                            gst_type varchar(200),sales_voucher_no varchar(200),party_account_details varchar(200),
                            primary key(id))
                            """
                            )

        self.mycursor.execute(
                            """
                            CREATE TABLE IF NOT EXISTS SALES_VOUCHER_LIST (id int not null auto_increment,barcode varchar(200),batch_no varchar(200),
                            hsn_code varchar(200),quantity int,rate float,discount int,taxable_amount float,cgst int,sgst int,igst int,cess int,total_amount float,
                            sales_voucher_id int,ledger_id int,item_id int,
                            CONSTRAINT sales_item_master FOREIGN KEY (item_id) REFERENCES ITEM_MASTER (id),
                            CONSTRAINT sales_ledger_id FOREIGN KEY (ledger_id) REFERENCES LEDGER (id),
                            CONSTRAINT sales_voucher_id FOREIGN KEY (sales_voucher_id) REFERENCES SALES_VOUCHER (id),
                            primary key(id))
                            """
                            )

    def fetch_all(self):
        pass
    
    def insert_data(self,sale_voucher,party_account_type,gst_type,party_account_date):
        self.mycursor.execute("INSERT INTO SALES_VOUCHER (sales_voucher_no,party_account_details,gst_type,date) VALUES ('"+sale_voucher+"','"+party_account_type+"','"+gst_type+"',str_to_date('"+party_account_date+"', '%d-%m-%Y') )") 
        self.save_changes()
        return self.mycursor.lastrowid

    def insert_items_data(self,items_list,sale_voucher_id):
        for item in items_list:
            barcode = item.get('barcode')
            batch_no = item.get('batch_no')
            item_name = item.get('item').get('name')
            item_id = str(item.get('item').get('id'))
            hsn_no = item.get('hsn_no')
            quantity = item.get('quantity')
            rate = item.get('rate')
            discount = item.get('discount')
            taxable_amount = item.get('taxable_amount')
            cgst = item.get('cgst')
            sgst = item.get('sgst')
            igst = item.get('igst')
            cess_id = str(item.get('cess').get('id'))
            ledger_id = str(item.get('ledger').get('id'))
            total_amount = item.get('total_amount')
            self.mycursor.execute("INSERT INTO SALES_VOUCHER_LIST(barcode,batch_no,hsn_code,quantity,rate,discount,taxable_amount,cgst,sgst,igst,cess,total_amount,sales_voucher_id,ledger_id,item_id) VALUES ('"+barcode+"','"+batch_no+"','"+hsn_no+"','"+quantity+"','"+rate+"','"+discount+"','"+taxable_amount+"','"+cgst+"','"+sgst+"','"+igst+"','"+cess_id+"','"+total_amount+"','"+sale_voucher_id+"','"+ledger_id+"','"+item_id+"') ")
        self.save_changes()

    def update_data(self):
        pass

    def delete_data(self):
        pass
    

    def get_items(self,item_name):
        self.mycursor.execute("SELECT id AS item_id,name AS item_name FROM item_master where name LIKE '"+item_name+"%'")
        return self.mycursor.fetchall()

    def get_batches(self,item_name):
        self.mycursor.execute("""
                            SELECT batch_no,item_master.id AS item_id,item_master.name AS item_name,expiry_date
                            FROM purchase_voucher_list INNER JOIN ITEM_MASTER ON purchase_voucher_list.item_id = item_master.id 
                            where item_master.name = '{}'
                            GROUP BY batch_no
                            """.format(item_name)
                            )
        
        return self.mycursor.fetchall()
    
    def get_party_accounts(self):
        """Fetch all Ledgers Which Comes Under Sundry Credior Group"""
        self.mycursor.execute("select ledger.id AS ledger_id,ledger.name AS ledger_name from ledger INNER JOIN account_groups on ledger.account_group_id = account_groups.id where account_groups.name = 'Sundry Creditors'")
        return self.mycursor.fetchall()


    def get_sales_ledgers(self):
        self.mycursor.execute(
                            """
                            SELECT id AS ledger_id,name AS ledger_name from ledger where account_group_id = (select id from account_groups where name = 'SALES' )
                            """
                            )

        return self.mycursor.fetchall()

    def get_item_details(self,batch_no):
        self.mycursor.execute(
                            """
                            SELECT barcode,hsn_code,rate,manufacturing_date,expiry_date FROM purchase_voucher_list where batch_no = '{}' 
                            """.format(batch_no)
                            )
        return self.mycursor.fetchone()

    def get_sales_transactions(self):
        self.mycursor.execute("""SELECT sales_voucher.date,sales_voucher.gst_type,item_master.name AS item_name,ledger.name AS ledger_name,
                                sales_voucher_list.quantity,sales_voucher_list.total_amount
                                from sales_voucher_list 
                                LEFT JOIN item_master on item_id = item_master.id
                                LEFT JOIN sales_voucher on sales_voucher_id = sales_voucher.id
                                LEFT JOIN ledger on ledger_id = ledger.id
                             """)
        records = self.mycursor.fetchall()
        return records


    def get_cess_details(self):
        self.mycursor.execute("SELECT id AS cess_id,name AS cess_name FROM cess_master")
        return self.mycursor.fetchall()

    def get_cess_percentage(self,cess_id):
        self.mycursor.execute(
                            """
                            SELECT percentage AS cess_percentage FROM cess_master where id = '{}'
                            """.format(cess_id)
                            )
                            
        return self.mycursor.fetchone().get('cess_percentage')

    def get_supplier_gst_number(self,ledger_id):
        self.mycursor.execute(
                            """
                            SELECT gst_no from ledger where id = '{}'
                            """.format(ledger_id)
                            )
        records = self.mycursor.fetchone()
        return records.get('gst_no')
        
    def get_company_gst_number(self):
        self.mycursor.execute(
                            """
                                SELECT gst_no AS company_gst_no from company_master
                            """
                            )

        records = self.mycursor.fetchone()
        return records.get('company_gst_no')

    def get_ledger_gst_percentage(self,ledger_id):
        print(ledger_id,'heyyy')
        self.mycursor.execute(
                            """
                                SELECT gst_percentage from ledger where id='{}'
                            """.format(ledger_id)
                            )
        records = self.mycursor.fetchone()
        return records.get('gst_percentage')