import re
from src.models.base_model import BaseModel

class CategoryModel(BaseModel):
    def __init__(self):
        super(CategoryModel, self).__init__()
        self._create_table()

    def _create_table(self):
        self.mycursor.execute("CREATE TABLE IF NOT EXISTS CATEGORY (id INT NOT NULL AUTO_INCREMENT,name varchar(200),department_id int,primary key(id),CONSTRAINT department_id FOREIGN KEY(department_id) REFERENCES DEPARTMENTS(id) )")

    def fetch_all(self):
        self.mycursor.execute(
            'SELECT category.id AS category_id ,category.name AS category_name,departments.name AS department_name FROM CATEGORY INNER JOIN DEPARTMENTS ON department_id = departments.id')
        records = self.mycursor.fetchall()
        return records

    def insert_data(self,category_name,department_id):
        self.mycursor.execute("insert into category (name,department_id) values ('" + category_name + "','"+department_id+"')")
        self.save_changes()

    def update_data(self,category_name,category_id):
        self.mycursor.execute("UPDATE CATEGORY SET name = '" + category_name + "' WHERE id ='" + category_id + "' ")
        self.save_changes()

    def delete_data(self,category_id):
        self.mycursor.execute("DELETE FROM CATEGORY WHERE id = '" +category_id+ "' ")
        self.save_changes()


    def get_departments(self):
        self.mycursor.execute("""
                            SELECT id as department_id,name as department_name from departments
                            """)
        return self.mycursor.fetchall()

    def search_category(self,category_name):
        self.mycursor.execute("SELECT category.id,category.name,departments.name FROM CATEGORY INNER JOIN DEPARTMENTS ON category.department_id = departments.id where category.name LIKE '"+category_name+"%'")
        records = self.mycursor.fetchall()
        return records
