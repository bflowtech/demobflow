import re
from src.models.base_model import BaseModel
from itertools import groupby, zip_longest
import json
import decimal

class DecimalEncoder(json.JSONEncoder):
    def default(self, o):
        if isinstance(o, decimal.Decimal):
            return str(o)
        return super(DecimalEncoder, self).default(o)



class StocksReportModel(BaseModel):
    def __init__(self):
        super(StocksReportModel, self).__init__()
        self._create_table()

    def _create_table(self):
        pass
    
    def fetch_all(self):
        self.mycursor.execute("""SELECT item_master.id AS item_id,item_master.name AS item_name,SUM(item_master.opening_qty) AS opening_qty,
                                departments.name AS department_name,margin,cost_price,brands.name AS brand_name
                                from ITEM_MASTER
                                LEFT JOIN departments ON item_master.department_id = departments.id
                                LEFT JOIN brands on item_master.brand_id = brands.id
                                LEFT JOIN gst_master on item_master.gst_id = gst_master.id
                                LEFT JOIN cess_master on item_master.cess_id = cess_master.id
                                GROUP BY ITEM_MASTER.id
                            """)

        records = self.mycursor.fetchall()
        purchase_datas = self.get_purchase_voucher()
        sales_datas = self.get_sales_voucher()
        total_transactions = purchase_datas + sales_datas + records

        items_datas = []
        records = self.get_total_transactions(total_transactions)

        print(json.dumps(records,cls=DecimalEncoder))
        
        print('ItemWise')
        for name,value in records.items():
            item_name = name
            inward_qty = 0 if value.get('inward_qty') == None else value.get('inward_qty')
            outward_qty = 0 if value.get('outward_qty') == None else value.get('outward_qty')
            item_id = value.get('item_id')
            opening_qty = value.get('opening_qty')
            department_name = value.get('department_name')
            margin_amount = value.get('margin')
            cost_price = value.get('cost_price')
            brand_name = value.get('brand_name')
            purchase_rate = 0 if value.get('purchase_rate') == None else value.get('purchase_rate')
            sales_rate = value.get('sales_rate')



            closing_stock = (opening_qty+inward_qty)-outward_qty
            closing_value = int(closing_stock) * float(cost_price)
            
            print(item_name,closing_stock,cost_price,closing_value)
            margin_percentage = str(round(margin_amount / cost_price * 100, 2))+"%"
    
            # closing_value = float(purchase_rate) * (int(opening_qty)+int(inward_qty)-int(outward_qty))

            details = {
                    'item_id':item_id,'name':item_name,'opening_qty':opening_qty,'purchase_rate':cost_price,'selling_price':sales_rate,
                    'inward_qty':inward_qty,'outward_qty':outward_qty,'margin_amount':margin_amount,'margin_percentage':margin_percentage,
                    'closing_value':closing_value,'department':department_name,'brand':brand_name,'closing_stock':closing_stock,
                    }

            items_datas.append(details)
  
        return items_datas

    def get_purchase_voucher(self):
        self.mycursor.execute("""
                            SELECT item_master.name AS item_name,quantity AS inward_qty,ROUND(rate) AS purchase_rate,
                            departments.name AS department_name,brands.name As brand_name,category.name AS category_name,
                            sub_category.name AS sub_category,purchase_voucher_list.batch_no AS batch_no
                            FROM purchase_voucher_list
                            INNER JOIN ITEM_MASTER ON purchase_voucher_list.item_id = item_master.id
                            INNER JOIN departments on item_master.department_id = departments.id
                            INNER JOIN brands on item_master.brand_id = brands.id
                            INNER JOIN category on item_master.category_id = category.id
                            INNER JOIN sub_category on item_master.sub_category_id = sub_category.id
                            GROUP BY item_master.id
                            """)
        records = self.mycursor.fetchall()
        return records

    def get_sales_voucher(self):
        self.mycursor.execute("""
                            SELECT item_master.name AS item_name,quantity AS outward_qty,ROUND(rate) AS sales_rate,
                            departments.name AS department_name,brands.name AS brand_name,category.name AS category_name,
                            sub_category.name AS sub_category,sales_voucher_list.batch_no AS batch_no
                            FROM sales_voucher_list
                            INNER JOIN ITEM_MASTER ON sales_voucher_list.item_id = item_master.id
                            INNER JOIN departments on item_master.department_id = departments.id
                            INNER JOIN brands on item_master.brand_id = brands.id
                            INNER JOIN category on item_master.category_id = category.id
                            INNER JOIN sub_category on item_master.sub_category_id = sub_category.id
                            GROUP BY item_master.id
                            """)
        records = self.mycursor.fetchall()
        return records

    def get_total_transactions(self,mixed_transactions):
        total_transactions = {}
        for count,transaction in enumerate(mixed_transactions):
            item_name = transaction.get('item_name')
            if 'inward_qty' in transaction.keys():
                inward_qty = transaction.get('inward_qty')
                purchase_rate = transaction.get('purchase_rate')
                if total_transactions.get(item_name) == None:
                    total_transactions[item_name] = {'inward_qty':inward_qty,'purchase_rate':purchase_rate}
                elif 'inward_qty' in total_transactions.get(item_name):
                    old_inward_qty = total_transactions.get(item_name).get('inward_qty')
                    total_transactions.get(item_name)['inward_qty'] = old_inward_qty + transaction.get('inward_qty')
                else:
                    total_transactions[item_name].update({'inward_qty':inward_qty,'purchase_rate':purchase_rate})
            elif 'outward_qty' in transaction.keys():
                outward_qty = transaction.get('outward_qty')
                sales_rate = transaction.get('sales_rate')
                if total_transactions.get(item_name) == None:
                    total_transactions[item_name] = {'outward_qty':outward_qty,'sales_rate':sales_rate}
                elif 'outward_qty' in total_transactions.get(item_name):
                    old_outward_qty = total_transactions.get(item_name).get('outward_qty')
                    total_transactions.get(item_name)['outward_qty'] = old_outward_qty + transaction.get('outward_qty')
                else:
                    total_transactions[item_name].update({'outward_qty':outward_qty,'sales_rate':sales_rate})
            else:
                item_id = transaction.get('item_id')
                opening_qty = transaction.get('opening_qty')
                department_name = transaction.get('department_name')
                margin = transaction.get('margin')
                cost_price = transaction.get('cost_price')
                brand_name = transaction.get('brand_name')
                
                if item_name not in total_transactions.keys():
                    total_transactions[item_name] = {'item_id': item_id,'opening_qty': opening_qty, 'outward_qty':0,'inward_qty':0,'department_name': department_name, 'margin': margin, 'cost_price': cost_price, 'brand_name': brand_name}
                else:
                    total_transactions[item_name].update({'item_id': item_id,'opening_qty': opening_qty, 'department_name': department_name, 'margin': margin, 'cost_price': cost_price, 'brand_name': brand_name})
        
        return total_transactions

    def fetch_departments(self):
        self.mycursor.execute("""SELECT item_master.id AS item_id,item_master.name AS item_name,SUM(item_master.opening_qty) AS opening_qty,
                                departments.name AS department_name,margin,cost_price,brands.name AS brand_name
                                from ITEM_MASTER
                                LEFT JOIN departments ON item_master.department_id = departments.id
                                LEFT JOIN brands on item_master.brand_id = brands.id
                                LEFT JOIN gst_master on item_master.gst_id = gst_master.id
                                LEFT JOIN cess_master on item_master.cess_id = cess_master.id
                                GROUP BY ITEM_MASTER.name""")

        records = self.mycursor.fetchall()
        purchase_datas = self.get_purchase_voucher()
        sales_datas = self.get_sales_voucher()

        total_transactions = purchase_datas + sales_datas + records
        department_wise_record = self.report_group_by(total_transactions)

        department_datas = {}
        print('Department Vise')
        for department_name,item_details in department_wise_record.items():
            for item_name,details in item_details.items():
                if department_datas.get(department_name) == None:

                    opening_qty = details.get('opening_qty')
                    inward_qty = details.get('inward_qty')
                    purchase_rate = details.get('cost_price')
                    outward_qty =  0 if details.get('outward_qty') == None else details.get('outward_qty')
                    sales_rate = 0 if details.get('sales_rate') == None else details.get('sales_rate')
                    
                    closing_stock = (opening_qty + inward_qty) - outward_qty
                    closing_value = int(closing_stock) * float(purchase_rate)
                    
                    print(item_name,closing_stock,purchase_rate,closing_value)

                    department_datas[department_name] = {
                        'opening_qty':opening_qty,
                        'inward_qty':inward_qty,
                        'purchase_rate':purchase_rate,
                        'outward_qty':outward_qty,
                        'sales_rate':sales_rate,
                        'closing_stock':closing_stock,
                        'closing_value':closing_value
                        }

                else:
                    old_opening_qty = department_datas.get(department_name).get('opening_qty')
                    old_inward_qty = department_datas.get(department_name).get('inward_qty')
                    old_purchase_rate = department_datas.get(department_name).get('purchase_rate')                    
                    old_outward_qty = department_datas.get(department_name).get('outward_qty')
                    old_sales_rate = department_datas.get(department_name).get('sales_rate')
                    old_closing_stock = department_datas.get(department_name).get('closing_stock')

                    old_closing_value = department_datas.get(department_name).get('closing_value')

                    

                    opening_qty = old_opening_qty + (0 if details.get('opening_qty')  == None else details.get('opening_qty'))
                    inward_qty = old_inward_qty + (0 if details.get('inward_qty')  == None else details.get('inward_qty'))
                    purchase_rate = old_purchase_rate + (0 if details.get('cost_price') == None else details.get('cost_price'))
                    outward_qty =  old_outward_qty + (0 if details.get('outward_qty') == None else details.get('outward_qty'))
                    sales_rate = old_sales_rate + (0 if details.get('sales_rate') == None else details.get('sales_rate'))

                    closing_stock = old_closing_stock + details.get('opening_qty') + details.get('inward_qty') + (0 if details.get('outward_qty') == None else details.get('outward_qty'))

                    closing_value = old_closing_value + int(details.get('opening_qty') + details.get('inward_qty') + (0 if details.get('outward_qty') == None else details.get('outward_qty'))) * float(details.get('cost_price'))

                    department_datas[department_name].update({
                        'opening_qty':opening_qty,
                        'inward_qty':inward_qty,
                        'purchase_rate':purchase_rate,
                        'outward_qty':outward_qty,
                        'sales_rate':sales_rate,
                        'closing_stock':closing_stock,
                        'closing_value':closing_value
                        })


        department_records = []
        for department_name,details in department_datas.items():
            department_records.append({
                'name':department_name,
                'opening_qty':details.get('opening_qty'),
                'selling_price':details.get('sales_rate'),
                'outward_qty':details.get('outward_qty'),
                'purchase_rate':details.get('purchase_rate'),
                'inward_qty':details.get('inward_qty'),
                'closing_stock':details.get('closing_stock'),
                'closing_value':details.get('closing_value')
            })
        return department_records

    def report_group_by(self,MIXED_TRANSACTIONS):
        grouped_transaction = {}
        for count,transaction in enumerate(MIXED_TRANSACTIONS):
            department_name = transaction.get('department_name')
            item_name = transaction.get('item_name')

            if 'inward_qty' in transaction.keys():
                inward_qty = transaction.get('inward_qty')
                purchase_rate = transaction.get('purchase_rate')

                if grouped_transaction.get(department_name) == None:
                    grouped_transaction[department_name] = {item_name:{'inward_qty':inward_qty,'purchase_rate':purchase_rate}}
                elif grouped_transaction.get(department_name).get(item_name) == None:
                    grouped_transaction[department_name].update({item_name:{'inward_qty':inward_qty,'purchase_rate':purchase_rate}})
                else:
                    old_inward_qty = grouped_transaction.get(department_name).get(item_name).get('inward_qty')
                    inward_qty = old_inward_qty + transaction.get('inward_qty')
                    grouped_transaction[department_name][item_name].update({'inward_qty':inward_qty,'purchase_rate':purchase_rate})

            elif 'outward_qty' in transaction.keys():
                outward_qty = transaction.get('outward_qty')
                sales_rate = transaction.get('sales_rate')

                if grouped_transaction.get(department_name) == None:
                    grouped_transaction[department_name] = {item_name:{'outward_qty':outward_qty,'sales_rate':sales_rate}}
                elif grouped_transaction.get(department_name).get(item_name).get('outward_qty') == None:
                    grouped_transaction[department_name][item_name].update({'outward_qty':outward_qty,'sales_rate':sales_rate})
                else:
                    old_outward_qty = grouped_transaction.get(department_name).get(item_name).get('outward_qty')
                    outward_qty = old_outward_qty + transaction.get('outward_qty')
                    grouped_transaction[department_name][item_name].update({'outward_qty':outward_qty,'sales_rate':sales_rate})
            else:
                item_id = transaction.get('item_id')
                opening_qty = transaction.get('opening_qty')
                department_name = transaction.get('department_name')
                margin = transaction.get('margin')
                cost_price = transaction.get('cost_price')
                brand_name = transaction.get('brand_name')

                if not department_name in grouped_transaction.keys():
                    grouped_transaction[department_name] = {item_name:{'item_id': item_id,'opening_qty': opening_qty,'outward_qty':0,'inward_qty':0,'margin': margin, 'cost_price': cost_price, 'brand_name': brand_name}}
                elif grouped_transaction.get(department_name).get(item_name) == None:
                    grouped_transaction[department_name].update({item_name:{'item_id': item_id,'opening_qty': opening_qty,'outward_qty':0,'inward_qty':0,'margin': margin, 'cost_price': cost_price, 'brand_name': brand_name}})
                else:
                    grouped_transaction[department_name][item_name].update({'item_id': item_id,'opening_qty': opening_qty,'margin': margin, 'cost_price': cost_price, 'brand_name': brand_name})
        
        return grouped_transaction

    def fetch_brands(self):
        self.mycursor.execute("""SELECT item_master.id AS item_id,item_master.name AS item_name,SUM(item_master.opening_qty) AS opening_qty,
                                departments.name AS department_name,margin,cost_price,brands.name AS brand_name
                                from ITEM_MASTER
                                LEFT JOIN departments ON item_master.department_id = departments.id
                                LEFT JOIN brands on item_master.brand_id = brands.id
                                LEFT JOIN gst_master on item_master.gst_id = gst_master.id
                                LEFT JOIN cess_master on item_master.cess_id = cess_master.id
                                GROUP BY ITEM_MASTER.id
                            """)

        records = self.mycursor.fetchall()
        purchase_datas = self.get_purchase_voucher()
        sales_datas = self.get_sales_voucher()
        total_transactions = purchase_datas + sales_datas + records

        brand_wise_record = self.__groupby_brands(total_transactions)
        
        brand_datas = {}
        for brand_name,item_details in brand_wise_record.items():
            for item_name,details in item_details.items():
                if brand_datas.get(brand_name) == None:
                    opening_qty = details.get('opening_qty')
                    inward_qty = 0 if details.get('inward_qty') == None else details.get('inward_qty')
                    purchase_rate = details.get('cost_price')
                    outward_qty = 0 if details.get('outward_qty') == None else details.get('outward_qty')
                    sales_rate = details.get('sales_rate')
                    closing_stock = (opening_qty + inward_qty) - outward_qty
                    closing_value = int(closing_stock) * float(purchase_rate)

                    brand_datas[brand_name] = {
                        'opening_qty':opening_qty,
                        'inward_qty':inward_qty,
                        'purchase_rate':purchase_rate,
                        'outward_qty':outward_qty,
                        'sales_rate':sales_rate,
                        'closing_stock':closing_stock,
                        'closing_value':closing_value
                        }

                else:
                    old_opening_qty = 0 if brand_datas.get(brand_name).get('opening_qty') == None else brand_datas.get(brand_name).get('opening_qty')
                    old_inward_qty = 0 if brand_datas.get(brand_name).get('inward_qty') == None else brand_datas.get(brand_name).get('inward_qty')
                    old_purchase_rate = 0 if brand_datas.get(brand_name).get('purchase_rate') == None else brand_datas.get(brand_name).get('purchase_rate')
                    old_outward_qty = 0 if brand_datas.get(brand_name).get('outward_qty') == None else brand_datas.get(brand_name).get('outward_qty')
                    old_sales_rate = 0 if brand_datas.get(brand_name).get('sales_rate') == None else brand_datas.get(brand_name).get('sales_rate')
                    old_closing_stock = 0 if brand_datas.get(brand_name).get('closing_stock') == None else brand_datas.get(brand_name).get('closing_stock')
                    old_closing_value = 0 if brand_datas.get(brand_name).get('closing_value') == None else brand_datas.get(brand_name).get('closing_value')

                    opening_qty = old_opening_qty + (0 if details.get('opening_qty')  == None else details.get('opening_qty'))
                    inward_qty = old_inward_qty + (0 if details.get('inward_qty')  == None else details.get('inward_qty'))
                    purchase_rate = old_purchase_rate + (0 if details.get('cost_price') == None else details.get('cost_price'))
                    outward_qty =  old_outward_qty + (0 if details.get('outward_qty') == None else details.get('outward_qty'))
                    sales_rate = old_sales_rate + (0 if details.get('sales_rate') == None else details.get('sales_rate'))
    
                    closing_stock = old_closing_stock + details.get('opening_qty') + ( 0 if details.get('inward_qty') == None else details.get('inward_qty') ) + (0 if details.get('outward_qty') == None else details.get('outward_qty'))
                    closing_value = old_closing_value + int(details.get('opening_qty') + (0 if details.get('inward_qty') == None else details.get('inward_qty')) + (0 if details.get('outward_qty') == None else details.get('outward_qty'))) * float(details.get('cost_price'))

                    brand_datas[brand_name].update(
                        {
                            'opening_qty':opening_qty,
                            'inward_qty':inward_qty,
                            'purchase_rate':purchase_rate,
                            'outward_qty':outward_qty,
                            'sales_rate':sales_rate,
                            'closing_stock':closing_stock,
                            'closing_value':closing_value 
                        }
                        )

        brand_records = []
        for brand_name,details in brand_datas.items():
            brand_records.append({
                'name':brand_name,
                'opening_qty':details.get('opening_qty'),
                'selling_price':details.get('sales_rate'),
                'outward_qty':details.get('outward_qty'),
                'purchase_rate':details.get('purchase_rate'),
                'inward_qty':details.get('inward_qty'),
                'closing_stock':details.get('closing_stock'),
                'closing_value':details.get('closing_value')
            })
        
        return brand_records

    def __groupby_brands(self,MIXED_TRANSACTIONS):
        grouped_transactions = {}
        for count,transaction in enumerate(MIXED_TRANSACTIONS):
            brand_name = transaction.get('brand_name')
            item_name = transaction.get('item_name')

            if 'inward_qty' in transaction.keys():
                inward_qty = transaction.get('inward_qty')
                purchase_rate = transaction.get('purchase_rate')

                if grouped_transactions.get(brand_name) == None:
                    grouped_transactions[brand_name] = {item_name:{'inward_qty':inward_qty,'purchase_rate':purchase_rate}}
                elif grouped_transactions.get(brand_name).get(item_name) == None:
                    grouped_transactions[brand_name].update({item_name:{'inward_qty':inward_qty,'purchase_rate':purchase_rate}})
                else:
                    old_inward_qty = 0 if grouped_transactions.get(brand_name).get(item_name).get('inward_qty') == None else grouped_transactions.get(brand_name).get(item_name).get('inward_qty')
                    inward_qty = old_inward_qty + transaction.get('inward_qty')
                    grouped_transactions[brand_name][item_name].update({'inward_qty':inward_qty,'purchase_rate':purchase_rate})

            elif 'outward_qty' in transaction.keys():
                outward_qty = transaction.get('outward_qty')
                sales_rate = transaction.get('sales_rate')

                if grouped_transactions.get(brand_name) == None:
                    grouped_transactions[brand_name] = {item_name:{'outward_qty':outward_qty,'sales_rate':sales_rate}} 
                elif grouped_transactions.get(brand_name).get(item_name) == None:
                    grouped_transactions[brand_name].update({item_name:{'outward_qty':outward_qty,'sales_rate':sales_rate}})
                else:
                    old_outward_qty = 0 if grouped_transactions.get(brand_name).get(item_name).get('outward_qty') == None else grouped_transactions.get(brand_name).get(item_name).get('outward_qty')
                    outward_qty = old_outward_qty + transaction.get('outward_qty')
                    grouped_transactions[brand_name][item_name].update({'outward_qty':outward_qty,'sales_rate':sales_rate})
            else:
                item_id = transaction.get('item_id')
                opening_qty = transaction.get('opening_qty')
                cost_price = transaction.get('cost_price')
                
                if not brand_name in grouped_transactions.keys():
                    grouped_transactions[brand_name] = {item_name:{'item_id': item_id,'opening_qty': opening_qty,'outward_qty':0,'inward_qty':0,'cost_price': cost_price}}
                elif grouped_transactions.get(brand_name).get(item_name) == None:
                    grouped_transactions[brand_name].update({item_name:{'item_id': item_id,'opening_qty': opening_qty,'outward_qty':0,'inward_qty':0,'cost_price': cost_price}})
                else:
                    grouped_transactions[brand_name][item_name].update({'item_id': item_id,'opening_qty': opening_qty,'cost_price': cost_price})
        
        return grouped_transactions

    def fetch_category(self):
        self.mycursor.execute("""
                                SELECT item_master.id AS item_id,item_master.name AS item_name,SUM(item_master.opening_qty) AS opening_qty,
                                departments.name AS department_name,margin,cost_price,brands.name AS brand_name,category.name AS category_name
                                from ITEM_MASTER
                                INNER JOIN departments ON item_master.department_id = departments.id
                                INNER JOIN brands on item_master.brand_id = brands.id
                                INNER JOIN gst_master on item_master.gst_id = gst_master.id
                                INNER JOIN cess_master on item_master.cess_id = cess_master.id
                                INNER JOIN category on item_master.category_id = category.id
                                GROUP BY ITEM_MASTER.id
                            """)

        records = self.mycursor.fetchall()
        purchase_datas = self.get_purchase_voucher()
        sales_datas = self.get_sales_voucher()
        total_transactions = purchase_datas + sales_datas + records
        
        category_wise_records = self.__group_by_category(total_transactions)

        category_datas = {}
        for category_name,item_details in category_wise_records.items():
            for item_name,details in item_details.items():
                if category_datas.get(category_name) == None:
                    opening_qty = details.get('opening_qty')
                    inward_qty = 0 if details.get('inward_qty') == None else details.get('inward_qty')
                    purchase_rate = details.get('cost_price')
                    outward_qty = 0 if details.get('outward_qty') == None else details.get('outward_qty')
                    sales_rate = details.get('sales_rate')
                    closing_stock = (opening_qty + inward_qty) - outward_qty
                    closing_value = int(closing_stock) * float(purchase_rate)

                    category_datas[category_name] = {
                        'opening_qty':opening_qty,
                        'inward_qty':inward_qty,
                        'purchase_rate':purchase_rate,
                        'outward_qty':outward_qty,
                        'sales_rate':sales_rate,
                        'closing_stock':closing_stock,
                        'closing_value':closing_value
                        }
                else:
                    old_opening_qty = category_datas.get(category_name).get('opening_qty')
                    old_inward_qty = category_datas.get(category_name).get('inward_qty')
                    old_purchase_rate = category_datas.get(category_name).get('purchase_rate')
                    old_outward_qty = category_datas.get(category_name).get('outward_qty')
                    old_sales_rate = category_datas.get(category_name).get('sales_rate')
                    old_closing_stock = category_datas.get(category_name).get('closing_stock')
                    old_closing_value = category_datas.get(category_name).get('closing_value')

                    opening_qty = old_opening_qty + (0 if details.get('opening_qty')  == None else details.get('opening_qty'))
                    inward_qty = old_inward_qty + (0 if details.get('inward_qty')  == None else details.get('inward_qty'))
                    purchase_rate = old_purchase_rate + (0 if details.get('cost_price') == None else details.get('cost_price'))
                    outward_qty =  old_outward_qty + (0 if details.get('outward_qty') == None else details.get('outward_qty'))
                    sales_rate = old_sales_rate + (0 if details.get('sales_rate') == None else details.get('sales_rate'))

                    closing_stock = old_closing_stock + details.get('opening_qty') + details.get('inward_qty') + (0 if details.get('outward_qty') == None else details.get('outward_qty'))
                    closing_value = old_closing_value + int(details.get('opening_qty') + details.get('inward_qty') + (0 if details.get('outward_qty') == None else details.get('outward_qty'))) * float(details.get('cost_price'))

                    category_datas[category_name].update(
                        {
                            'opening_qty':opening_qty,
                            'inward_qty':inward_qty,
                            'purchase_rate':purchase_rate,
                            'outward_qty':outward_qty,
                            'sales_rate':sales_rate,
                            'closing_stock':closing_stock,
                            'closing_value':closing_value 
                        }
                        )

        category_records = []
        for category_name,details in category_datas.items():
            category_records.append({
                'name':category_name,
                'opening_qty':details.get('opening_qty'),
                'selling_price':details.get('sales_rate'),
                'outward_qty':details.get('outward_qty'),
                'purchase_rate':details.get('purchase_rate'),
                'inward_qty':details.get('inward_qty'),
                'closing_stock':details.get('closing_stock'),
                'closing_value':details.get('closing_value')
            })
        
        return category_records

    def __group_by_category(self,MIXED_TRANSACTIONS):
        grouped_transactions = {}

        for count,transaction in enumerate(MIXED_TRANSACTIONS):

            category_name = transaction.get('category_name')
            item_name = transaction.get('item_name')

            if 'inward_qty' in transaction.keys():
                inward_qty = transaction.get('inward_qty')
                purchase_rate = transaction.get('purchase_rate')

                if grouped_transactions.get(category_name) == None:
                    grouped_transactions[category_name] = {item_name:{'inward_qty':inward_qty,'purchase_rate':purchase_rate}}
                elif grouped_transactions.get(category_name).get(item_name) == None:
                    grouped_transactions[category_name].update({item_name:{'inward_qty':inward_qty,'purchase_rate':purchase_rate}})
                else:
                    old_inward_qty = 0 if grouped_transactions.get(category_name).get(item_name).get('inward_qty') else grouped_transactions.get(category_name).get(item_name).get('inward_qty')
                    inward_qty = old_inward_qty + transaction.get('inward_qty')
                    grouped_transactions[category_name][item_name].update({'inward_qty':inward_qty,'purchase_rate':purchase_rate})

            elif 'outward_qty' in transaction.keys():
                outward_qty = transaction.get('outward_qty')
                sales_rate = transaction.get('sales_rate')

                if grouped_transactions.get(category_name) == None:
                    grouped_transactions[category_name] = {item_name:{'outward_qty':outward_qty,'sales_rate':sales_rate}}
                elif grouped_transactions.get(category_name).get(item_name) == None:
                    grouped_transactions[category_name].update({item_name:{'outward_qty':outward_qty,'sales_rate':sales_rate}})
                else:
                    old_outward_qty = 0 if grouped_transactions.get(category_name).get(item_name).get('outward_qty') == None else grouped_transactions.get(category_name).get(item_name).get('outward_qty')
                    outward_qty = old_outward_qty + transaction.get('outward_qty')
                    grouped_transactions[category_name][item_name].update({'outward_qty':outward_qty,'sales_rate':sales_rate})
            else:
                item_id = transaction.get('item_id')
                opening_qty = transaction.get('opening_qty')
                cost_price = transaction.get('cost_price')

                if not category_name in grouped_transactions.keys():
                    grouped_transactions[category_name] = {item_name:{'item_id':item_id,'opening_qty':opening_qty,'cost_price':cost_price,'outward_qty':0,'inward_qty':0}}
                elif grouped_transactions.get(category_name).get(item_name) == None:
                    grouped_transactions[category_name].update({item_name:{'item_id':item_id,'opening_qty':opening_qty,'cost_price':cost_price,'outward_qty':0,'inward_qty':0}})
                else:
                    grouped_transactions[category_name][item_name].update({'item_id':item_id,'opening_qty':opening_qty,'cost_price':cost_price})
            
        return grouped_transactions

    def fetch_sub_category(self):
        self.mycursor.execute(
                    """
                        SELECT item_master.id AS item_id,item_master.name AS item_name,SUM(item_master.opening_qty) AS opening_qty,
                        departments.name AS department_name,margin,cost_price,brands.name AS brand_name,category.name AS category_name,
                        sub_category.name AS sub_category
                        from ITEM_MASTER
                        INNER JOIN departments ON item_master.department_id = departments.id
                        INNER JOIN brands on item_master.brand_id = brands.id
                        INNER JOIN gst_master on item_master.gst_id = gst_master.id
                        INNER JOIN cess_master on item_master.cess_id = cess_master.id
                        INNER JOIN category on item_master.category_id = category.id
                        INNER JOIN sub_category on item_master.sub_category_id = sub_category.id
                        GROUP BY ITEM_MASTER.id
                    """)
        
        records = self.mycursor.fetchall()
        purchase_datas = self.get_purchase_voucher()
        sales_datas = self.get_sales_voucher()
        total_transactions = purchase_datas + sales_datas + records

        sub_category_vise_records = self.__group_by_sub_category(total_transactions)
        
        sub_category_datas = {}
        for sub_category_name,item_details in sub_category_vise_records.items():
            for item_name,details in item_details.items():
                if sub_category_datas.get(sub_category_name) == None:
                    opening_qty = details.get('opening_qty')
                    inward_qty = 0 if details.get('inward_qty') == None else details.get('inward_qty')
                    purchase_rate = details.get('cost_price')
                    outward_qty = 0 if details.get('outward_qty') == None else details.get('outward_qty')
                    sales_rate = details.get('sales_rate')
                    closing_stock = (opening_qty + inward_qty) - outward_qty
                    closing_value = int(closing_stock) * float(purchase_rate)

                    sub_category_datas[sub_category_name] = {
                        'opening_qty':opening_qty,
                        'inward_qty':inward_qty,
                        'purchase_rate':purchase_rate,
                        'outward_qty':outward_qty,
                        'sales_rate':sales_rate,
                        'closing_stock':closing_stock,
                        'closing_value':closing_value
                        }

                else:
                    old_opening_qty = sub_category_datas.get(sub_category_name).get('opening_qty')
                    old_inward_qty = sub_category_datas.get(sub_category_name).get('inward_qty')
                    old_purchase_rate = sub_category_datas.get(sub_category_name).get('purchase_rate')
                    old_outward_qty = sub_category_datas.get(sub_category_name).get('outward_qty')
                    old_sales_rate = sub_category_datas.get(sub_category_name).get('sales_rate')
                    old_closing_stock = sub_category_datas.get(sub_category_name).get('closing_stock')
                    old_closing_value = sub_category_datas.get(sub_category_name).get('closing_value')

                    opening_qty = old_opening_qty + (0 if details.get('opening_qty')  == None else details.get('opening_qty'))
                    inward_qty = old_inward_qty + (0 if details.get('inward_qty')  == None else details.get('inward_qty'))
                    purchase_rate = old_purchase_rate + (0 if details.get('cost_price') == None else details.get('cost_price'))
                    outward_qty =  old_outward_qty + (0 if details.get('outward_qty') == None else details.get('outward_qty'))
                    sales_rate = old_sales_rate + (0 if details.get('sales_rate') == None else details.get('sales_rate'))
    
                    closing_stock = old_closing_stock + details.get('opening_qty') + details.get('inward_qty') + (0 if details.get('outward_qty') == None else details.get('outward_qty'))
                    closing_value = old_closing_value + int(details.get('opening_qty') + details.get('inward_qty') + (0 if details.get('outward_qty') == None else details.get('outward_qty'))) * float(details.get('cost_price'))

                    sub_category_datas[sub_category_name].update(
                        {
                            'opening_qty':opening_qty,
                            'inward_qty':inward_qty,
                            'purchase_rate':purchase_rate,
                            'outward_qty':outward_qty,
                            'sales_rate':sales_rate,
                            'closing_stock':closing_stock,
                            'closing_value':closing_value 
                        }
                        )

        sub_category_records = []
        for sub_category_name,details in sub_category_datas.items():
            sub_category_records.append({
                'name':sub_category_name,
                'opening_qty':details.get('opening_qty'),
                'selling_price':details.get('sales_rate'),
                'outward_qty':details.get('outward_qty'),
                'purchase_rate':details.get('purchase_rate'),
                'inward_qty':details.get('inward_qty'),
                'closing_stock':details.get('closing_stock'),
                'closing_value':details.get('closing_value')
            })

        return sub_category_records

    def __group_by_sub_category(self,MIXED_TRANSACTIONS):
        grouped_transactions = {}

        for count,transaction in enumerate(MIXED_TRANSACTIONS):
            sub_category_name = transaction.get('sub_category')
            item_name = transaction.get('item_name')

            if 'inward_qty' in transaction.keys():
                inward_qty = transaction.get('inward_qty')
                purchase_rate = transaction.get('purchase_rate')

                if grouped_transactions.get(sub_category_name) == None:
                    grouped_transactions[sub_category_name] = {item_name:{'inward_qty':inward_qty,'purchase_rate':purchase_rate}}
                elif grouped_transactions.get(sub_category_name).get(item_name) == None:
                    grouped_transactions[sub_category_name].update({item_name:{'inward_qty':inward_qty,'purchase_rate':purchase_rate}})
                else:
                    old_inward_qty = 0 if grouped_transactions.get(sub_category_name).get('inward_qty') == None else grouped_transactions.get(sub_category_name).get('inward_qty')
                    inward_qty = old_inward_qty + transaction.get('inward_qty')
                    grouped_transactions[sub_category_name][item_name].update({'inward_qty':inward_qty,'purchase_rate':purchase_rate})

            elif 'outward_qty' in transaction.keys():
                outward_qty = transaction.get('outward_qty')
                sales_rate = transaction.get('sales_rate')

                if grouped_transactions.get(sub_category_name) == None:
                    grouped_transactions[sub_category_name] = {item_name:{'outward_qty':outward_qty,'sales_rate':sales_rate}}
                elif grouped_transactions.get(sub_category_name).get(item_name) == None:
                    grouped_transactions[sub_category_name].update({item_name:{'outward_qty':outward_qty,'sales_rate':sales_rate}})
                else:
                    old_outward_qty = 0 if grouped_transactions.get(sub_category_name).get('outward_qty') == None else grouped_transactions.get(sub_category_name).get('outward_qty')
                    outward_qty = old_outward_qty + transaction.get('outward_qty')
                    grouped_transactions[sub_category_name][item_name].update({'outward_qty':outward_qty,'sales_rate':sales_rate})
            else:
                item_id = transaction.get('item_id')
                opening_qty = transaction.get('opening_qty')
                cost_price = transaction.get('cost_price')

                if not sub_category_name in grouped_transactions.keys():
                    grouped_transactions[sub_category_name] = {item_name:{'item_id':item_id,'opening_qty':opening_qty,'cost_price':cost_price,'outward_qty':0,'inward_qty':0}}
                elif grouped_transactions.get(sub_category_name).get(item_name) == None:
                    grouped_transactions[sub_category_name].update({item_name:{'item_id':item_id,'opening_qty':opening_qty,'cost_price':cost_price,'outward_qty':0,'inward_qty':0}})
                else:
                    grouped_transactions[sub_category_name][item_name].update({'item_id':item_id,'opening_qty':opening_qty,'cost_price':cost_price})
        
        return grouped_transactions

    def fetch_batchs(self):
        self.mycursor.execute(
                            """
                            SELECT item_master.id AS item_id,item_master.name AS item_name,item_master.opening_qty AS opening_qty,
                            departments.name AS department_name,margin,cost_price,brands.name AS brand_name
                            from ITEM_MASTER
                            INNER JOIN departments ON item_master.department_id = departments.id
                            INNER JOIN brands on item_master.brand_id = brands.id
                            INNER JOIN gst_master on item_master.gst_id = gst_master.id
                            INNER JOIN cess_master on item_master.cess_id = cess_master.id
                            """
                            )
        records = self.mycursor.fetchall()

        self.mycursor.execute(
                            """
                            SELECT item_master.name AS item_name,quantity AS inward_qty,ROUND(rate) AS purchase_rate,
                            departments.name AS department_name,brands.name As brand_name,category.name AS category_name,
                            sub_category.name AS sub_category,purchase_voucher_list.batch_no AS batch_no
                            FROM purchase_voucher_list
                            INNER JOIN ITEM_MASTER ON purchase_voucher_list.item_id = item_master.id
                            INNER JOIN departments on item_master.department_id = departments.id
                            INNER JOIN brands on item_master.brand_id = brands.id
                            INNER JOIN category on item_master.category_id = category.id
                            INNER JOIN sub_category on item_master.sub_category_id = sub_category.id
                            """
                            )

        purchase_datas = self.mycursor.fetchall()

        self.mycursor.execute(
                            """
                            SELECT item_master.name AS item_name,quantity AS outward_qty,ROUND(rate) AS sales_rate,
                            departments.name AS department_name,brands.name AS brand_name,category.name AS category_name,
                            sub_category.name AS sub_category,sales_voucher_list.batch_no AS batch_no
                            FROM sales_voucher_list
                            INNER JOIN ITEM_MASTER ON sales_voucher_list.item_id = item_master.id
                            INNER JOIN departments on item_master.department_id = departments.id
                            INNER JOIN brands on item_master.brand_id = brands.id
                            INNER JOIN category on item_master.category_id = category.id
                            INNER JOIN sub_category on item_master.sub_category_id = sub_category.id
                            """
                            )

        sales_datas = self.mycursor.fetchall()

        total_transactions = purchase_datas + sales_datas + records


        batch_wise_records = self.__groupby_batches(total_transactions)
        
        batches_datas = {}
        for item_name,item_details in batch_wise_records.items():
            opening_qty = item_details.get('opening_qty')
            cost_price = item_details.get('cost_price')

            if item_details.get('batches') != None:
                for batch_name,details in item_details.get('batches').items():
                    if batches_datas.get(item_name) == None:
                        inward_qty = details.get('inward_qty')
                        purchase_rate = details.get('purchase_rate')
                        outward_qty = details.get('outward_qty')
                        sales_rate = details.get('sales_rate')
                        closing_stock = (opening_qty + inward_qty) - outward_qty
                        closing_value = int(closing_stock) * float(purchase_rate)

                        batches_datas[item_name] = {
                            batch_name:{
                            'opening_qty':opening_qty,
                            'inward_qty':inward_qty,
                            'purchase_rate':purchase_rate,
                            'outward_qty':outward_qty,
                            'sales_rate':sales_rate,
                            'closing_stock':closing_stock,
                            'closing_value':closing_value
                            }
                        }

                    elif batches_datas.get(item_name).get(batch_name) == None:
                        inward_qty = details.get('inward_qty')
                        purchase_rate = details.get('purchase_rate')
                        outward_qty = details.get('outward_qty')
                        sales_rate = details.get('sales_rate')

                        closing_stock = (opening_qty + inward_qty) - outward_qty
                        closing_value = int(closing_stock) * float(purchase_rate)

                        batches_datas[item_name].update({batch_name:{
                            'opening_qty':opening_qty,
                            'inward_qty':inward_qty,
                            'purchase_rate':purchase_rate,
                            'outward_qty':outward_qty,
                            'sales_rate':sales_rate,
                            'closing_stock':closing_stock,
                            'closing_value':closing_value
                            }})

                    else:
                        old_opening_qty = batches_datas.get(item_name).get(batch_name).get('opening_qty')
                        old_inward_qty = batches_datas.get(item_name).get(batch_name).get('inward_qty')
                        old_purchase_rate = batches_datas.get(item_name).get(batch_name).get('purchase_rate')
                        old_outward_qty = batches_datas.get(item_name).get(batch_name).get('outward_qty')
                        old_sales_rate = batches_datas.get(item_name).get(batch_name).get('sales_rate')
                        old_closing_stock = batches_datas.get(item_name).get(batch_name).get('closing_stock')
                        old_closing_value = batches_datas.get(item_name).get(batch_name).get('closing_value')

                        opening_qty = old_opening_qty + (0 if details.get('opening_qty')  == None else details.get('opening_qty'))
                        inward_qty = old_inward_qty + (0 if details.get('inward_qty')  == None else details.get('inward_qty'))
                        purchase_rate = old_purchase_rate + (0 if details.get('cost_price') == None else details.get('cost_price'))
                        outward_qty =  old_outward_qty + (0 if details.get('outward_qty') == None else details.get('outward_qty'))
                        sales_rate = old_sales_rate + (0 if details.get('sales_rate') == None else details.get('sales_rate'))
        
                        closing_stock = old_closing_stock + details.get('opening_qty') + details.get('inward_qty') + (0 if details.get('outward_qty') == None else details.get('outward_qty'))
                        closing_value = old_closing_value + int(details.get('opening_qty') + details.get('inward_qty') + (0 if details.get('outward_qty') == None else details.get('outward_qty'))) * float(details.get('cost_price'))


                        batches_datas[item_name][batch_name].update({
                            'opening_qty':opening_qty,
                            'inward_qty':inward_qty,
                            'purchase_rate':purchase_rate,
                            'outward_qty':outward_qty,
                            'sales_rate':sales_rate,
                            'closing_stock':closing_stock,
                            'closing_value':closing_value
                            })
            else:
                pass
                # FIX BATCHES IN ITEM_MASTER AND COME

        
        # print(batches_datas)
                    

    def __groupby_batches(self,MIXED_TRANSACTIONS):

        grouped_transactions = {}

        for count,transaction in enumerate(MIXED_TRANSACTIONS):
            item_name = transaction.get('item_name')

            if 'inward_qty' in transaction.keys():
                batch_no = transaction.get('batch_no')
                inward_qty = transaction.get('inward_qty')
                purchase_rate = transaction.get('purchase_rate')

                if grouped_transactions.get(item_name) == None:
                    grouped_transactions[item_name] = {'batches':{batch_no:{'inward_qty':inward_qty,'purchase_rate':purchase_rate}}}
                elif grouped_transactions.get(item_name).get('batches').get(batch_no) == None:
                    grouped_transactions.get(item_name).get('batches').update({batch_no:{'inward_qty':inward_qty,'purchase_rate':purchase_rate}})
                else:
                    old_inward_qty = 0 if grouped_transactions.get(item_name).get('batches').get(batch_no).get('inward_qty') == None else grouped_transactions.get(item_name).get('batches').get(batch_no).get('inward_qty')
                    inward_qty = old_inward_qty + transaction.get('inward_qty')
                    grouped_transactions[item_name]['batches'][batch_no].update({'inward_qty':inward_qty,'purchase_rate':purchase_rate})
                    
            elif 'outward_qty' in transaction.keys():
                batch_no = transaction.get('batch_no')
                outward_qty = transaction.get('outward_qty')
                sales_rate = transaction.get('sales_rate')
                
                if grouped_transactions.get(item_name) == None:
                    grouped_transactions[item_name] = {'batches':{batch_no:{'outward_qty':outward_qty,'sales_rate':sales_rate}}}
                elif grouped_transactions.get(item_name).get('batches').get(batch_no) == None:
                    grouped_transactions.get(item_name).get('batches').update({batch_no:{'outward_qty':outward_qty,'sales_rate':sales_rate}})
                else:
                    old_outward_qty = 0 if grouped_transactions.get(item_name).get('batches').get(batch_no).get('sales_rate') == None else grouped_transactions.get(item_name).get('batches').get(batch_no).get('outward_qty')
                    outward_qty = old_outward_qty + transaction.get('outward_qty')
                    grouped_transactions[item_name]['batches'][batch_no].update({'outward_qty':outward_qty,'sales_rate':sales_rate})

            else:
                item_id = transaction.get('item_id')
                opening_qty = transaction.get('opening_qty')
                cost_price = transaction.get('cost_price')    
                
                if item_name not in grouped_transactions.keys():
                    grouped_transactions[item_name] = {'item_id': item_id,'opening_qty': opening_qty,'cost_price': cost_price,'outward_qty':0,'inward_qty':0}
                else:
                    grouped_transactions[item_name].update({'item_id': item_id,'opening_qty': opening_qty,'cost_price': cost_price})

        return grouped_transactions


    def insert_data(self):
        pass

    def update_data(self):
        pass

    def delete_data(self):
        pass
    

    def search_item(self,item_name):
        pass
