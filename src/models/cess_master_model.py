from src.models.base_model import BaseModel

class CessMasterModel(BaseModel):
    def __init__(self):
        super(CessMasterModel, self).__init__()
        self._create_table()

    def _create_table(self):
        self.mycursor.execute(
                "CREATE TABLE IF NOT EXISTS CESS_MASTER (id INT NOT NULL AUTO_INCREMENT,name varchar(200),percentage int,primary key(id) )")


    def fetch_all(self):
        self.mycursor.execute('SELECT id AS cess_id,name AS cess_name,percentage FROM CESS_MASTER')
        records = self.mycursor.fetchall()
        return records

    def insert_data(self,cess_name,cess_percentage):
        self.mycursor.execute(
                "INSERT INTO CESS_MASTER (name,percentage) VALUES ('"+cess_name+"','"+cess_percentage+"')")
        self.save_changes()


    def update_data(self,cess_name,cess_percentage,cess_id):
        self.mycursor.execute(
            "UPDATE CESS_MASTER SET name = '" + cess_name + "', percentage='"+cess_percentage+"' WHERE id ='" + cess_id + "'")
        self.save_changes()


    def delete_data(self,cess_id):
        self.mycursor.execute("DELETE FROM CESS_MASTER WHERE ID= '"+cess_id+"' ")
        self.save_changes()