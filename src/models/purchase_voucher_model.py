from src.models.base_model import BaseModel

class PurchaseModel(BaseModel):
    def __init__(self):
        super(PurchaseModel, self).__init__()
        self._create_table()

    def _create_table(self):
        self.mycursor.execute("""CREATE TABLE IF NOT EXISTS PURCHASE_VOUCHER (id int not null auto_increment,date DATE,invoice_date DATE,invoice_no varchar(200),gst_type varchar(200),
                                purchase_voucher_no varchar(200),party_account_details varchar(200),primary key(id)
                               )""")

        self.mycursor.execute("""CREATE TABLE IF NOT EXISTS PURCHASE_VOUCHER_LIST (id int not null auto_increment,barcode varchar(200),batch_no varchar(200),
                                hsn_code varchar(200),quantity int,rate float,discount int,taxable_amount float,cgst int,sgst int,igst int,cess_id int,total_amount float,
                                manufacturing_date DATE,expiry_date DATE,
                                purchase_voucher_id int,ledger_id int,item_id int,
                                CONSTRAINT item_id FOREIGN KEY (item_id) REFERENCES ITEM_MASTER (id),
                                CONSTRAINT ledger_id FOREIGN KEY (ledger_id) REFERENCES LEDGER (id),
                                CONSTRAINT purchase_voucher_id FOREIGN KEY (purchase_voucher_id) REFERENCES PURCHASE_VOUCHER (id),
                                primary key(id)
                            )""")

    def fetch_all(self):
        pass

    def insert_data(self,invoice_no,purchase_voucher,party_account_type,gst_type,party_account_date,invoice_date):
        self.mycursor.execute("INSERT INTO PURCHASE_VOUCHER (invoice_no,purchase_voucher_no,party_account_details,gst_type,invoice_date,date) VALUES ('"+invoice_no+"','"+purchase_voucher+"','"+party_account_type+"','"+gst_type+"',str_to_date('"+invoice_date+"', '%d-%m-%Y'),str_to_date('"+party_account_date+"', '%d-%m-%Y') )") 
        self.save_changes()
        
        return self.mycursor.lastrowid

    def update_data(self):
        pass

    def delete_data(self):
        pass

    def insert_items_data(self,items_list,purchase_voucher_id):
        for item in items_list:
            barcode = item.get('barcode')
            batch_no = item.get('batch_no')
            item_id = str(item.get('item').get('id'))
            hsn_no = item.get('hsn_no')
            quantity = item.get('quantity')
            rate = item.get('rate')
            discount = item.get('discount')
            taxable_amount = item.get('taxable_amount')
            cgst = item.get('cgst')
            sgst = item.get('sgst')
            igst = item.get('igst')
            cess_id = str(item.get('cess').get('id'))
            ledger_id = str(item.get('ledger').get('id'))
            total_amount = item.get('total_amount')
            mfg_date = item.get('mfg_date').replace('/', '-')
            expiry_date = item.get('expiry_date').replace('/', '-')

            self.mycursor.execute("INSERT INTO PURCHASE_VOUCHER_LIST(barcode,batch_no,hsn_code,quantity,rate,discount,taxable_amount,cgst,sgst,igst,cess_id,total_amount,purchase_voucher_id,ledger_id,item_id,manufacturing_date,expiry_date) VALUES ('"+barcode+"','"+batch_no+"','"+hsn_no+"','"+quantity+"','"+rate+"','"+discount+"','"+taxable_amount+"','"+cgst+"','"+sgst+"','"+igst+"','"+cess_id+"','"+total_amount+"','"+purchase_voucher_id+"','"+ledger_id+"','"+item_id+"',str_to_date('"+mfg_date+"', '%d-%m-%Y' ),str_to_date('"+expiry_date+"', '%d-%m-%Y' )) ")
        self.save_changes()


    def get_items(self,item_name):
        self.mycursor.execute("SELECT id AS item_id,name AS item_name FROM item_master where name LIKE '"+item_name+"%'")
        return self.mycursor.fetchall()


    def get_party_accounts(self):
        """Fetch all Ledgers Which Comes Under Sundry Credior Group"""
        self.mycursor.execute("select ledger.id AS ledger_id,ledger.name AS ledger_name from ledger INNER JOIN account_groups on ledger.account_group_id = account_groups.id where account_groups.name = 'Sundry Creditors'")
        return self.mycursor.fetchall()


    def get_item_details(self,item_id):
        self.mycursor.execute("SELECT barcode,hsn_code,selling_price FROM item_master where id = '"+item_id+"' ")
        return self.mycursor.fetchone()

    def get_cess_details(self):
        self.mycursor.execute("SELECT id AS cess_id,name AS cess_name FROM cess_master")
        return self.mycursor.fetchall()

    def get_cess_percentage(self,cess_id):
        self.mycursor.execute("""
                            SELECT percentage AS cess_percentage FROM cess_master where id = '{}'
                            """.format(cess_id))
                            
        return self.mycursor.fetchone().get('cess_percentage')

    def lpo_fetch(self,lpo_number):
        self.mycursor.execute("""select purchase_orders_list.barcode,purchase_orders_list.batch_no,purchase_orders_list.name,purchase_orders_list.hsn_code,
                                purchase_orders_list.quantity,purchase_orders_list.rate,purchase_orders_list.total_amount,
                                purchase_orders.party_account_details,purchase_orders.date
                                from purchase_orders_list inner join purchase_orders on purchase_orders_list.purchase_order_id = purchase_orders.id
                                where purchase_orders.order_no = '{}'
                                """.format(lpo_number))
        return self.mycursor.fetchall()

    def get_purchase_ledgers(self):
        self.mycursor.execute(""" SELECT id AS ledger_id,name AS ledger_name from ledger 
                                where account_group_id = (select id from account_groups where name = 'Direct Expense')
                            """)
        return self.mycursor.fetchall()


    def get_purchase_transactions(self):
        self.mycursor.execute("""SELECT purchase_voucher.invoice_no,purchase_voucher.invoice_date,purchase_voucher.gst_type,item_master.name AS item_name,ledger.name AS ledger_name,
                                purchase_voucher_list.quantity,purchase_voucher_list.total_amount
                                from purchase_voucher_list 
                                LEFT JOIN item_master on item_id = item_master.id
                                LEFT JOIN purchase_voucher on purchase_voucher_id = purchase_voucher.id
                                LEFT JOIN ledger on ledger_id = ledger.id
                            """)

        records = self.mycursor.fetchall()
        return records

    
    
    def get_supplier_gst_number(self,ledger_id):
        self.mycursor.execute("""
                            SELECT gst_no AS supplier_gst_no from ledger where id = '{}'
                            """.format(ledger_id))
        records = self.mycursor.fetchone()
        return records.get('supplier_gst_no')
        
    def get_company_gst_number(self):
        self.mycursor.execute("""
                                SELECT gst_no AS company_gst_no from company_master
                            """)
        records = self.mycursor.fetchone()
        return records.get('company_gst_no')

    def get_ledger_gst_percentage(self,ledger_id):
        self.mycursor.execute("""
                                SELECT gst_percentage from ledger where id='{}'
                            """.format(ledger_id))
        
        records = self.mycursor.fetchone()

        return records.get('gst_percentage')
