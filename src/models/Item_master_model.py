import re
from tkinter.constants import S
from src.models.base_model import BaseModel
from src.helpers import get_id_from_string


class ItemMasterModel(BaseModel):
    def __init__(self):
        super(ItemMasterModel, self).__init__()
        self._create_table()

    
    def _create_table(self):
        self.mycursor.execute(
                """CREATE TABLE IF NOT EXISTS ITEM_MASTER (id INT NOT NULL AUTO_INCREMENT,name varchar(200),item_code varchar(200),
                    barcode varchar(200),hsn_code varchar(200),brand_id int,department_id int,category_id int,sub_category_id int,
                    unit_id int,cost_price float,gst_id int,cess_id int,margin float,selling_price float,mrp float,rack_id int,
                    opening_qty int,alternative_unit_id int,alternative_unit varchar(200),expiry DATE,primary key(id)
                    )""")

    def brand_fetch(self):
        self.mycursor.execute('SELECT id,name from BRANDS')
        records = self.mycursor.fetchall()
        return records

    def dept_fetch(self):
        self.mycursor.execute('SELECT id,name FROM DEPARTMENTS')
        records = self.mycursor.fetchall()
        return records

    def cat_fetch(self,dept):
        dept_id = get_id_from_string(dept)
        self.mycursor.execute('SELECT id,name FROM CATEGORY where  department_id = "'+dept_id+'" ')
        records = self.mycursor.fetchall()
        return records

    def sub_cat_fetch(self,cat_id):
        cat_id = get_id_from_string(cat_id)
        self.mycursor.execute('SELECT id,name FROM SUB_CATEGORY where category_id = "'+cat_id+'" ')
        records = self.mycursor.fetchall()
        return records
    
    def unit_fetch(self,unit_name):
        self.mycursor.execute("SELECT id AS unit_id,name AS unit_name from units WHERE name LIKE '"+unit_name+"%' ")
        records = self.mycursor.fetchall()
        return records

    def gst_fetch(self,gst_name):
        self.mycursor.execute("SELECT id AS gst_id,name AS gst_name from gst_master where Name LIKE '"+gst_name+"%' ")
        records = self.mycursor.fetchall()
        return records

    def ces_fetch(self,cess_name):
        self.mycursor.execute("SELECT id AS cess_id,name As cess_name from cess_master where name LIKE '"+cess_name+"%' ")
        records = self.mycursor.fetchall()
        return records


    def get_gst_percentage(self,gst_id):
        self.mycursor.execute("SELECT percentage AS gst_percentage from gst_master where id =  '"+gst_id+"' ")
        record = self.mycursor.fetchone()
        return record.get('gst_percentage')

    def get_cess_percentage(self,cess_id):
        self.mycursor.execute("SELECT percentage AS cess_percentage from cess_master where id = '"+cess_id+"' ")
        record = self.mycursor.fetchone()
        print(record)
        return record.get('cess_percentage')


    def fetch_all(self):
        self.mycursor.execute("""select item_master.id AS item_id,item_master.name AS item_name,item_master.barcode,item_master.hsn_code,brands.name AS brand_name,
                                departments.name AS department_name,category.name AS category_name,sub_category.name AS sub_category_name,units.name AS unit_name,
                                item_master.cost_price AS cost_price,gst_master.percentage AS gst_percentage ,cess_master.percentage AS cess_percentage,item_master.margin AS margin,
                                item_master.selling_price,item_master.mrp,racks.name AS rack_name,item_master.opening_qty,item_master.expiry
                                FROM item_master 
                                LEFT JOIN brands on item_master.brand_id = brands.id 
                                LEFT JOIN departments on item_master.department_id = departments.id 
                                LEFT JOIN category on item_master.category_id = category.id 
                                LEFT JOIN sub_category on item_master.sub_category_id = sub_category.id 
                                LEFT JOIN units on item_master.unit_id = units.id 
                                LEFT JOIN GST_MASTER on item_master.gst_id = gst_master.id 
                                LEFT JOIN CESS_MASTER ON item_master.cess_id = cess_master.id
                                LEFT JOIN RACKS ON item_master.rack_id = racks.id
                                """)

        records = self.mycursor.fetchall()
        return records


    def insert_data(self,item_name,item_code,hsn_code,barcode,
                    brand_id,department_id,category_id,sub_category_id,
                    cost_price,gst_id,cess_id,margin,selling_price,mrp,
                    rack_id,opening_qty,unit_id,alternative_unit_id,alternative_unit,expiry):

        self.mycursor.execute("INSERT INTO ITEM_MASTER (name,item_code,hsn_code,barcode,brand_id,department_id,category_id,sub_category_id,cost_price,gst_id,cess_id,margin,selling_price,mrp,rack_id,opening_qty,unit_id,alternative_unit_id,alternative_unit,expiry) VALUES ('"+item_name+"','"+item_code+"','"+hsn_code+"','"+barcode+"','"+brand_id+"','"+department_id+"','"+category_id+"','"+sub_category_id+"','"+cost_price+"','"+gst_id+"','"+cess_id+"','"+margin+"','"+selling_price+"','"+mrp+"','"+rack_id+"','"+opening_qty+"','"+unit_id+"','"+alternative_unit_id+"','"+alternative_unit+"', str_to_date('"+expiry+"', '%d-%m-%Y' ) )")
        self.save_changes()




    def update_data(self):
        pass


    def delete_data(self):
        pass


    def insert_unit_data(self,unit_name,no_of_decimal):
        self.mycursor.execute("INSERT INTO UNITS (name,no_of_decimal) VALUES ('"+unit_name+"','"+no_of_decimal+"')")
        self.save_changes()

    def insert_brand_data(self,brand_name,brand_type_id):
        self.mycursor.execute("insert into brands (name,type_id) values ('" + brand_name + "','"+brand_type_id+"')")
        self.save_changes()

    def insert_department_data(self,department_name):
        self.mycursor.execute("insert into departments (name) values ('" + department_name + "')")
        self.save_changes()

    
    def insert_category_data(self,category_name,department_id):
        self.mycursor.execute("INSERT INTO category (name,department_id) values ('"+category_name+"', '"+department_id+"') ")
        self.save_changes()

    
    def insert_sub_category_data(self,sub_category_name,category_id):
        self.mycursor.execute("INSERT INTO sub_category (name,category_id) values ('"+sub_category_name+"','"+category_id+"')")
        self.save_changes()
        

    def get_brand_types(self):
        self.mycursor.execute("SELECT id,name from BRAND_TYPES")
        records = self.mycursor.fetchall()
        return records

    def get_brands(self,brand_name):
        self.mycursor.execute("SELECT id AS brand_id,name AS brand_name from brands where name LIKE '"+brand_name+"%' ")
        records = self.mycursor.fetchall()
        return records

    def get_departments(self,department_name):
        self.mycursor.execute("SELECT id AS department_id,name AS department_name from DEPARTMENTS where name LIKE '"+department_name+"%' ")
        records = self.mycursor.fetchall()
        return records

    def get_category(self,category_name):
        self.mycursor.execute("SELECT id AS category_id,name AS category_name from category where name LIKE '"+category_name+"%' ")
        records = self.mycursor.fetchall()
        return records

    def get_sub_category(self,sub_category_name):
        self.mycursor.execute("SELECT id AS sub_category_id,name AS sub_category_name from sub_category where name LIKE '"+sub_category_name+"%' ")
        records = self.mycursor.fetchall()
        return records

    def get_alternate_units(self,selected_unit_name):
        self.mycursor.execute("SELECT id AS unit_id,name AS unit_name from units where name != '"+selected_unit_name+"' ")
        records = self.mycursor.fetchall()
        return records    
        
    def get_racks(self,rack_name):
        self.mycursor.execute("SELECT id AS rack_id,name AS rack_name from racks where name LIKE '"+rack_name+"%' ")
        records = self.mycursor.fetchall()
        return records


    def get_gst_percentage(self,gst_id):
        """GET GST %"""
        self.mycursor.execute("SELECT percentage AS gst_percentage from gst_master where id = '"+gst_id+"' ")
        gst_percentage = self.mycursor.fetchone()
        return gst_percentage.get('gst_percentage')
    