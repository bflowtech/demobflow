from src.models.base_model import BaseModel

class CompanyModel(BaseModel):
    def __init__(self):
        super(CompanyModel, self).__init__()
        self._create_table()
    
    def _create_table(self):
        self.mycursor.execute("""
                                CREATE TABLE IF NOT EXISTS COMPANY_MASTER (id int not null auto_increment,name varchar(200),address varchar(200),
                                city varchar(200),state varchar(200),country varchar(200),pincode varchar(200),gst_no varchar(200),phone_no varchar(200),
                                mobile_no varchar(200),fax_no varchar(200),email varchar(200),website varchar(200),primary key(id)
                                )
                            """)
        self.save_changes()
    


    def fetch_all(self):
        self.mycursor.execute("""
                                SELECT id,name,address,city,state,country,pincode,gst_no,phone_no,mobile_no,fax_no,email,website
                                FROM COMPANY_MASTER
                            """)
        return self.mycursor.fetchone()

    def insert_data(self,name,address,city,state,country,pincode,gst_no,phone_no,mobile_no,fax_no,email,website):
        self.mycursor.execute("""
                            INSERT INTO COMPANY_MASTER (name,address,city,state,country,pincode,gst_no,phone_no,mobile_no,fax_no,email,website) 
                            VALUES ('{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}')
                            """.format(name,address,city,state,country,pincode,gst_no,phone_no,mobile_no,fax_no,email,website)
                            )
        self.save_changes()

    def update_data(self,name,address,city,state,country,pincode,gst_no,phone_no,mobile_no,fax_no,email,website,company_id):
        self.mycursor.execute("""
                            UPDATE COMPANY_MASTER SET name='{}',address='{}',city='{}',state='{}',country='{}',pincode='{}',gst_no='{}',phone_no='{}',
                            mobile_no='{}',fax_no='{}',email='{}',website='{}' WHERE ID = '{}'
                            """.format(name,address,city,state,country,pincode,gst_no,phone_no,mobile_no,fax_no,email,website,company_id)
                            )
        self.save_changes()

    def delete_data(self):
        pass