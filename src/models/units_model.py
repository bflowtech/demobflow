from src.models.base_model import BaseModel

class UnitsModel(BaseModel):
    def __init__(self):
        super(UnitsModel, self).__init__()
        self._create_table()


    def _create_table(self):
        self.mycursor.execute(
                "CREATE TABLE IF NOT EXISTS UNITS (id INT NOT NULL AUTO_INCREMENT,name varchar(200),no_of_decimal int,primary key(id) )")

    def fetch_all(self):
        self.mycursor.execute("SELECT ID AS units_id , NAME AS unit_name, no_of_decimal from UNITS")
        records = self.mycursor.fetchall()
        return records

    def insert_data(self,unit_name,no_of_decimal):
        self.mycursor.execute("INSERT INTO UNITS (name,no_of_decimal) VALUES ('"+unit_name+"','"+no_of_decimal+"')")
        self.save_changes()

    def update_data(self,unit_name,no_of_decimel,unit_id):
        self.mycursor.execute(
            "UPDATE units SET name = '" + unit_name + "',no_of_decimal='" + no_of_decimel + "' WHERE id ='" + unit_id + "' ")
        self.save_changes()

    def delete_data(self,unit_id):
        self.mycursor.execute("DELETE FROM UNITS WHERE ID = '"+unit_id+"' ")
        self.save_changes()







