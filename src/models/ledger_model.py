from src.models.base_model import BaseModel

class LedgerModel(BaseModel):
    def __init__(self):
        super(LedgerModel, self).__init__()
        self._create_table()

    def _create_table(self):
        self.mycursor.execute("""CREATE TABLE IF NOT EXISTS LEDGER (id int not null auto_increment,name varchar(200),type varchar(200),credit_period varchar(200),contact_details varchar(200),
                                                                    address varchar(200),phone_no varchar(200),pancard_no varchar(200),gst_type varchar(200),gst_no varchar(200),
                                                                    gst_percentage varchar(200),
                                                                    duties_type varchar(200),tax_type varchar(200),tax_percentage varchar(200),
                                                                    opening_balance varchar(200),bank_name varchar(200),
                                                                    bank_address varchar(200),account_number varchar(200),ifsc_code varchar(200),
                                                                    branch_no varchar(200),branch_code varchar(200),bank_contact_no varchar(200),
                                                                    cheque_no varchar(200),
                                                                    account_group_id int,primary key(id),
                                                                    CONSTRAINT account_group_id FOREIGN KEY (account_group_id) REFERENCES account_groups(id)
                                                                    )""")
        self.save_changes()

    def fetch_all(self):
        pass

    def insert_data(self):
        pass

    def update_data(self):
        pass

    def delete_data(self):
        pass


    def get_accounts_groups(self,group_name):
        self.mycursor.execute("select account_groups.id,account_groups.name from account_groups INNER JOIN accounts  ON account_groups.account_id = accounts.id where accounts.name = '"+group_name+"' ")
        records = self.mycursor.fetchall()
        return records
        