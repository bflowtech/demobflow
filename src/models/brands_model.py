from src.models.base_model import BaseModel

class BrandsModel(BaseModel):
    def __init__(self):
        super(BrandsModel, self).__init__()
        self._create_table()

    def _create_table(self):
        self.mycursor.execute(
            "CREATE TABLE IF NOT EXISTS BRANDS (id INT NOT NULL AUTO_INCREMENT,name varchar(200),type_id int,CONSTRAINT type_id FOREIGN KEY (type_id) REFERENCES BRAND_TYPES(id),primary key(id) )")


    def insert_data(self,brand_name,brand_type_id):
        self.mycursor.execute("insert into brands (name,type_id) values ('" + brand_name + "','"+brand_type_id+"')")
        self.save_changes()

    def update_data(self,brand_name,brand_type_id,brand_id):
        self.mycursor.execute("UPDATE brands SET name = '" + brand_name + "',type_id='"+brand_type_id+"' WHERE id ='" + brand_id +"' ")
        self.save_changes()

    def fetch_all(self):
        self.mycursor.execute('SELECT BRANDS.id AS brand_id,BRANDS.name AS brand_name,BRAND_TYPES.name AS brand_type FROM BRANDS INNER JOIN BRAND_TYPES on brands.type_id = BRAND_TYPES.id')
        records = self.mycursor.fetchall()
        return records

    def delete_data(self,brand_id):
        self.mycursor.execute("DELETE FROM BRANDS WHERE id = '" + brand_id + "' ")
        self.save_changes()


    def serach_brand(self,brand_name):
        self.mycursor.execute("SELECT id,name,type FROM brands where name LIKE '"+brand_name+"%'")
        records = self.mycursor.fetchall()
        return records

    def fetch_brand_types(self):
        self.mycursor.execute("SELECT id,name from BRAND_TYPES")
        records = self.mycursor.fetchall()
        return records
        


