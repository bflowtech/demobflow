from src.models.base_model import BaseModel


class DepartmentsModel(BaseModel):
    def __init__(self):
        super(DepartmentsModel, self).__init__()
        self._create_table()


    def _create_table(self):
        self.mycursor.execute(
                "CREATE TABLE IF NOT EXISTS DEPARTMENTS (id INT NOT NULL AUTO_INCREMENT,name varchar(200),primary key(id))")

    def fetch_all(self):
        self.mycursor.execute('SELECT id as department_id,name as department_name FROM DEPARTMENTS')
        records = self.mycursor.fetchall()
        return records


    def insert_data(self,department_name):
        self.mycursor.execute("insert into departments (name) values ('" + department_name + "')")
        self.save_changes()


    def update_data(self,department_name,deparment_id):
        self.mycursor.execute("UPDATE DEPARTMENTS SET name = '" + department_name + "' WHERE id ='" + deparment_id + "'")
        self.save_changes()

    def delete_data(self,department_id):
        self.mycursor.execute("DELETE FROM DEPARTMENTS WHERE id = '" + department_id + "' ")
        self.save_changes()

    def serach_department(self,department_name):
        self.mycursor.execute("SELECT id,name FROM departments where name LIKE '"+department_name+"%' ")
        records = self.mycursor.fetchall()
        return records

