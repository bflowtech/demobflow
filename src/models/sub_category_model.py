from src.models.base_model import BaseModel

class SubCategoryModel(BaseModel):
    def __init__(self):
        super(SubCategoryModel, self).__init__()
        self._create_table()


    def _create_table(self):
        self.mycursor.execute(
        "CREATE TABLE IF NOT EXISTS SUB_CATEGORY (id INT NOT NULL AUTO_INCREMENT,name varchar(200),category_id int,primary key(id),CONSTRAINT category_id FOREIGN KEY(category_id) REFERENCES CATEGORY (id) )")

    def fetch_all(self):
        self.mycursor.execute(
            'select sub_category.id AS sub_category_id,sub_category.name AS sub_category_name,category.name AS category_name,departments.name AS department_name from ((sub_category inner join CATEGORY ON sub_category.category_id = category.id) INNER JOIN Departments ON category.department_id = departments.id )')
        records = self.mycursor.fetchall()
        return records

    def insert_data(self,sub_category_name, category_id):
        self.mycursor.execute(
            "insert into SUB_CATEGORY (name,category_id) values ('" + sub_category_name + "','" + category_id + "')")
        self.save_changes()

    def update_data(self,sub_category_name,category_id,sub_category_id):
        self.mycursor.execute(
            "UPDATE sub_category SET name = '" + sub_category_name + "',category_id='" + category_id + "' WHERE id ='" + sub_category_id + "' ")
        self.save_changes()

    def delete_data(self,sub_category_id):
        self.mycursor.execute("DELETE FROM SUB_CATEGORY WHERE ID = '"+sub_category_id+"' ")
        self.save_changes()

    def get_category(self):
        self.mycursor.execute("""
                                SELECT id AS category_id,name AS category_name FROM category
                            """)
        records = self.mycursor.fetchall()
        return records


