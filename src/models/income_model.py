from src.models.base_model import BaseModel

class IncomeModel(BaseModel):
    def __init__(self):
        super(IncomeModel, self).__init__()
        self._create_table()

    def _create_table(self):
        self.mycursor.execute("CREATE TABLE IF NOT EXISTS ACCOUNT_GROUPS (id int not null auto_increment,name varchar(200),account_id int,primary key(id),CONSTRAINT account_id FOREIGN KEY(account_id) REFERENCES ACCOUNTS(id) )")
        income_groups = ['Direct Income','Indirect Income','Miscellaneous Income','Sales','Sales Return']
        for income in income_groups:
            self.mycursor.execute("insert into ACCOUNT_GROUPS (name,account_id) SELECT * FROM (SELECT '"+income+"','3') as temp WHERE NOT EXISTS ( SELECT name FROM ACCOUNT_GROUPS WHERE name = '"+income+"') ")
        self.save_changes()
    


    def fetch_all(self):
        self.mycursor.execute("select account_groups.id,account_groups.name,accounts.name as account_name from account_groups INNER JOIN accounts  ON account_groups.account_id = accounts.id where accounts.name = 'INCOME'")
        records = self.mycursor.fetchall()
        return records

    def insert_data(self,income_group_name):
        self.mycursor.execute("INSERT INTO ACCOUNT_GROUPS(name,account_id) VALUES ('"+income_group_name+"','3') ")
        self.save_changes()
    

    def update_data(self,income_group_name,income_group_id):
        self.mycursor.execute("UPDATE ACCOUNT_GROUPS SET name='"+income_group_name+"' where id = '"+income_group_id+"' ")
        self.save_changes()

    def delete_data(self,income_group_id):
        self.mycursor.execute("DELETE FROM ACCOUNT_GROUPS where id = '"+income_group_id+"' ")
        self.save_changes()


    def insert_to_ledger(self,ledger_name,ledger_type,gst_percentage,opening_balance,group_id):
        self.mycursor.execute("INSERT INTO LEDGER(name,type,gst_percentage,opening_balance,account_group_id) VALUES ('"+ledger_name+"','"+ledger_type+"','"+gst_percentage+"','"+opening_balance+"','"+group_id+"')")
        self.save_changes()
        