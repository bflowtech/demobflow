from src.models.base_model import BaseModel


class SalesOrderModel(BaseModel):
    def __init__(self):
        super(SalesOrderModel, self).__init__()
        self._create_table()

    def _create_table(self):
        self.mycursor.execute("""CREATE TABLE IF NOT EXISTS SALES_ORDERS (id int not null auto_increment,date DATE,order_voucher_no varchar(200),party_account_details varchar(200),
                                reference_no varchar(200),order_no varchar(200),bill_type varchar(200),primary key(id)
                               )""")

        self.mycursor.execute("""CREATE TABLE IF NOT EXISTS SALES_ORDERS_LIST (id int not null auto_increment,name varchar(200),barcode varchar(200),batch_no varchar(200),
                                hsn_code varchar(200),quantity int,rate float,discount int,taxable_amount float,cgst int,sgst int,igst int,cess int,total_amount float,sales_order_id int,
                                CONSTRAINT sales_order_id FOREIGN KEY (sales_order_id) REFERENCES SALES_ORDERS(id),primary key(id) 
                            )""")


    def fetch_all(self):
        pass
    
    def insert_data(self,voucher_no,party_account_type,reference_no,order_no,bill_type):
        self.mycursor.execute("INSERT INTO SALES_ORDERS(order_voucher_no,party_account_details,reference_no,order_no,bill_type,date) VALUES ('"+voucher_no+"','"+party_account_type+"','"+reference_no+"','"+order_no+"','"+bill_type+"',curdate()) ")
        self.save_changes()
        return self.mycursor.lastrowid
    
    def insert_items_data(self,items_list,sales_order_id):
        for item in items_list:
            barcode = item[0]
            batch_no = item[1]
            item_name = item[2]
            hsn_code = item[3]
            quantity = item[4]
            rate = item[5]
            discount = item[6]
            taxable_amount = item[7]
            cgst = item[8]
            sgst = item[9]
            igst = item[10]
            cess = item[11]
            total_amount = item[12]
            self.mycursor.execute("INSERT INTO SALES_ORDERS_LIST (name,barcode,batch_no,hsn_code,quantity,rate,discount,taxable_amount,cgst,sgst,igst,cess,total_amount,sales_order_id) VALUES ('"+item_name+"','"+barcode+"','"+batch_no+"','"+hsn_code+"','"+quantity+"','"+rate+"','"+discount+"','"+taxable_amount+"','"+cgst+"','"+sgst+"','"+igst+"','"+cess+"','"+total_amount+"','"+str(sales_order_id)+"') ")
        self.save_changes()

    def update_data(self):
        pass

    def delete_data(self):
        pass

    def get_items(self):
        self.mycursor.execute("SELECT id AS item_id,name AS item_name FROM item_master")
        return self.mycursor.fetchall()

    
    def get_party_accounts(self):
        """Fetch all Ledgers Which Comes Under Sundry Credior Group"""
        self.mycursor.execute("select ledger.id AS ledger_id,ledger.name AS ledger_name from ledger INNER JOIN account_groups on ledger.account_group_id = account_groups.id where account_groups.name = 'Sundry Creditors'")
        return self.mycursor.fetchall()


    def get_item_details(self,item_id):
        self.mycursor.execute("SELECT barcode,hsn_code,selling_price FROM item_master where id = '"+item_id+"' ")
        return self.mycursor.fetchone()