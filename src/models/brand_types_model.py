from src.models.base_model import BaseModel

class BrandTypesModel(BaseModel):
    def __init__(self):
        super(BrandTypesModel, self).__init__()
        self._create_table()

    def _create_table(self):
        self.mycursor.execute(
            "CREATE TABLE IF NOT EXISTS BRAND_TYPES (id INT NOT NULL AUTO_INCREMENT,name varchar(200),primary key(id))")

        brand_types = ['Brands','Generic Brands', 'Others']
        for brand in brand_types:
            self.mycursor.execute(" insert into BRAND_TYPES (name) Select '"+brand+"' Where not exists(select * from BRAND_TYPES where name ='"+brand+"') ")
        self.save_changes()



    def insert_data(self,brand_type_name):
        self.mycursor.execute("INSERT INTO BRAND_TYPES (name) VALUES ('"+brand_type_name+"')")
        self.save_changes()

    def update_data(self,brand_type_name,brand_type_id):
        self.mycursor.execute("UPDATE BRAND_TYPES SET name = '"+brand_type_name+"' WHERE id = '"+brand_type_id+"' ")
        self.save_changes()

    def fetch_all(self):
        self.mycursor.execute("SELECT id,name from BRAND_TYPES")
        records = self.mycursor.fetchall()
        return records

    def delete_data(self,brand_type_id):
        self.mycursor.execute("DELETE FROM BRAND_TYPES WHERE ID = '"+brand_type_id+"' ")
        self.save_changes()


    def serach_brand(self):
        pass
