from src.models.base_model import BaseModel

class LiablityModel(BaseModel):
    def __init__(self):
        super(LiablityModel, self).__init__()
        self._create_table()

    def _create_table(self):
        self.mycursor.execute("CREATE TABLE IF NOT EXISTS ACCOUNT_GROUPS (id int not null auto_increment,name varchar(200),account_id int,primary key(id),CONSTRAINT account_id FOREIGN KEY(account_id) REFERENCES ACCOUNTS(id) )")
        liablity_groups = ['Outstanding Expenses','Duties And Taxes','Capital','Drawings','Bank Loans','Loans From Financers','Bills Payable','Sundry Creditors']
        for liablity in liablity_groups:
             self.mycursor.execute("insert into ACCOUNT_GROUPS (name,account_id) SELECT * FROM (SELECT '"+liablity+"','2') as temp WHERE NOT EXISTS ( SELECT name FROM ACCOUNT_GROUPS WHERE name = '"+liablity+"') ")
        self.save_changes()


    def fetch_all(self):
        self.mycursor.execute("select account_groups.id,account_groups.name,accounts.name as account_name from account_groups INNER JOIN accounts  ON account_groups.account_id = accounts.id where accounts.name = 'LIABLITY'")
        records = self.mycursor.fetchall()
        return records
    
    def insert_data(self,liablity_group_name):
        self.mycursor.execute("INSERT INTO ACCOUNT_GROUPS(name,account_id) VALUES ('"+liablity_group_name+"','2') ")
        self.save_changes()
    
    def update_data(self,liablity_group_name,liablity_group_id):
        self.mycursor.execute("UPDATE ACCOUNT_GROUPS SET name='"+liablity_group_name+"' where id = '"+liablity_group_id+"' ")
        self.save_changes()
    
    def delete_data(self,liablity_group_id):
        self.mycursor.execute("DELETE FROM ACCOUNT_GROUPS where id = '"+liablity_group_id+"' ")
        self.save_changes()


    def insert_into_ledger(self,ledger_name,ledger_type,contact_details,address_details,
                        phone_number,gst_type,gst_no,opening_balance,account_group_id,
                        tax_type,percentage_of_tax,
                        bank_name,bank_address,account_no,ifsc_code,branch_no,
                        branch_code,bank_contact,credit_period,pancard_no,duties_type):

        self.mycursor.execute("INSERT INTO LEDGER (name,type,contact_details,address,phone_no,gst_type,gst_no,opening_balance,account_group_id,tax_type,tax_percentage,bank_name,bank_address,account_number,ifsc_code,branch_no,branch_code,bank_contact_no,credit_period,pancard_no,duties_type) VALUES ('"+ledger_name+"','"+ledger_type+"','"+contact_details+"','"+address_details+"','"+phone_number+"','"+gst_type+"','"+gst_no+"','"+opening_balance+"','"+account_group_id+"','"+tax_type+"','"+percentage_of_tax+"','"+bank_name+"','"+bank_address+"','"+account_no+"','"+ifsc_code+"','"+branch_no+"','"+branch_code+"','"+bank_contact+"','"+credit_period+"','"+pancard_no+"','"+duties_type+"')")
        self.save_changes()
    
