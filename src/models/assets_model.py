from src.models.base_model import BaseModel

class AssetsModel(BaseModel):
    def __init__(self):
        super(AssetsModel, self).__init__()
        self._create_table()

    def _create_table(self):
        self.mycursor.execute("CREATE TABLE IF NOT EXISTS ACCOUNT_GROUPS (id int not null auto_increment,name varchar(200),account_id int,primary key(id),CONSTRAINT account_id FOREIGN KEY(account_id) REFERENCES ACCOUNTS(id) )")
        asset_groups = ['Current Asset','Bank','Cash in Hand','Petty Cash','Sundry debtors (customer)','Bills Receivable','Prepaid expense','Accured Income']
        for asset in asset_groups:
            self.mycursor.execute("insert into ACCOUNT_GROUPS (name,account_id) SELECT * FROM (SELECT '"+asset+"','1') as temp WHERE NOT EXISTS ( SELECT name FROM ACCOUNT_GROUPS WHERE name = '"+asset+"') ")
        self.save_changes()

    def fetch_all(self):
        self.mycursor.execute("select account_groups.id,account_groups.name,accounts.name as account_name from account_groups INNER JOIN accounts  ON account_groups.account_id = accounts.id where accounts.name = 'ASSET'")
        records = self.mycursor.fetchall()
        return records

    def insert_data(self,asset_group_name):
        self.mycursor.execute("INSERT INTO ACCOUNT_GROUPS(name,account_id) VALUES ('"+asset_group_name+"','1') ")
        self.save_changes()

    def update_data(self,asset_group_name,asset_group_id):
        self.mycursor.execute("UPDATE ACCOUNT_GROUPS SET name='"+asset_group_name+"' where id = '"+asset_group_id+"' ")
        self.save_changes()

    def delete_data(self,asset_group_id):
        self.mycursor.execute("DELETE FROM ACCOUNT_GROUPS where id = '"+asset_group_id+"' ")
        self.save_changes()


    #ledger
    def insert_to_ledger(self,ledger_name,ledger_type,credit_period,contact_details,address,
                        phone_number,pan_no,gst_type,gst_no,gst_percentage,
                        opening_balance,
                        bank_name,bank_address,account_no,ifsc_code,branch_no,branch_code,bank_contact,cheque_no,
                        account_group_id
                        ):
        

        self.mycursor.execute("INSERT INTO ledger (name,type,credit_period,contact_details,address,phone_no,pancard_no,gst_type,gst_no,gst_percentage,opening_balance,bank_name,bank_address,account_number,ifsc_code,branch_no,branch_code,bank_contact_no,cheque_no,account_group_id) VALUES ('{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}')".format(ledger_name,ledger_type,credit_period,contact_details,address,phone_number,pan_no,gst_type,gst_no,gst_percentage,opening_balance,bank_name,bank_address,account_no,ifsc_code,branch_no,branch_code,bank_contact,cheque_no,account_group_id))
        self.save_changes()