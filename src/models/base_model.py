import mysql.connector as connector
from abc import ABC, abstractmethod


class BaseModel(ABC):
    def __init__(self):
        self.db = self.connect_to_database()
        self.mycursor = self.db.cursor(dictionary=True)
        

    
    @abstractmethod
    def fetch_all(self):
        pass

    @abstractmethod
    def insert_data(self):
        pass

    @abstractmethod
    def update_data(self):
        pass

    @abstractmethod
    def delete_data(self):
        pass

    @abstractmethod
    def _create_table(self):
        pass



    def connect_to_database(self):
        db = connector.connect(host='localhost', user='admin', password='admin', db='drona')
        return db

    def open_connection(self):
        pass

    def close_connection(self):
        self.mycursor.close()
        self.db.close()

    def save_changes(self):
        self.db.commit()
        # self.close_connection()

