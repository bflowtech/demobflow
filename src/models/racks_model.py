from src.models.base_model import BaseModel


class RacksModel(BaseModel):
    def __init__(self):
        super(RacksModel, self).__init__()
        self._create_table()

    def _create_table(self):
        self.mycursor.execute(
            "CREATE TABLE IF NOT EXISTS RACKS (id INT NOT NULL AUTO_INCREMENT,name varchar(200),starts int,ends int,primary key(id))")



    def fetch_all(self):
        self.mycursor.execute("SELECT id AS rack_id,name AS rack_name,starts,ends from RACKS")
        records = self.mycursor.fetchall()
        return records


    def insert_data(self,rack_name, rack_starts, rack_ends):
        self.mycursor.execute(
            "INSERT INTO RACKS (name,starts,ends) VALUES ('" + rack_name + "','" + rack_starts + "','" + rack_ends + "')")
        self.save_changes()

    def update_data(self,rack_name,starts,ends,rack_id):
        self.mycursor.execute("UPDATE RACKS SET name = '" + rack_name + "',starts='" + starts + "', ends='"+ends+"' WHERE id ='" + rack_id + "' ")
        self.save_changes()

    def delete_data(self,rack_id):
        self.mycursor.execute("DELETE FROM RACKS WHERE ID = '"+rack_id+"' ")
        self.save_changes()


