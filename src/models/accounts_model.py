from src.models.base_model import BaseModel

class AccountsModel(BaseModel):
    def __init__(self):
        super(AccountsModel, self).__init__()
        self._create_table()
    
    def _create_table(self):
        self.mycursor.execute("CREATE TABLE IF NOT EXISTS ACCOUNTS (id int not null auto_increment,name varchar(200),primary key(id))")
        account_groups = ['ASSET','LIABLITY','INCOME','EXPENSE']
        for account in account_groups:
            self.mycursor.execute(" insert into ACCOUNTS (name) Select '"+account+"' Where not exists(select * from ACCOUNTS where name ='"+account+"') ")
        self.save_changes()
    
    def fetch_all(self):
        pass

    def insert_data(self):
        pass

    def update_data(self):
        pass

    def delete_data(self):
        pass